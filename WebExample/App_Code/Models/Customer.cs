﻿using System;
using System.Collections.Generic;
using System.Web;
using VGFramework;
using System.Globalization;
//using System.Dynamic;
using VGFramework.Data;

namespace Models
{
    /// <summary>
    /// Summary description for Customer
    /// </summary>
    public class Customer : VGFramework.Data.ORM
    {

        //private dynamic ORM;
        public List<string> Errors = new List<string>();
        public dynamic ORM;

        public Customer(Manager Manager, int id)
            : base(Manager, "Customers", true)
        {
            this.columns["idCustomer"].Value = id;
            this.Load();
        }

        //public override bool TrySetMember(SetMemberBinder binder, object value)
        //{
        //    if (!columns.ContainsKey(binder.Name))
        //    {
        //        return false;
        //    }

        //    switch (binder.Name)
        //    {
        //        case "Name":
        //            if (!this.nameValidation(value.ToString()))
        //            {
        //                throw new Exception("El parámetro Name es inválido");
        //            }
        //            break;
        //        case "Comment":
        //            if (!this.nameValidation(value.ToString()))
        //            {
        //                throw new Exception("El parámetro Comment es inválido");
        //            }
        //            break;
        //        default:
        //            break;
        //    }

        //    Column column = columns[binder.Name];
        //    column.Value = value;
        //    return true;
        //}


        public string Name
        {
            get { return columns["Name"].Value.ToString(); }
            set
            {
                if (!this.nameValidation(value))
                {
                    throw new Exception("El parámetro Name es inválido");
                }

                columns["Name"].Value = value;
            }
        }
        public string Comment
        {
            get { return columns["Comment"].Value.ToString(); }
            set
            {
                if (!this.commentValidation(value))
                {
                    throw new Exception("El parámetro Comment es inválido");
                }

                columns["Comment"].Value = value;
            }
        }
        public int idCustomer { get { return (int)columns["idCustomer"].Value; } }
        public DateTime? LastReport { get { return (DateTime?)columns["LastReport"].Value; } }
        public decimal? AccumulatedMinutes { get { return (decimal?)columns["AccumulatedMinutes"].Value; } }
        public int? NumberOfDays { get { return (int?)columns["NumberOfDays"].Value; } }
        public decimal? Projection { get { return (decimal?)columns["Projection"].Value; } }

        public bool nameValidation(string name)
        {
            if (name != null && Validator.Check(Validator.Type.Text, name, 1, 250))
                return true;
            else
                return false;
        }
        public bool commentValidation(string comment)
        {
            if (Validator.Check(Validator.Type.Text, comment, 0, 255))
                return true;
            else
                return false;
        }
        public bool lastReportValidation(string lastReport, out DateTime? result)
        {
            if (lastReport.Equals(""))
            {
                result = null;
                return true;
            }
            else
            {

                DateTime partialResult = new DateTime();
                IFormatProvider culture = CultureInfo.CreateSpecificCulture("en-US");
                DateTimeStyles styles = DateTimeStyles.None;

                if (DateTime.TryParse(lastReport, culture, styles, out partialResult))
                {
                    result = partialResult;
                    return true;
                }
                else
                {
                    result = null;
                    return false;
                }
            }
        }
        public bool accumulatedMinutesValidation(string accumulatedMinutes)
        {
            if ((accumulatedMinutes.Split('.')).Length <= 1)
            {
                if (Validator.Check(Validator.Type.Integer, accumulatedMinutes, 0, 18))
                    return true;
                else
                    return false;
            }
            else
            {
                if (Validator.Check(Validator.Type.Decimal, accumulatedMinutes, 1, 4) && accumulatedMinutes.Split('.')[0].Length <= 18)
                    return true;
                else
                    return false;
            }
        }
        public bool numberOfDaysValidation(string numberOfDays)
        {
            if (Validator.Check(Validator.Type.Integer, numberOfDays, 0, 11))
                return true;
            else
                return false;
        }
        public bool projectionValidation(string projection)
        {
            if ((projection.Split('.')).Length <= 1)
            {
                if (Validator.Check(Validator.Type.Integer, projection, 0, 18))
                    return true;
                else
                    return false;
            }
            else
            {
                if (Validator.Check(Validator.Type.Decimal, projection, 1, 4) && projection.Split('.')[0].Length <= 18)
                    return true;
                else
                    return false;
            }
        }
        public bool setProperties(Dictionary<string, string> Parameters)
        {
            string[] columns = { "Name", "Comment" };
            foreach (string column in columns)
            {
                try
                {
                    if (Parameters.ContainsKey(column.ToLower()))
                        GetType().GetProperty(column).SetValue(this, Parameters[column.ToLower()]);
                }
                catch (Exception e)
                {
                    Errors.Add(e.InnerException.Message);
                }
            }

            return Errors.Count == 0;
        }


    }
}