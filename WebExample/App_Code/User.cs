﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Text;
using System.Data;
using System.Text.RegularExpressions;
public class Users
{

    public void Login(VGFramework.WebManager Manager)
    {
        try
        {
            //obtener parametros
            string user = Manager.Parameters.Get("user", "");
            string pass = Manager.Parameters.Get("password", "");

            //validar parametros
            if (user.Equals("") || pass.Equals(""))
                throw new Exception("Debe ingresar Usuario y Password");

            //validar sql injection

            //buscar usuario
            dynamic WebOper = Manager.ORM("WebOper");
            WebOper.OperCode = this.RemoveSpecialCharacters(user);
            WebOper.Password = this.RemoveSpecialCharacters(pass);
            
            if (!WebOper.Load())
                throw new Exception("Usuario o contraseña incorrectos");

            //generar token de sesion
            string encodedToken = this.GenerateToken(Manager, Convert.ToString(WebOper.OperID), Manager.RequestHTTP.UserHostAddress);

            if (encodedToken == null)
                throw new Exception("Error de generacion de Token");

            Manager.Response.Set("access_token", encodedToken);
        }
        catch (Exception e)
        {
            Manager.Response.Set("Error", e.Message);
        }
    }

    /*Genera un token formado por idWebOper | timeLifeToken | ip*/
    public string GenerateToken(VGFramework.WebManager Manager, string operID, string ip)
    {
        string tokenLifeTime = DateTime.Now.AddMinutes(20).ToString();

        //string tokenString = Manager.Session["OperID"] + "|" + tokenLifeTime + "|" + Manager.RequestHTTP.UserHostAddress;
        string tokenString = operID + "|" + tokenLifeTime + "|" + ip;
        var tokenStringBytes = Encoding.UTF8.GetBytes(tokenString);
        string encodedToken = Convert.ToBase64String(tokenStringBytes);

        return encodedToken;
    }

    /*Recibe un token y verifica que sea valido*/
    public static void ValidateToken(VGFramework.WebManager Manager)
    {
        string encodedToken = Manager.Parameters.Get("access_token", "");

        if (encodedToken.Equals(""))
        {
            Manager.StatusCode = 407;
            throw new Exception("Debe incluir access_token");
        }

        var encodedTextBytes = Convert.FromBase64String(encodedToken);
        string plainText = Encoding.UTF8.GetString(encodedTextBytes);
        //token = idWebOper | timeLifeToken | ip
        var textToken = plainText.Split('|');

        var tokenTimeLife = Convert.ToDateTime(textToken[1]);
        if (tokenTimeLife < DateTime.Now)
        {
            Manager.StatusCode = 407;
            throw new Exception("Token expirado");
        }

        if (!textToken[2].Equals(Manager.RequestHTTP.UserHostAddress))
        {
            Manager.StatusCode = 407;
            throw new Exception("Token no valido");
        }

        Manager.Parameters.Remove("access_token");
    }

    public string RemoveSpecialCharacters(string str)
    {
        return Regex.Replace(str, @"--|[/\*]+|[;']+", "", RegexOptions.None);
    }
}