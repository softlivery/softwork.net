﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;

public class Customers
{
    //Recibe id customer, fecha de inicio y fecha de fin y devuelve el historico del mismo entre esas fechas. 
    public void history(VGFramework.WebManager Manager)
    {
        Users.ValidateToken(Manager);

        //recibir parametros
        string from = Manager.Parameters.Get("from", "");
        string to = Manager.Parameters.Get("to", "");
        int customer = ValidateCustomer(Manager);

        DateTime dateFrom = new DateTime();
        DateTime dateTo = new DateTime();
        if (from.Equals("") || to.Equals(""))
            throw new Exception("Debe especificar From y To");

        //validar parametros
        IFormatProvider culture = CultureInfo.CreateSpecificCulture("en-US");
        DateTimeStyles styles = DateTimeStyles.None;
        //Formato de fecha aceptado MM/dd/YYYY
        if (!DateTime.TryParse(from, culture, styles, out dateFrom) || !DateTime.TryParse(to + " 23:59:59", culture, styles, out dateTo))
            throw new Exception("Formato de fecha no valido");
        if (dateFrom > dateTo)
            throw new Exception("La fecha de inicio no puede ser mayor a la de finalización");

        VGFramework.Data.Report dbResults = new VGFramework.Data.Report(Manager, "Results");
        dbResults.Select("idcustomer", "customer");
        dbResults.Select("name", "name");
        dbResults.Select("datefrom", "day");
        dbResults.Select("duration", "minutes", "/60");
        dbResults.Select("callcount", "calls");
        dbResults.Filter("idcustomer", VGFramework.Data.Report.FilterType.EQUAL, customer.ToString(), VGFramework.Data.Report.Connector.AND);
        dbResults.Filter("datefrom", VGFramework.Data.Report.FilterType.GREATER_EQUAL, dateFrom.ToString("yyyy-MM-dd HH:mm:ss", culture), VGFramework.Data.Report.Connector.AND);
        dbResults.Filter("dateto", VGFramework.Data.Report.FilterType.LESS_EQUAL, dateTo.ToString("yyyy-MM-dd HH:mm:ss", culture));
        dbResults.Order("datefrom", VGFramework.Data.Report.OrderType.ASC);

        DataSet ds = dbResults.Execute();
        this.loadTableInManager(Manager, ds, "results");
    }

    private static int ValidateCustomer(VGFramework.WebManager Manager)
    {
        string customerID = Manager.Parameters.Get("customer", "");
        if (customerID.Equals(""))
            throw new Exception("Debe especificar un Customer");

        int customer;
        if (!int.TryParse(customerID, out customer))
            throw new Exception("El identificador de Customer no es valido");

        return customer;
    }

    //Recibe un id de customer y devuelve los datos correspodientes al mismo.
    //En caso de ser un metodo POST, realiza las modificaciones o la insercion y devuelve el objeto modificado o insertado
    public void customer(VGFramework.WebManager Manager)
    {
        Users.ValidateToken(Manager);

        //recibir parametros
        int id = ValidateCustomer(Manager);

        Models.Customer customer = new Models.Customer(Manager, id);
        Manager.Response.Set("Name", customer.Name);
        Manager.Response.Set("Comment", customer.Comment);
        Manager.Response.Set("LastReport", customer.LastReport);
        Manager.Response.Set("AccumulatedMinutes", customer.AccumulatedMinutes);
        Manager.Response.Set("NumberOfDays", customer.NumberOfDays);
        Manager.Response.Set("Projection", customer.Projection);

        string[] parts = customer.Name.Split('-');
        if (parts.Length == 2)
        {
            Manager.Response.Set("Type", parts[0].Trim());
            Manager.Response.Set("Name", parts[1].Trim());
        }

        if (!Manager.isMethod("post"))
            return;

        if (!customer.setProperties(Manager.Parameters.All()))
        {
            Manager.StatusCode = 400;
            Manager.Response.Set("errors", customer.Errors);
            throw new Exception("Error al modificar Customer");
        }

        //guardar

        //actualizar respuesta
        Manager.Response.Set("Name", customer.Name);
        Manager.Response.Set("Comment", customer.Comment);
        Manager.Response.Set("LastReport", customer.LastReport);
        Manager.Response.Set("AccumulatedMinutes", customer.AccumulatedMinutes);
        Manager.Response.Set("NumberOfDays", customer.NumberOfDays);
        Manager.Response.Set("Projection", customer.Projection);

        parts = customer.Name.Split('-');
        if (parts.Length == 2)
        {
            Manager.Response.Set("Type", parts[0].Trim());
            Manager.Response.Set("Name", parts[1].Trim());
        }
    }

    //Devuelve todos los customers. 
    //En caso de tener parametros, aplica el filtro. 
    public void traffic(VGFramework.WebManager Manager)
    {
        Users.ValidateToken(Manager);

        List<String> acceptParameters = new List<string>();
        //Los parametros aceptables deben guardarse en mayusculas. 
        acceptParameters.Add("NAME");
        acceptParameters.Add("COMMENT");
        acceptParameters.Add("LASTREPORT");
        acceptParameters.Add("ACCUMULATEDMINUTES");
        acceptParameters.Add("NUMBEROFDAYS");
        acceptParameters.Add("PROJECTION");
        //validar parametros
        VGFramework.Data.Report dbCustomers = new VGFramework.Data.Report(Manager, "Customers");
        int count = Manager.Parameters.Count - 1;
        foreach (var parameter in Manager.Parameters.All())
        {
            var splitParam = parameter.Key.Split('-');
            if (!acceptParameters.Contains(splitParam[splitParam.Length - 1].ToUpper()))
                throw new Exception("Parámetro incorrecto");
            if (count == 0)
                this.FilterFromParameter(dbCustomers, parameter.Key, parameter.Value);
            else
                this.FilterFromParameter(dbCustomers, parameter.Key, parameter.Value, VGFramework.Data.Report.Connector.AND);

            count--;
        }

        DataSet ds = dbCustomers.Execute();
        this.loadTableInManager(Manager, ds, "customers");

    }

    public void FilterFromParameter(VGFramework.Data.Report report, string key, string value, VGFramework.Data.Report.Connector connector = VGFramework.Data.Report.Connector.NONE)
    {
        var keySplit = key.Split('-');

        if (keySplit.Length == 1)
        {
            report.Filter(keySplit[0], VGFramework.Data.Report.FilterType.EQUAL, value, connector);
            return;
        }
        var operador = key.Substring(0, key.IndexOf('-'));
        VGFramework.Data.Report.FilterType typeOperator = new VGFramework.Data.Report.FilterType();

        switch (operador.ToUpper())
        {
            case "LESS":
                typeOperator = VGFramework.Data.Report.FilterType.LESS;
                break;
            case "LESS_EQUAL":
                typeOperator = VGFramework.Data.Report.FilterType.LESS_EQUAL;
                break;
            case "GREATER":
                typeOperator = VGFramework.Data.Report.FilterType.GREATER;
                break;
            case "GREATER_EQUAL":
                typeOperator = VGFramework.Data.Report.FilterType.GREATER_EQUAL;
                break;
            case "REGEXP":
                typeOperator = VGFramework.Data.Report.FilterType.REGEXP;
                break;
            case "POSTFIX":
                typeOperator = VGFramework.Data.Report.FilterType.POSTFIX;
                break;
            case "PREFIX":
                typeOperator = VGFramework.Data.Report.FilterType.PREFIX;
                break;
            case "LIKE":
                typeOperator = VGFramework.Data.Report.FilterType.LIKE;
                break;
            case "NOT_LIKE":
                typeOperator = VGFramework.Data.Report.FilterType.NOT_LIKE;
                break;
            case "EQUAL":
            default:
                typeOperator = VGFramework.Data.Report.FilterType.EQUAL;
                break;
        }
        report.Filter(keySplit[1], typeOperator, value, connector);
    }

    //Recibe un manager, un ds y el nombre de la tabla, carga en el manager los datos de la ds pasado por parametro 
    //Si el dataset tiene solo un registro, el tableName no se utiliza
    public void loadTableInManager(VGFramework.WebManager Manager, DataSet ds, string tableName)
    {
        if (Manager == null || ds == null)
        {
            throw new Exception("Fail to load table in Manager");
        }

        if (ds.Tables[0].Rows.Count == 1)
        {
            var row = ds.Tables[0].Rows[0];
            var column = ds.Tables[0].Columns;

            foreach (DataColumn col in column)
            {
                Manager.Response.Set(col.ColumnName, row[col]);
            }
            return;
        }
        else
        {
            List<Dictionary<string, object>> table = new List<Dictionary<string, object>>();
            Dictionary<string, object> data;

            foreach (DataTable dt in ds.Tables)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    data = new Dictionary<string, object>();
                    foreach (DataColumn col in dt.Columns)
                    {
                        data.Add(col.ColumnName, dr[col]);
                    }
                    table.Add(data);
                }
            }
            Manager.Response.Set(tableName, table);
        }
    }
}