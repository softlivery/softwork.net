﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public class Demo
{
    public Demo()
    {
        int u = 0;
    }
    public void index(VGFramework.WebManager Manager)
    {
        //Manager.ResponseAdd("test", "any value");
        //dynamic dbWebOper = Manager.ORM("WebOper");
        //Manager.ResponseAdd("password", dbWebOper.Password);
        //dbWebOper.Password = "1234567890";
        //Manager.ResponseAdd("password2", dbWebOper.Password);

        VGFramework.Data.Report dbWebOpers = new VGFramework.Data.Report(Manager, "WebOper");
        dbWebOpers.Select("OperCode", "Usuario");
        dbWebOpers.Select("Password", "Clave");
        dbWebOpers.Filter("OperID", VGFramework.Data.Report.FilterType.EQUAL, 1, VGFramework.Data.Report.Connector.OR);
        dbWebOpers.Filter("OperID", VGFramework.Data.Report.FilterType.GREATER_EQUAL, 2);


        
        DataSet ds = dbWebOpers.Execute();
        ArrayList root = new ArrayList();
        List<Dictionary<string, object>> table;
        Dictionary<string, object> data;
        
        foreach (DataTable dt in ds.Tables)
        {
            table = new List<Dictionary<string, object>>();
            foreach (DataRow dr in dt.Rows)
            {
                data = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    data.Add(col.ColumnName, dr[col]);
                }
                table.Add(data);
            }
            root.Add(table);
        }

        Manager.Response.Set("rows", root[0]);
    }
}