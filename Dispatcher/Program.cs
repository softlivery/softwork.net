﻿using System;
using System.Data;
using VGFramework;

namespace Dispatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager Manager = new Manager();

            dynamic Usuario = Manager.ORM("Usuarios");
            Usuario.ID = 1;
            Usuario.Load();
            Console.WriteLine(Usuario.SendSMS + " " + Usuario.SendSMS.GetType());
            Console.WriteLine(Usuario.TipoBilletera + " " + Usuario.TipoBilletera.GetType());
            Console.WriteLine(Usuario.TipoBilleteraDesde + " " + Usuario.TipoBilleteraDesde.GetType());
            Console.WriteLine(Usuario.SaldoReal + " " + Usuario.SaldoReal.GetType());
            //Console.WriteLine(Usuarios.Estado_Fincimex + " " + Usuarios.Estado_Fincimex.GetType());
            Console.WriteLine(Usuario.Pass + " " + Usuario.Pass.GetType());
            Console.WriteLine(Usuario.HashVerifEmail + " " + Usuario.HashVerifEmail.GetType());
            Console.WriteLine(Usuario.GetString("NroBilletera") + " test");

            //Console.WriteLine(Validator.Check(Validator.Type.URL, "www.voipgroup.com"));
        }
    }
}
