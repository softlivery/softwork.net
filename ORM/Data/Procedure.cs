﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    public class Procedure : ORM
    {
        private string name;
        private List<string> Parameters = new List<string>();

        public Procedure(Manager Manager, string name) : base(Manager, "")
        {
            this.name = name;
        }

        public DataSet Execute()
        {
            var query = ToString();
            manager.LogDB(query);

            var ds = new DataSet();
            var command = new SqlCommand(query, Connect());
            var adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);
            return ds;
        }

        public override string ToString()
        {
            return "exec " + name + " " + string.Join(",", Parameters);
        }

        public void AddParameter(string parameter)
        {
            Parameters.Add("'" + parameter + "'");
        }

        public void AddParameter(object parameter)
        {
            Parameters.Add(parameter.ToString());
        }

        public void AddParameter(string name, object value, string type = "")
        {
            Parameters.Add(name + "='" + Column.ValueToDB(type, value).ToString() + "'");
        }
    }
}
    