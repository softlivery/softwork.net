﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace VGFramework.Data
{
    public class File
    {
        public Dictionary<string, Dictionary<string, string>> AllFiles { get; set; }
        
        public void ReceiveFiles(HttpFileCollection uploadFiles, List<string> dirs, List<string> names)
        {
            AllFiles = new Dictionary<string, Dictionary<string, string>>();
            for (int i = 0; i < uploadFiles.Count; i++)
            {
                string name = "";
                string dir = new Manager().GetDirPath("uploadFiles");

                if (dirs.ElementAtOrDefault(i) != null && dirs.ElementAtOrDefault(i) != "")
                    dir = dirs[i];
                if (names.ElementAtOrDefault(i) != null && names.ElementAtOrDefault(i) != "")
                    name = names[i];

                Dictionary<string, string> fileData = ProcessFile(uploadFiles[i], dir, name);

                if (fileData.Count != 0)
                {
                    if (!AllFiles.Keys.Any(x => x == uploadFiles.GetKey(i)))
                        AllFiles.Add(uploadFiles.GetKey(i), fileData);
                    else
                        AllFiles.Add(uploadFiles.GetKey(i) + AllFiles.Count, fileData);
                }
            }
        }

        private Dictionary<string, string> ProcessFile(HttpPostedFile file, string dir, string name = "")
        {
            Dictionary<string, string> fileData = new Dictionary<string, string>();
            if (name == "")
                name = file.FileName;
            else
                name = name + Path.GetExtension(file.FileName);

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            var filePath = dir + name;

            if (Path.GetExtension(filePath) != "")
            {
                file.SaveAs(filePath);
                fileData.Add("Path", filePath);
                fileData.Add("Name", name);
                fileData.Add("Extension", Path.GetExtension(filePath));
                fileData.Add("MimeType", file.ContentType);
                fileData.Add("Uploaded", DateTime.Now.ToString());
            }
            return fileData;
        }

        public void Move(string pathFrom, string pathTo)
        {
            string dir = pathTo.Replace(Path.GetFileName(pathTo), "");
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            System.IO.File.Move(pathFrom, pathTo);
        }

        public void DeleteFromDB(int id)
        {
            Manager manager = new Manager();
            dynamic File = manager.InternalORM("Files");
            File.Id = id;
            if (!File.Load())
                throw new Exception("File doesn't exist");
            File.Delete();
        }

        public int SaveDB(Dictionary<string, string> fileData)
        {
            int id = -1;
            byte[] bytes;
            string path = fileData["Path"];
            bytes = System.IO.File.ReadAllBytes(path);

            //Manager manager = new Manager();
            //dynamic File = manager.InternalORM("Files");
            //File.Path = path;
            //File.Name = Path.GetFileName(path);
            //File.Extension = fileData["Extension"];
            //File.MimeType = fileData["MimeType"];
            //File.Uploaded = fileData["DateUpload"];
            //File.User = fileData["User"];
            //File.Content = bytes;
            //File.IdAssociated = null;
            //File.Save();

            string query = "INSERT INTO [MakeList].[dbo].[VGF_Files] VALUES(@Path, @Name, @Extension, @MimeType, @Uploaded, @User, @Content, @IdAssociated) SELECT @@IDENTITY";

            Manager manager = new Manager();
            using (SqlConnection con = new SqlConnection(manager.GetConfig("ConnectionString")))
            using (SqlCommand cmd = new SqlCommand(query, con))
            {
                cmd.Parameters.AddWithValue("@Path", path);
                cmd.Parameters.AddWithValue("@Name", Path.GetFileName(path));
                cmd.Parameters.AddWithValue("@Extension",fileData["Extension"]);
                cmd.Parameters.AddWithValue("@MimeType", fileData["MimeType"]);
                cmd.Parameters.AddWithValue("@Uploaded", fileData["Uploaded"]);
                cmd.Parameters.AddWithValue("@User", fileData["User"]);
                SqlParameter param = cmd.Parameters.Add("@Content", System.Data.SqlDbType.VarBinary);
                cmd.Parameters.AddWithValue("@IdAssociated", "NULL");
                param.Value = bytes;

                con.Open();
                id = Convert.ToInt32(cmd.ExecuteScalar());
                con.Close();
            }
            return id;
        }

        public Dictionary<string, string> Load(int id)
        {
            Dictionary<string, string> fileData = new Dictionary<string, string>();
            //string query = "SELECT * FROM [MakeList].[dbo].[VGF_Files] WHERE Id = @Id";

            //Manager manager = new Manager();
            //using (SqlConnection con = new SqlConnection(manager.GetConfig("ConnectionString")))
            //using (SqlCommand cmd = new SqlCommand(query, con))
            //{
            //    cmd.Parameters.AddWithValue("@Id", id);
            //    var ds = new DataSet();
            //    var adapter = new SqlDataAdapter(cmd);
            //    adapter.Fill(ds);
            //    con.Open();
            //    con.Close();

            //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //        for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
            //            fileData.Add(ds.Tables[0].Columns[j].ToString(), ds.Tables[0].Rows[i][j].ToString());

            //    //return fileData;
            //}

            Manager manager = new Manager();
            dynamic File = manager.InternalORM("Files");
            File.Id = id;
            if (!File.Load())
                throw new Exception("File cannot be loaded");
            fileData.Add("Id", File.Id.ToString());
            fileData.Add("Path", File.Path.ToString());
            fileData.Add("Name", File.Name.ToString());
            fileData.Add("Extension", File.Extension.ToString());
            fileData.Add("MimeType", File.MimeType.ToString());
            fileData.Add("Uploaded", File.Uploaded.ToString());
            fileData.Add("User", File.User.ToString());
            fileData.Add("Content", File.Content.ToString());
            fileData.Add("IdAssociated", File.IdAssociated.ToString());
            return fileData;
        }

        public bool Validate(string type, HttpPostedFile file)
        {
            bool validate = false;
            switch (type)
            {
                case "audio":
                    if (file.ContentType == "audio/aac" ||
                        file.ContentType == "audio/mpeg" ||
                        file.ContentType == "audio/webm" ||
                        file.ContentType == "audio/ogg" ||
                        file.ContentType == "audio/wav" ||
                        file.ContentType == "audio/midi" ||
                        file.ContentType == "audio/x-wav")
                        validate = true;
                    break;
                case "image":
                    if (file.ContentType == "image/gif" ||
                        file.ContentType == "image/png" ||
                        file.ContentType == "image/jpeg" ||
                        file.ContentType == "image/bmp" ||
                        file.ContentType == "image/webp" ||
                        file.ContentType == "image/svg+xml" ||
                        file.ContentType == "image/tiff" ||
                        file.ContentType == "image/x-icon")
                        validate = true;
                    break;
                case "text":
                    if (file.ContentType == "text/plain" ||
                        file.ContentType == "text/html" ||
                        file.ContentType == "text/css" ||
                        file.ContentType == "text/javascript" ||
                        file.ContentType == "text/csv")
                        validate = true;
                    break;
                case "video":
                    if (file.ContentType == "video/webm" ||
                        file.ContentType == "video/ogg" ||
                        file.ContentType == "video/x-msvideo" ||
                        file.ContentType == "video/mpeg")
                        validate = true;
                    break;
                case "application":
                    if (file.ContentType == "application/octet-stream" ||
                        file.ContentType == "application/pkcs12" ||
                        file.ContentType == "application/vnd.mspowerpoint" ||
                        file.ContentType == "application/xhtml+xml" ||
                        file.ContentType == "application/xml" ||
                        file.ContentType == "application/pdf" ||
                        file.ContentType == "application/x-bzip" ||
                        file.ContentType == "application/x-bzip2" ||
                        file.ContentType == "application/msword" ||
                        file.ContentType == "application/javascript" ||
                        file.ContentType == "application/json" ||
                        file.ContentType == "application/x-rar-compressed" ||
                        file.ContentType == "application/vnd.ms-excel" ||
                        file.ContentType == "application/zip" ||
                        file.ContentType == "application/x-7z-compressed")
                        validate = true;
                    break;
                default:
                    break;
            }
            return validate;
        }
    }
}
