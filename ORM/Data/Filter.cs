﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace VGFramework.Data
{
    public class Filter
    {
        private List<object> fields = new List<object>();
        private string column;
        private Report.FilterType type;
        private string value;
        private string optionalValue;
        public Report.Connector connector { get; set; }

        public Filter(List<object> list, Report.FilterType type, Report.Connector connector)
        {
            fields = list;
            column = list[0].ToString();
            list.RemoveAt(0);
            value = string.Join(",", list);
            this.type = type;
            this.connector = connector;
        }

        public Filter(string column, Report.FilterType type, string value, Report.Connector connector = Report.Connector.NONE)
        {
            if (column.Contains("'"))
                this.column = column;
            else
                this.column = new Column(column).ToString();
            this.type = type;
            this.value = value;
            this.connector = connector;
        }

        public Filter(string column, Report.FilterType type, int value, Report.Connector connector = Report.Connector.NONE, int optionalValue = 0)
        {
            if (column.Contains("'"))
                this.column = column;
            else
                this.column = new Column(column).ToString();
            this.type = type;
            this.value = value.ToString();
            this.connector = connector;
            //OptionalValue = optionalValue.ToString();
        }
        public Filter(string column, Report.FilterType type, double value, Report.Connector connector = Report.Connector.NONE, double optionalVvalue = 0)
        {
            if (column.Contains("'"))
                this.column = column;
            else
                this.column = new Column(column).ToString();
            this.type = type;
            this.value = value.ToString(CultureInfo.InvariantCulture);
            this.connector = connector;
            //OptionalValue = optionalValue.ToString(CultureInfo.InvariantCulture);
        }

        public Filter(IStatement column, Report.FilterType type, string value, Report.Connector connector = Report.Connector.NONE)
        {
            this.column = column.ToString();
            this.type = type;
            this.value = value;
            this.connector = connector;
        }

        public Filter(IStatement column, Report.FilterType type, int value, Report.Connector connector = Report.Connector.NONE, int optionalValue = 0)
        {
            this.column = column.ToString();
            this.type = type;
            this.value = value.ToString();
            this.connector = connector;
            //OptionalValue = optionalValue.ToString();
        }

        public Filter(IStatement column, Report.FilterType type, double value, Report.Connector connector = Report.Connector.NONE, double optionalVvalue = 0)
        {
            this.column = column.ToString();
            this.type = type;
            this.value = value.ToString(CultureInfo.InvariantCulture);
            this.connector = connector;
            //OptionalValue = optionalValue.ToString(CultureInfo.InvariantCulture);
        }

        public override string ToString()
        {
            string result = "";
            switch (this.type)
            {
                case Report.FilterType.LESS:
                    result += this.column + " < '" + this.value + "'";
                    break;
                case Report.FilterType.LESS_EQUAL:
                    result += this.column + " <= '" + this.value + "'";
                    break;
                case Report.FilterType.GREATER:
                    result += this.column + " > '" + this.value + "'";
                    break;
                case Report.FilterType.GREATER_EQUAL:
                    result += this.column + " >= '" + this.value + "'";
                    break;
                case Report.FilterType.REGEXP:
                    result += "'" + this.value + "' REGEXP " + this.column;
                    break;
                case Report.FilterType.POSTFIX:
                    result += this.column + " LIKE '%" + this.value + "'";
                    break;
                case Report.FilterType.PREFIX:
                    result += this.column + " LIKE '" + this.value + "%'";
                    break;
                case Report.FilterType.LIKE:
                    result += this.column + " LIKE '%" + this.value + "%'";
                    break;
                case Report.FilterType.NOT_LIKE:
                    result += this.column + " NOT LIKE '%" + this.value + "%'";
                    break;
                case Report.FilterType.BETWEEN:
                    string[] parts = this.value.Split(',');
                    if (parts.Length != 2)
                        return "";
                    result += this.column + " BETWEEN '" + parts[0] + "' AND '" + parts[1] + "'";
                    break;
                case Report.FilterType.IN:
                    result += this.column + " IN ('" + string.Join("','", this.value.Split(',')) + "')";
                    break;
                case Report.FilterType.NOT_IN:
                    result += this.column + " NOT IN ('" + string.Join("','", this.value.Split(',')) + "')";
                    break;
                case Report.FilterType.ISNULL:
                    result += this.column + " IS NULL";
                    break;
                case Report.FilterType.ISNOTNULL:
                    result += this.column + " IS NOT NULL";
                    break;
                case Report.FilterType.NOT_EQUAL:
                    result += this.column + "<> '" + this.value + "'";
                    break;
                case Report.FilterType.EQUAL:
                default:
                    result += this.column + " = '" + this.value + "'";
                    break;
            }
            if (this.connector != Report.Connector.NONE)
                return string.Format("({0}) {1} ", result, this.connector.ToString());
            //return string.Format("\n({0})", result);
            return result;
        }
    }
}
