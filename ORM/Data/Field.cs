﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace VGFramework.Data
{
    public class Field
    {
        string sql;
        string alias;

        public Field(string column)
        {
            if (Regex.IsMatch(column, @"^[0-9]+$"))
            {
                sql = column;
            }
            else if (Regex.IsMatch(column, @"^[a-zA-Z0-9]+(.[a-zA-Z0-9]+)*$"))
            {
                sql = "[" + string.Join("].[", column.Split('.')) + "]";
            }
            /*else if (Regex.IsMatch(column, @"^[a-zA-Z]+$"))
            {

            }*/
            else
            {
                sql = column;
            }
            alias = null;
        }

        public Field(string column, string alias) : this(column)
        {
            this.alias = alias;
        }

        override public string ToString()
        {
            if (alias != null && alias != "")
                return string.Format("{0} AS '{1}'", sql, alias);
            return sql;
        }
    }
}
