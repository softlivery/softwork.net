﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    public class Component
    {
        private Dictionary<string, object> at = new Dictionary<string, object>();
        private string type;
        public string value { get; }
        public string alias { get; set; }
        private List<string> buttons = new List<string>();
        private List<Dictionary<string, object>> options = new List<Dictionary<string, object>>();

        public Component(string id, string type, string parentId, string value, string name, string order, Component parent = null)
        {
            this.type = type;
            this.value = value;
            this.alias = name;
            switch (type)
            {
                case "form":
                    break;
                case "section":
                    at.Add("id", int.Parse(id));
                    at.Add("name", name);
                    at.Add("order", int.Parse(order));
                    break;
                case "button":
                    if (value == "")
                        AddButtons(parent, name);
                    else
                    {
                        at.Add("icon", alias);
                        at.Add("route", value);
                    }
                    break;
                case "option":
                    at.Add("order", int.Parse(order));
                    at.Add("name", name);
                    AddOptions(parent, this.Get());
                    break;
                case "data":
                case "datakey":
                case "datamethod":
                case "datavalue":
                    parent.at.Add(type, name);
                    break;
                case "column":
                    at.Add("key", name);
                    at.Add("label", value);
                    break;
                default:
                    at.Add("order", int.Parse(order));
                    at.Add("type", type);
                    at.Add("value", value);
                    at.Add("name", name);
                    at.Add("section", int.Parse(parentId));
                    break;
            }
        }

        public void AddOptions(Component component, Dictionary<string, object> option)
        {
            component.options.Add(option);
        }

        public void AddButtons(Component component, string button)
        {
            component.buttons.Add(button);
        }

        public Dictionary<string, object> Get()
        {
            if (type != "form" && type != "button" && type != "option" && type != "column"/*this.buttons.Count() > 0 && this.options.Count() > 0*/)
            {
                at.Add("buttons", buttons);
                at.Add("options", options);
            }
            return at;
        }
    }
}
