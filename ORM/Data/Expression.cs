﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    public class Expression : IStatement
    {
        private object[] args;
        public Expression(params string[] args)
        {
            this.args = args;
            for (int i = 0; i < args.Count(); i++)
            {
                if (!args[i].Contains("'"))
                {
                    args[i] = new Column(args[i]).ToString();
                }
            }
        }

        public override string ToString()
        {
            return string.Join(" + ", args);
        }
    }
}
