﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;

namespace VGFramework.Data
{
    public class ORM : DynamicObject
    {
        protected Manager manager;
        protected string table;
        protected string connectionString;
        protected bool empty;
        protected bool insertedData;
        protected Dictionary<string, Column> columns;
        protected List<string> keys;
        protected List<string> computed;
        protected static SqlConnection connection;
        protected static SqlCommand command;
        protected static SqlDataReader result;

        private bool Modify(string key, object value)
        {
            if (columns.ContainsKey(key))
            {
                Column column = columns[key];
                column.Value = value;
                return true;
            }
            return false;
        }

        public object this[string key]
        {
            get
            {
                if (columns.ContainsKey(key))
                {
                    Column column = columns[key];
                    return column.Value;
                }
                return null;
            }
            set
            {
                Modify(key, value);
            }
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = null;
            if (columns.ContainsKey(binder.Name))
            {
                Column column = columns[binder.Name];
                result = column.Value;
                return true;
            }
            return false;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            return Modify(binder.Name, value);
        }

        public virtual void Initilize(Manager Manager, string Table)
        {
            manager = Manager;
            connectionString = manager.GetConfig("ConnectionString");
            table = Table;
            empty = true;
            insertedData = true;
            columns = new Dictionary<string, Column>();
            keys = new List<string>();
            computed = new List<string>();
        }

        protected SqlConnection Connect()
        {
            if (connection == null || connection.State != System.Data.ConnectionState.Open)
            {
                try
                {
                    manager.LogDebug("Connect to " + connectionString);
                    connection = new SqlConnection(connectionString);
                    connection.Open();
                }
                catch(Exception e)
                {
                    manager.LogError(e.Message);
                    throw new Exception("orm_connect", e);
                }
            }
            return connection;
        }

        protected DataTable Load(string query)
        {
            DataTable dt = new DataTable();

            try
            {
                manager.LogDB(query);
                SqlDataAdapter da = new SqlDataAdapter(query, Connect());
                da.Fill(dt);
            }
            catch (Exception e)
            {
                manager.LogError(e.Message);
                throw new Exception("orm_query", e);
            }

            return dt;
        }
        protected SqlDataReader Query(string Query)
        {
            try
            {
                manager.LogDB(Query);
                command = new SqlCommand(Query, Connect());
                if (result != null && !result.IsClosed)
                {
                    result.Close();
                }
                result = command.ExecuteReader();
            }
            catch (Exception e)
            {
                result = null;
                manager.LogError(e.Message);
                throw new Exception("orm_query", e);
            }
            return result;
        }

        protected SqlDataReader Query(string Query, Dictionary<string, object> Params)
        {
            try
            {
                manager.LogDB(Query);
                command = new SqlCommand(Query, Connect());
                command.CommandTimeout = 60;

                foreach (string param in Params.Keys)
                {
                    command.Parameters.AddWithValue(param, Params[param]);
                }

                if (result != null && !result.IsClosed)
                {
                    result.Close();
                }
                result = command.ExecuteReader();
            }
            catch (Exception e)
            {
                result = null;
                manager.LogError(e.Message);
                throw new Exception("orm_query", e);
            }
            return result;
        }

        protected ORM(Manager Manager, string Table)
        {
            Initilize(Manager, Table);
        }

        public ORM(Manager Manager, string Table, bool Hydrate = true)
        {
            Initilize(Manager, Table);
            if (Hydrate && !HydrateStruct())
            {
                throw new Exception("orm_contruct_hydration");
            }
        }

        protected void AddColumn(string name, string type, object @default, bool nullable, bool computed, int length)
        {
            columns.Add(name, new Column(name, type, @default, nullable, computed, length));
        }

        protected void AddKey(string name)
        {
            keys.Add(name);
        }

        private bool HydrateStruct()
        {
            //leer columnas y valores desde la DB
            //DataTable dt = Load(string.Format("SELECT * FROM information_schema.columns WITH (NOLOCK) where TABLE_NAME = '{0}'", table));
            DataTable dt = Load(string.Format("SELECT ic.*,sc.is_computed FROM information_schema.columns ic WITH (NOLOCK) LEFT JOIN sys.columns sc WITH(NOLOCK) ON ic.COLUMN_NAME = sc.name WHERE ic.TABLE_NAME = '{0}' AND object_id = OBJECT_ID('{0}')", table));
            if(dt.Rows.Count > 0)
            {
                Column column;
                foreach (var row in dt.AsEnumerable())
                {
                    column = Column.From(row);
                    columns.Add(column.Name, column);
                    if (column.Computed)
                        computed.Add(column.Name);
                }
            }

            /*
            SqlDataReader reader = Query(string.Format("SELECT * FROM information_schema.columns WITH (NOLOCK) where TABLE_NAME = '{0}'", table));
            if (reader == null || !reader.HasRows)
            {
                reader.Close();
                return false;
            }

            try
            {
                Column column;
                while (reader.Read())
                {
                    column = Column.FromReader(reader);
                    columns.Add(column.Name, column);
                }
                reader.Close();
            }
            catch (Exception e)
            {
                throw new Exception("orm_hydrate_column", e);
            }
            */

            return HydrateKeys();
        }

        private bool HydrateKeys()
        {
            DataTable dt = Load(string.Format(@"SELECT column_name
                FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
                INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
                    ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY'
                    AND TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
                    AND KU.table_name = '{0}'
                ORDER BY KU.ORDINAL_POSITION", table));

            if (dt.Rows.Count > 0)
            {
                foreach (var row in dt.AsEnumerable())
                {
                    keys.Add(row[0].ToString());
                }
            }

            /*
            SqlDataReader reader = Query(string.Format(@"SELECT column_name
                FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS TC
                INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS KU
                    ON TC.CONSTRAINT_TYPE = 'PRIMARY KEY'
                    AND TC.CONSTRAINT_NAME = KU.CONSTRAINT_NAME
                    AND KU.table_name = '{0}'
                ORDER BY KU.ORDINAL_POSITION", table));
            if (reader == null)
            {
                return false;
            }

            if (!reader.HasRows)
            {
                reader.Close();
                return true;
            }

            try
            {
                while (reader.Read())
                {
                    //keys.Add(reader.GetString(0));
                    keys.Add(reader["column_name"].ToString());
                }
                reader.Close();
            }
            catch (Exception e)
            {
                throw new Exception("orm_hydrate_key", e);
            }*/

            return true;
        }

        public bool Load()
        {
            Column column;
            string where = "";
            foreach (string key in columns.Keys)
            {
                column = columns[key];
                if (column.Modified)
                {
                    where += " [" + column.Name + "]=" + column.ValueQuery + " AND";
                }
            }
            where = where.Substring(0, where.Length - 4);

            //obtener columnas y valores desde una consulta where keys
            DataTable dt = Load(string.Format("SELECT * FROM [{0}] WITH (NOLOCK) WHERE {1}", table, where));
            if (dt.Rows.Count > 0)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (!columns.ContainsKey(col.ColumnName))
                    {
                        throw new Exception("orm_hydrate_column");
                    }

                    columns[col.ColumnName].ValueDB = dt.Rows[0][col];
                }
                empty = false;
                return true;
            }
            return false;
        }
        
        public string[] Properties()
        {
            return columns.Keys.ToArray();
        }

        public bool Empty()
        {
            return empty;
        }

        public string GetString(string column)
        {
            if(columns.ContainsKey(column))
            {
                Column col = columns[column];
                return col.GetString();
            }
            throw new Exception("orm_getstring_column");
        }

        public Column GetColumn(string column)
        {
            if (columns.ContainsKey(column))
            {
                return columns[column];
            }
            throw new Exception("orm_getcolumn");
        }

        public static string ParseColumnName(string column)
        {
            if (column.Contains("."))
            {
                return "[" + string.Join("].[", column.Split('.')) + "]";
            }
            return "[" + column + "]";
        }

        public Dictionary<string, object> ToDictionary()
        {
            return columns.ToDictionary(k => k.Key, k => k.Value.Value);
        }

        public Dictionary<string, string> ToSession()
        {
            return columns.ToDictionary(k => k.Key, k => k.Value.Value.ToString());
        }

        public bool Save()
        {
            if (empty)
            {
                return Insert();
            }

            return Update();
        }

        private bool Update()
        {
            List<string> ignored = new List<string>(keys);
            ignored.AddRange(computed);

            string where = string.Join(" AND ", keys.Select(key => string.Format("[{0}]={1}", key, columns[key].ValueQuery)));
            string values = string.Join(", ", columns.Keys.Except(ignored).Select(key => string.Format("[{0}]={1}", key, columns[key].ValueQuery)));

            try
            {
                SqlDataReader reader = Query(string.Format("UPDATE [{0}] SET {1} WHERE {2}", table, values, where));
                if (reader != null && reader.RecordsAffected == 1)
                {
                    reader.Close();
                    return true;
                }
            }
            catch(Exception e)
            {
                throw new Exception("orm_update", e);
            }
            return false;
        }

        private bool Insert()
        {
            List<string> ignored = new List<string>(keys);
            ignored.AddRange(computed);

            string values = string.Join(", ", columns.Keys.Except(ignored).Select(key => columns[key].ValueQuery));
            //string values = string.Join(", ", columns.Keys.Except(ignored).Select(key => string.Format("@{0}", key)));
            string cols = string.Join(", ", columns.Keys.Except(ignored).Select(key => string.Format("[{0}]", key)));
            string output = insertedData ? "OUTPUT INSERTED." + string.Join(", INSERTED.", columns.Keys.Except(computed).Select(key => string.Format("[{0}]", key))) : "";

            Dictionary<string, object> Params = new Dictionary<string, object>();
            foreach(string key in columns.Keys.Except(ignored))
            {
                Params.Add("@" + key, columns[key].ValueQuery);
            }

            try
            {
                SqlDataReader reader = Query(string.Format("INSERT INTO [{0}] ({1}) {3} VALUES ({2})", table, cols, values, output), Params);
                if (reader != null && reader.HasRows ) //&& reader.RecordsAffected == 1)
                {
                    string name;
                    reader.Read();
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        name = reader.GetName(i);
                        columns[name].ValueDB = reader.GetValue(i);
                    }

                    if (!reader.IsClosed)
                    {
                        reader.Close();
                    }
                    empty = false;
                    return true;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                if(e.InnerException != null)
                    throw new Exception("orm_insert", e.InnerException);
                throw new Exception("orm_insert", e);
            }
            return false;
        }

        public bool Delete()
        {
            string where = "";
            foreach (string key in keys)
            {
                where += " [" + key + "]=" + columns[key].ValueQuery + " AND";
            }
            where = where.Substring(0, where.Length - 4);

            try
            {
                SqlDataReader reader = Query(string.Format("DELETE FROM [{0}] WHERE {1}", table, where));
                if (reader != null && reader.RecordsAffected == 1)
                {
                    reader.Close();
                    return true;
                }
                reader.Close();
            }
            catch (Exception e)
            {
                throw new Exception("orm_delete", e);
            }
            return false;
        }

        public void DropKeys()
        {
            keys.Clear();
        }

        public void IgnoreInsertedData()
        {
            insertedData = false;
        }

        public bool Reload()
        {
            string where = string.Join(" AND ", keys.Select(key => string.Format("[{0}]={1}", key, columns[key].ValueQuery)));

            //obtener columnas y valores desde una consulta where keys
            DataTable dt = Load(string.Format("SELECT * FROM [{0}] WITH (NOLOCK) WHERE {1}", table, where));
            if (dt.Rows.Count > 0)
            {
                foreach (DataColumn col in dt.Columns)
                {
                    if (!columns.ContainsKey(col.ColumnName))
                    {
                        throw new Exception("orm_hydrate_column");
                    }

                    columns[col.ColumnName].ValueDB = dt.Rows[0][col];
                }
                empty = false;
                return true;
            }

            return false;
        }

        public bool DuplicityCheck(List<string> unique_cols)
        {
            string where = string.Join(" AND ", unique_cols.Select(key => string.Format("[{0}]={1}", key, columns[key].ValueQuery)));
            string query_where = (!where.Equals("") ? "WHERE (" + where + ")" : "");

            if (!empty)
            {
                string excluded = string.Join(" AND ", keys.Select(key => string.Format("[{0}]={1}", key, columns[key].ValueQuery)));
                query_where = (query_where.Equals("") ? "WHERE NOT (" + excluded + ")" : query_where + " AND NOT (" + excluded + ")");
            }

            string keys_select = string.Join(", ", keys);
            string query = string.Format("SELECT {0} FROM [{1}] {2}", keys_select, table, query_where);

            try
            {
                DataTable dt = Load(query);
                return (dt.Rows.Count > 0);
            }
            catch (Exception e)
            {
                throw new Exception("orm_duplicity_check", e);
            }
        }
    }
}
