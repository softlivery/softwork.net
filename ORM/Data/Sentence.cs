﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    class Sentence
    {
        string type;
        string value;
        List<object> list;
        public Sentence(string type, string value, List<object> list)
        {
            this.type = type;
            this.value = value;
            this.list = list;
        }

        public Sentence(string type, string value)
        {
            this.type = type;
            this.value = value;
        }

        public Sentence(string type, List<object> list)
        {
            this.type = type;
            this.list = list;
        }

        public override string ToString()
        {
            string sentence = "";
            switch (type)
            {
                case "report":
                    sentence = string.Join("\n ", list);
                    break;
                case "from":
                    sentence = "FROM [" + value + "]";
                    break;
                case "orderby":
                    sentence = "ORDER BY " + string.Join(", ", list);
                    break;
                case "where":
                    sentence = "WHERE " + string.Join(value, list);
                    break;
                case "union":
                    sentence = string.Join("UNION ", list);
                    break;
                case "table":
                    sentence = "[" + value + "]";
                    break;
                case "value":
                default:
                    sentence = value;
                    break;
            }
            return sentence;
        }
    }
}
