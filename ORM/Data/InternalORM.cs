﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    public class InternalORM : ORM
    {
        public InternalORM(Manager Manager, string table) : base(Manager, "VGF_" + table)
        {
        }

        public InternalORM(Manager Manager, string table, bool Hydrate = true) : base(Manager, "VGF_" + table, Hydrate)
        {
        }
    }
}
