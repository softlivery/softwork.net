﻿using System.Collections.Generic;

namespace VGFramework.Data
{
    public class Join
    {
        //va a definir los joins, debe tener como parametro: tabla2, tabla2.columna , tabla1.columna , tipo de join(definidos como enum).
        private Report.JoinType type;
        private string joinTable;
        private string joinColumn;
        private string otherTable;
        private string otherColumn;
        private string alias;
        private string on = "";

        public Join(List<object> list, Report.JoinType type)
        {
            this.type = type;
            foreach (object item in list)
            {
                switch (item.GetType().Name)
                {
                    case "String":
                    case "Sentence":
                        joinTable = item.ToString();
                        break;
                    case "Field":
                        if (on.Equals(""))
                            on = item.ToString();
                        else
                            on += " = " + item.ToString();
                        break;
                    case "Filter":
                    case "FilterGroup":
                        on = item.ToString();
                        break;
                    default:
                        break;
                }
            }
        }
        
        public Join(string joinTable, string joinColumn, string tableColumn, Report.JoinType joinType, string alias = "")
        {
            this.joinTable = ORM.ParseColumnName(joinTable);
            this.joinColumn = ORM.ParseColumnName(joinColumn);
            string j = ORM.ParseColumnName(tableColumn);
            otherTable = j.Split('.')[0];
            otherColumn = j.Split('.')[1];
            type = joinType;
            this.alias = alias;
        }

        public Join(Report query, string joinColumn, string tableColumn, Report.JoinType joinType, string alias)
        {
            joinTable = "(" + query.ToString() + ") ";
            this.joinColumn = ORM.ParseColumnName(joinColumn);
            string j = ORM.ParseColumnName(tableColumn);
            otherTable = j.Split('.')[0];
            otherColumn = j.Split('.')[1];
            type = joinType;
            this.alias = alias;
        }
 
        public override string ToString()
        {
            string result = "";
            string fromTable = alias.Equals("") ? joinTable : alias;

            if (on.Equals(""))
                on = fromTable + "." + joinColumn + " = " + otherTable + "." + otherColumn;

            switch (type)
            {
                case Report.JoinType.INNER:
                    result += "INNER JOIN " + joinTable + " " + alias + " ON " + on/*fromTable + "." + joinColumn + " = " + otherTable + "." + otherColumn*/;
                    break;
                case Report.JoinType.LEFT:
                    result += "LEFT JOIN " + joinTable + " " + alias + " ON " + on/*fromTable + "." + joinColumn + " = " + otherTable + "." + otherColumn*/;
                    break;
                case Report.JoinType.RIGHT:
                    result += "RIGHT JOIN " + joinTable + " " + alias + " ON " + fromTable + "." + on/*joinColumn + " = " + otherTable + "." + otherColumn*/;
                    break;
                case Report.JoinType.CROSS:
                    result += "CROSS JOIN " + joinTable + " " + alias + " ON " + fromTable + "." + on/*joinColumn + " = " + otherTable + "." + otherColumn*/;
                    break;
            }
            return result;
        }
    }
}
