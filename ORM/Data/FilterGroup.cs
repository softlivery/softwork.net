﻿using System;
using System.Collections.Generic;

namespace VGFramework.Data
{
    public class FilterGroup
    {
        private List<object> filters;
        private Report.Connector connector;

        public FilterGroup(Report.Connector connector = Report.Connector.NONE)
        {
            this.filters = new List<object>();
            this.connector = connector;
        }

        public FilterGroup(List<object> list, Report.Connector connector = Report.Connector.NONE)
        {
            filters = list;
            this.connector = connector;
        }

        public FilterGroup add(string column, Report.FilterType type, int value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(column, type, value.ToString(), connector));
            return this;
        }

        public FilterGroup add(string column, Report.FilterType type, double value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(column, type, value.ToString(), connector));
            return this;
        }
        public FilterGroup add(string column, Report.FilterType type, string value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(column, type, value, connector));
            return this;
        }
        public FilterGroup add(IStatement column, Report.FilterType type, int value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(column, type, value, connector));
            return this;
        }
        public FilterGroup add(IStatement column, Report.FilterType type, double value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(column, type, value.ToString(), connector));
            return this;
        }
        public FilterGroup add(IStatement column, Report.FilterType type, string value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(column, type, value, connector));
            return this;
        }
        public FilterGroup add(string column1, string column2, Report.OperationType operation, Report.FilterType type, string value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(new Operation(column1, column2, operation), type, value, connector));
            return this;
        }
        public FilterGroup add(string column1, string column2, Report.OperationType operation, Report.FilterType type, int value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(new Operation(column1, column2, operation), type, value, connector));
            return this;
        }
        public FilterGroup add(string column1, string column2, Report.OperationType operation, Report.FilterType type, double value, Report.Connector connector = Report.Connector.NONE)
        {
            this.filters.Add(new Filter(new Operation(column1, column2, operation), type, value, connector));
            return this;
        }
        public FilterGroup add(object filter)
        {
            this.filters.Add(filter);
            return this;
        }

        public override string ToString()
        {
            string result = "\n(" + string.Join("", this.filters) + "\n)";
            if (this.connector != Report.Connector.NONE)
                result += " " + this.connector.ToString() + " ";
            return result;
        }
    }
}
