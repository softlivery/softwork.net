﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;

namespace VGFramework.Data
{
    public class Column : IStatement
    {
        private string name;
        private string type;
        private object @default;
        private object value;
        private bool modified = false;
        private bool nullable;
        private bool computed;
        private int length;

        private string[] strings = { "char", "nchar", "varchar", "nvarchar" };
        
        public Column(string Name)
        {
            name = Name;
        }

        public Column()
        {
        }

        public Column(string name, string type, object @default, bool nullable, bool computed, int length)
        {
            this.name = name;
            this.type = type;
            this.@default = @default;
            this.value = @default;
            this.nullable = nullable;
            this.computed = computed;
            this.length = length;
        }

        public override string ToString()
        {
            return ORM.ParseColumnName(this.Name);
        }

        public string Name => name;
        public string Type => type;
        public bool Modified { get => modified; }
        public bool Computed { get => computed; }

        public object ValueDB
        {
            get { return value; }
            set { this.value = value; }
        }
        public object Value
        {
            get { return ValueFromDB(type, value); }
            set
            {
                this.modified = true;
                this.value = ValueToDB(type, value);
            }
        }
        public object ValueQuery
        {
            get
            {
                if (value == DBNull.Value)
                {
                    return "NULL";
                }

                switch (value.GetType().Name.ToLower())
                {
                    case "bool":
                    case "boolean":
                        return value.ToString().Equals("TRUE") ? "1" : "0";
                    case "byte[]":
                        //return (byte[])value;
                        //return Encoding.ASCII.GetString((byte[])value);
                        return "'" + Encoding.Default.GetString((byte[])value) + "'";
                    case "string":
                        return "'" + value.ToString().Replace("\'", "\'\'") + "'";
                    case "datetime":
                    case "date":
                        if (value.ToString().Equals(""))
                            return "NULL";
                        return "'" + ((DateTime) value).ToString("MM/dd/yyyy HH:mm:ss") + "'";
                    case "double":
                    case "decimal":
                        if (Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator.Equals(","))
                        {
                            return value.ToString().Replace(',', '.');
                        }
                        return value.ToString();
                    default:
                        return value.ToString();
                }
            }
        }

        /*internal static Column FromReader(SqlDataReader reader)
        {
            string name = reader.GetString(3);
            string type = reader.GetString(7);
            object @default = ValueFromDB(type, reader.GetValue(5));
            bool nullable = reader.GetString(6).Equals("YES") ? true : false;
            bool computed = reader.GetInt16(23) > 0 ? true : false;
            int length = 0;

            object tmp = reader.GetValue(8);
            if(tmp != DBNull.Value && !int.TryParse(tmp.ToString(), out length))
            {
                throw new Exception("column_fromreader_length");
            }

            return new Column() {
                name = name,
                type = type,
                @default = @default,
                value = @default,
                nullable = nullable,
                length = length,
                computed = computed
            };
        }*/

        public static object ValueFromDB(string type, object value)
        {
            try
            {
                if (value == DBNull.Value)
                {
                    return null;
                }

                string val = DropBrackets(value);
                switch (type.ToLower())
                {
                    case "datetime":
                        if (val.Equals("getdate"))
                        {
                            return DateTime.Now.ToString(WebManager.Instance.GetConfig("DateTimeFormat"));
                        }
                        return ((DateTime)value).ToString(WebManager.Instance.GetConfig("DateTimeFormat"));
                    case "date":
                        if (val.Equals("getdate"))
                        {
                            return DateTime.Now.ToString(WebManager.Instance.GetConfig("DateFormat"));
                        }
                        return ((DateTime)value).ToString(WebManager.Instance.GetConfig("DateFormat"));
                    case "bit":
                        return val.Equals("1");
                    case "bigint":
                        long tmp1;
                        return long.TryParse(val, out tmp1) ? tmp1 : 0;
                    case "int":
                        int tmp2;
                        return int.TryParse(val, out tmp2) ? tmp2 : 0;
                    case "money":
                    case "decimal":
                        double tmp3;
                        return double.TryParse(val, out tmp3) ? tmp3 : 0;
                    case "varbinary":
                        return (byte[])value;
                    case "char":
                    case "nchar":
                    case "varchar":
                    case "nvarchar":
                    default:
                        return val;
                }
            }
            catch (Exception e)
            {
                throw new Exception("column_value_get", e);
            }
        }

        public static object ValueToDB(string type, object value)
        {
            try
            {
                if (value == null)
                {
                    return DBNull.Value;
                }
                else
                {
                    switch (type.ToLower())
                    {
                        case "datetime":
                            if (value.ToString().Equals(""))
                                return DBNull.Value;
                            return DateTime.ParseExact(value.ToString(), WebManager.Instance.GetConfig("DateTimeFormat"), null);
                        case "date":
                            if (value.ToString().Equals(""))
                                return DBNull.Value;
                            return DateTime.ParseExact(value.ToString(), WebManager.Instance.GetConfig("DateFormat"), null);
                        case "bigint":
                            if (value.ToString().Equals(""))
                                return DBNull.Value;
                            return long.Parse(value.ToString());
                        case "int":
                            if (value.ToString().Equals(""))
                                return DBNull.Value;
                            return int.Parse(value.ToString());
                        case "money":
                        case "decimal":
                            if (value.ToString().Equals(""))
                                return DBNull.Value;
                            return double.Parse(value.ToString().Replace(".", "").Replace(',', '.'));
                        case "varbinary":
                            return (byte[])value;
                            //string test = Encoding.ASCII.GetString((byte[])value);
                            //return Encoding.ASCII.GetBytes(test);
                        case "char":
                        case "nchar":
                        case "varchar":
                        case "nvarchar":
                        default:
                            return value;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("column_value_get", e);
            }
        }

        private static string DropBrackets(object value)
        {
            char[] chars = { '(', ')', ' ', '\'' };
            return value.ToString().Trim(chars);
        }

        public string GetString()
        {
            if (strings.Contains(type))
            {
                return value != null ? value.ToString() : null;
            }
            throw new Exception("column_getstring_type");
        }

        internal static Column From(DataRow row)
        {
            string name = row[3].ToString();
            string type = row[7].ToString();
            object @default = ValueToDB(type, ValueFromDB(type, row[5]));
            bool nullable = row[6].ToString().Equals("YES") ? true : false;
            bool computed = (bool)row[23];
            int length = 0;

            object tmp = row[8];
            if (tmp != DBNull.Value && !int.TryParse(tmp.ToString(), out length))
            {
                throw new Exception("column_fromreader_length");
            }

            return new Column()
            {
                name = name,
                type = type,
                @default = @default,
                value = @default,
                nullable = nullable,
                length = length,
                computed = computed
            };
        }
    }
}
