﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Data
{
    class Group
    {
        private List<object> groups;

        public Group(List<object> list)
        {
            groups = list;
        }

        public override string ToString()
        {
            return string.Join(", ", this.groups);
        }
    }
}
