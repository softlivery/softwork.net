﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using VGFramework.Helpers;

namespace VGFramework.Data
{
    public class ReportTree : Report
    {
        public int paginate = -1;
        Manager Manager;
        private SortedDictionary<long, ReportTreeNode> nodes = new SortedDictionary<long, ReportTreeNode>();
        public ReportTree(Manager Manager, string table, string userLevel) : base(Manager, table)
        {
            this.Manager = Manager;
            GetNodes(table, userLevel);
        }

        private void GetNodes(string rootName, string userLevel)
        {
            ReportTreeNode node;
            Procedure procedure = new Procedure(Manager, "GetReport");
            procedure.AddParameter(rootName);
            procedure.AddParameter(userLevel);
            DataSet ds = procedure.Execute();
            if (ds.Tables[0].Rows.Count == 0)
                throw new Exception("Report not found");

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                long id = long.Parse(ds.Tables[0].Rows[i]["Id"].ToString());
                string type = ds.Tables[0].Rows[i]["Type"].ToString().ToLower().Trim(' ');
                long parentId = long.Parse(ds.Tables[0].Rows[i]["Parent"].ToString());
                string value = ds.Tables[0].Rows[i]["Value"].ToString();
                string alias = ds.Tables[0].Rows[i]["Alias"].ToString();
                string order = ds.Tables[0].Rows[i]["Order"].ToString().ToUpper();
                string operation = ds.Tables[0].Rows[i]["Operator"].ToString().ToUpper();
                string connector = ds.Tables[0].Rows[i]["Connector"].ToString().ToUpper();
                bool hidden = false;
                if (ds.Tables[0].Rows[i][9].ToString() != null && ds.Tables[0].Rows[i]["Hidden"].ToString() != "")
                    hidden = bool.Parse(ds.Tables[0].Rows[i][9].ToString());

                if (type.Equals("from"))
                    table = value;
                if (parentId == 0)
                {
                    node = new ReportTreeNode(id, type, value, alias, order, operation, connector);
                    nodes.Add(id, node);
                    continue;
                }
                if (type != "paginate")
                {
                    ReportTreeNode parent = nodes[parentId];
                    node = new ReportTreeNode(id, type, value, alias, order, operation, connector, parent);
                    nodes.Add(id, node);
                    node.AddAsAChild();
                    continue;
                }
                paginate = int.Parse(value);
            }
        }

        private string Process(ReportTreeNode node)
        {
            string query = "";
            object processNode = null;

            if (node.HasChilds())
            {
                for (int i = 0; i < node.GetChilds().Count; i++)
                {
                    Process(node.GetChilds()[i]);
                }
            }
            switch (node.type)
            {
                case "report":
                    processNode = new Sentence(node.type, node.processChilds);
                    break;
                case "select":
                    processNode = new Select(node.processChilds);
                    break;
                case "from":
                    processNode = new Sentence(node.type, node.value);
                    break;
                case "column":
                    processNode = new Field(node.value, node.alias);
                    break;
                case "value":
                    processNode = new Sentence(node.type, node.value);
                    break;
                case "operation":
                    processNode = new Operation(node.processChilds, node.operationType, node.alias);
                    break;
                case "filter":
                    processNode = new Filter(node.processChilds, node.filterType, node.connectorType);
                    break;
                case "filtergroup":
                    processNode = new FilterGroup(node.processChilds, node.connectorType);
                    break;
                case "condition":
                    processNode = new Condition(node.processChilds);
                    break;
                case "when":
                    processNode = new Condition(node.processChilds, node.type);
                    break;
                case "then":
                    processNode = new Condition(node.processChilds, node.type, node.value);
                    break;
                case "else":
                    processNode = new Condition(node.processChilds, node.type, node.value);
                    break;
                case "function":
                    processNode = new Function(node.alias, node.processChilds);
                    break;
                case "case":
                    processNode = new Case(node.processChilds, node.alias);
                    break;
                case "orderby":
                    processNode = new Sentence(node.type, node.processChilds);
                    break;
                case "order":
                    processNode = new Order(node.value, node.orderType);
                    break;
                case "groupby":
                    processNode = new Group(node.processChilds);
                    break;
                case "join":
                    processNode = new Join(node.processChilds, node.joinType);
                    break;
                case "where":
                    processNode = new Sentence(node.type, node.connector, node.processChilds);
                    break;
                case "union":
                    processNode = new Sentence(node.type, node.processChilds);
                    break;
                case "table":
                    processNode = new Sentence(node.type, node.value);
                    break;
            }
            query += processNode.ToString();
            if (node.type != "report")
                nodes[node.parent.id].AddProcessChild(processNode);

            return query;
        }

        public override string ToString()
        {
            return Process(nodes[nodes.First().Key]);
        }

        public override void Order(string values)
        {
            ReportTreeNode order = null;
            foreach (ReportTreeNode node in nodes.Values)
                if (node.type == "orderby") { order = node; break; }

            if (order == null)
            {
                order = new ReportTreeNode(nodes.Keys.Last<long>() + 1, "orderby", null, null, null, null, null, nodes[nodes.Keys.First<long>()]);
                order.AddAsAChild();
                nodes.Add(nodes.Keys.Last<long>() + 1, order);
            }

            order.GetChilds().Clear();
            ReportTreeNode newNode;
            foreach (string value in values.Split(','))
            {
                newNode = new ReportTreeNode(nodes.Keys.Last<long>() + 1, "order", value.Split('.')[0], null, value.Split('.')[1], null, null, order);
                newNode.AddAsAChild();
                nodes.Add(nodes.Keys.Last<long>() + 1, newNode);
            }
        }

        public override void Filter(string column, Report.FilterType type, string value)
        {
            ReportTreeNode where = null;
            foreach (ReportTreeNode node in nodes.Values)
                if (node.type == "where") { where = node; where.connector = "AND"; break; }

            if (where == null)
            {
                bool join = false;
                where = new ReportTreeNode(nodes.Keys.Last<long>() + 1, "where", null, null, null, null, "AND", nodes[nodes.Keys.First<long>()]);
                foreach (ReportTreeNode node in nodes.Values)
                    if (node.type == "join") { join = true; break; }

                if (join)
                    where.AddAsAChild(3);
                else
                    where.AddAsAChild(2);

                nodes.Add(nodes.Keys.Last<long>() + 1, where);
            }

            ReportTreeNode filter = new ReportTreeNode(nodes.Keys.Last<long>() + 1, "filter", null, null, null, type.ToString(), null, where);
            filter.AddAsAChild();
            nodes.Add(nodes.Keys.Last<long>() + 1, filter);

            List<string> values = value.Split(',').ToList();

            ReportTreeNode columnNode = new ReportTreeNode(nodes.Keys.Last<long>() + 1, "column", column, null, null, null, null, filter);
            columnNode.AddAsAChild();

            if (type.ToString() != "ISNULL" && type.ToString() != "ISNOTNULL")
            {
                foreach (string val in values)
                {
                    ReportTreeNode valueNode = new ReportTreeNode(nodes.Keys.Last<long>() + 1, "value", "'" + val + "'", null, null, null, null, filter);
                    valueNode.AddAsAChild();
                    nodes.Add(nodes.Keys.Last<long>() + 1, valueNode);
                }
            }
        }

        protected override string Select()
        {
            string select = "";
            foreach (KeyValuePair<long, ReportTreeNode> node in nodes)
            {
                if (node.Value.type.ToLower() == "select")
                {
                    Process(node.Value);
                    select = string.Join(", ", node.Value.processChilds);
                    break;
                }
            }
            return select;
        }

        protected override string Filter()
        {
            string where = "";
            foreach (KeyValuePair<long, ReportTreeNode> node in nodes)
            {
                if (node.Value.type.ToLower() == "where")
                {
                    Process(node.Value);
                    where = string.Join(" " + node.Value.connector + " ", node.Value.processChilds);
                    break;
                }
            }
            return where;
        }

        protected override string Order()
        {
            string order = "";
            foreach (KeyValuePair<long, ReportTreeNode> node in nodes)
            {
                if (node.Value.type.ToLower() == "orderby")
                {
                    Process(node.Value);
                    order = string.Join(", ", node.Value.processChilds);
                    break;
                }
            }
            return order;
        }

        protected override string Related()
        {
            string join = "";
            foreach (KeyValuePair<long, ReportTreeNode> node in nodes)
            {
                if (node.Value.type.ToLower() == "join")
                {
                    join = Process(node.Value);
                    break;
                }
            }
            return join;
        }
    }
}
