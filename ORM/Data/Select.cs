﻿using System;
using System.Collections.Generic;

namespace VGFramework.Data
{
    class Select
    {
        private List<object> fields = new List<object>();
        private string field;
        private string alias = null;
        private string calc = null;

        public Select(List<object> list)
        {
            fields = list;
        }

        public Select(string field, string alias = null, string calc = null)
        {
            this.field = new Column(field).ToString();

            if (alias != null)
            {
                this.alias = alias;
            }

            if (calc != null)
            {
                this.calc = calc;
            }
        }

        public Select(IStatement field, string alias = null, string calc = null)
        {
            
            this.field = field.ToString();

            if (alias != null)
            {
                this.alias = alias;
            }

            if (calc != null)
            {
                this.calc = calc;
            }
        }

        public override string ToString()
        {
            //string field = this.field;

            //if (this.calc != null)
            //    field += this.calc;

            //if (this.alias != null)
            //    field += string.Format(" AS \"{0}\"", this.alias);

            //return field;

            if (!(fields.Count > 0))
            {
                string field = this.field;

                if (this.calc != null)
                    field += this.calc;

                if (this.alias != null)
                    field += string.Format(" AS \"{0}\"", this.alias);

                return field;
            }
            return "SELECT " + string.Join(", ", fields);
        }
    }
}
