﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VGFramework.Data;

namespace VGFramework.Helpers
{
    class ReportTreeNode
    {
        private List<ReportTreeNode> childrens = new List<ReportTreeNode>();
        public ReportTreeNode(long id, string type, string value = null, string alias = null, string order = null, string operation = null, string connector = null, ReportTreeNode parent = null)
        {
            this.id = id;
            this.parent = parent;
            this.type = type;
            this.value = value;
            this.alias = alias;
            this.connector = connector;
            switch (operation)
            {
                case "LESS":
                    filterType = Report.FilterType.LESS;
                    break;
                case "LESS_EQUAL":
                    filterType = Report.FilterType.LESS_EQUAL;
                    break;
                case "GREATER":
                    filterType = Report.FilterType.GREATER;
                    break;
                case "GREATER_EQUAL":
                    filterType = Report.FilterType.GREATER_EQUAL;
                    break;
                case "REGEXP":
                    filterType = Report.FilterType.REGEXP;
                    break;
                case "POSTFIX":
                    filterType = Report.FilterType.POSTFIX;
                    break;
                case "PREFIX":
                    filterType = Report.FilterType.PREFIX;
                    break;
                case "LIKE":
                    filterType = Report.FilterType.LIKE;
                    break;
                case "NOT_LIKE":
                    filterType = Report.FilterType.NOT_LIKE;
                    break;
                case "BEETWEN":
                    filterType = Report.FilterType.BETWEEN;
                    break;
                case "IN":
                    filterType = Report.FilterType.IN;
                    break;
                case "NOT_IN":
                    filterType = Report.FilterType.NOT_IN;
                    break;
                case "ISNULL":
                    filterType = Report.FilterType.ISNULL;
                    break;
                case "ISNOTNULL":
                    filterType = Report.FilterType.ISNOTNULL;
                    break;
                case "NOT_EQUAL":
                    filterType = Report.FilterType.NOT_EQUAL;
                    break;
                case "EQUAL":
                    filterType = Report.FilterType.EQUAL;
                    break;
                case "SUM":
                    operationType = Report.OperationType.SUM;
                    break;
                case "SUBSTRACTION":
                    operationType = Report.OperationType.SUBTRACTION;
                    break;
                case "INNER":
                    joinType = Report.JoinType.INNER;
                    break;
                case "LEFT":
                    joinType = Report.JoinType.LEFT;
                    break;
                case "RIGTH":
                    joinType = Report.JoinType.RIGHT;
                    break;
                case "CROSS":
                    joinType = Report.JoinType.CROSS;
                    break;
                default:
                    filterType = Report.FilterType.EQUAL;
                    break;
            }
            switch (connector)
            {
                case "AND":
                    connectorType = Report.Connector.AND;
                    break;
                case "OR":
                    connectorType = Report.Connector.OR;
                    break;
                default:
                    connectorType = Report.Connector.NONE;
                    break;
            }
            switch (order)
            {
                case "DESC":
                    orderType = Report.OrderType.DESC;
                    break;
                case "ASC":
                default:
                    orderType = Report.OrderType.ASC;
                    break;

            }
            processChilds = new List<object>();
        }

        public ReportTreeNode parent { get; set; }
        public long id { get; set; }
        public string type { get; set; }
        public string value { get; set; }
        public string alias { get; set; }
        public string connector { get; set; }
        public Report.FilterType filterType { get; set; }
        public Report.OperationType operationType { get; set; }
        public Report.Connector connectorType { get; set; }
        public Report.JoinType joinType { get; set; }
        public Report.OrderType orderType { get; set; }
        public List<object> processChilds { get; set; }

        public bool HasChilds()
        {
            return (childrens.Count > 0);
        }

        public void AddAsAChild()
        {
            parent.childrens.Add(this);
        }

        public void AddAsAChild(int position)
        {
            parent.childrens.Insert(position, this);
        }

        public List<ReportTreeNode> GetChilds()
        {
            return childrens;
        }

        public void AddProcessChild(object processChild)
        {
            processChilds.Add(processChild);
        }
    }
}