﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using VGFramework.Data;

namespace VGFramework.Helpers
{
    public class Makelist
    {
        public static void MakeForm(OAuthManager Manager)
        {
            string formName = Manager.Parameters.Get("name", "").ToString();

            string endpoint = "";
            List<Dictionary<string, object>> sections = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();

            GetForm(Manager, formName, ref endpoint, sections, items);
            Manager.Response.Set("sections", sections);
            Manager.Response.Set("items", items);
            Manager.Response.Set("endpoint", endpoint);
            Manager.Response.Set("method", "post");
        }

        public static void GetForm(OAuthManager Manager, string formName, ref string endpoint, List<Dictionary<string, object>> sections, List<Dictionary<string, object>> items)
        {
            long userLevel = Manager.User.Level;
            Dictionary<long, MakeListNode> nodes = new Dictionary<long, MakeListNode>();
            List<MakeListNode> aux = new List<MakeListNode>();

            MakeListNode node;
            Procedure procedure = new Procedure(Manager, "VGF_MakelistStructure");
            procedure.AddParameter(formName);
            procedure.AddParameter(userLevel.ToString());
            DataSet ds = procedure.Execute();

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                long id = long.Parse(ds.Tables[0].Rows[i]["Id"].ToString());
                string type = ds.Tables[0].Rows[i]["Type"].ToString().ToLower().Trim(' ');
                long parentId = long.Parse(ds.Tables[0].Rows[i]["Parent"].ToString());
                string value = ds.Tables[0].Rows[i]["Value"].ToString();
                string alias = ds.Tables[0].Rows[i]["Alias"].ToString();
                string order = ds.Tables[0].Rows[i]["Order"].ToString().ToUpper();
                string label = ds.Tables[0].Rows[i]["Label"].ToString();
                string placeholder = ds.Tables[0].Rows[i]["Placeholder"].ToString();
                string mask = ds.Tables[0].Rows[i]["Mask"].ToString();
                bool hidden = false;
                if (ds.Tables[0].Rows[i]["Hidden"].ToString() != null && ds.Tables[0].Rows[i]["Hidden"].ToString() != "")
                    hidden = bool.Parse(ds.Tables[0].Rows[i]["Hidden"].ToString());
                bool required = false;
                if (ds.Tables[0].Rows[i]["Required"].ToString() != null && ds.Tables[0].Rows[i]["Required"].ToString() != "")
                    required = bool.Parse(ds.Tables[0].Rows[i]["Required"].ToString());
                bool editable = true;
                if (ds.Tables[0].Rows[i]["Editable"].ToString() != null && ds.Tables[0].Rows[i]["Editable"].ToString() != "")
                    editable = bool.Parse(ds.Tables[0].Rows[i]["Editable"].ToString());

                if (type == "list")
                    throw new Exception("No corresponde a nombre de formulario");
                if (type == "form")
                    node = new MakeListNode(id.ToString(), type, parentId.ToString(), value, alias, order);
                else
                    node = new MakeListNode(id.ToString(), type, parentId.ToString(), value, alias, order, nodes[parentId], hidden, required, editable, label, placeholder, mask);

                switch (type)
                {
                    case "form":
                        nodes.Add(id, node);
                        break;
                    case "endpoint":
                        endpoint = value;
                        break;
                    case "icon":
                        nodes[parentId].alias = value;
                        break;
                    case "button":
                        if (!value.Equals(""))
                        {
                            aux.Add(node);
                            nodes.Add(id, node);
                        }
                        break;
                    case "section":
                    case "number":
                    case "text":
                    case "textarea":
                    case "password":
                    case "select":
                    case "date":
                    case "datetime":
                    case "time":
                    case "file":
                        aux.Add(node);
                        nodes.Add(id, node);
                        break;
                    default:
                        break;
                }
            }

            foreach (MakeListNode component in aux)
            {
                switch (component.type)
                {
                    case "section":
                        sections.Add(component.Get());
                        break;
                    default:
                        items.Add(component.Get());
                        break;
                }
            }
            return;
        }

        public static void MakeList(OAuthManager Manager)
        {
            long userLevel = Manager.User.Level;
            string listName = Manager.Parameters.Get("name", "").ToString();

            Dictionary<long, MakeListNode> nodes = new Dictionary<long, MakeListNode>();
            List<Dictionary<string, object>> buttons = new List<Dictionary<string, object>>();
            List<Dictionary<string, object>> columns = new List<Dictionary<string, object>>();
            List<MakeListNode> aux = new List<MakeListNode>();
            string endpoint = "";

            MakeListNode node;
            Procedure procedure = new Procedure(Manager, "VGF_MakelistStructure");
            procedure.AddParameter(listName);
            procedure.AddParameter(userLevel.ToString());
            DataSet ds = procedure.Execute();

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                long id = long.Parse(ds.Tables[0].Rows[i][0].ToString());
                string type = ds.Tables[0].Rows[i][1].ToString().ToLower().Trim(' ');
                long parentId = long.Parse(ds.Tables[0].Rows[i][2].ToString());
                string value = ds.Tables[0].Rows[i][3].ToString();
                string alias = ds.Tables[0].Rows[i][4].ToString();
                string order = ds.Tables[0].Rows[i][5].ToString().ToUpper();
                string operation = ds.Tables[0].Rows[i][7].ToString().ToUpper();
                string connector = ds.Tables[0].Rows[i][8].ToString().ToUpper();
                bool hidden = false;
                if (ds.Tables[0].Rows[i][9].ToString() != null && ds.Tables[0].Rows[i][9].ToString() != "")
                    hidden = bool.Parse(ds.Tables[0].Rows[i][9].ToString());

                if (type == "form")
                    throw new Exception("No corresponde a nombre de listado");
                if (type == "list")
                    node = new MakeListNode(id.ToString(), type, parentId.ToString(), value, alias.ToLower(), order);
                else
                    node = new MakeListNode(id.ToString(), type, parentId.ToString(), value, alias.ToLower(), order, nodes[parentId], hidden);

                switch (type)
                {
                    case "list":
                        nodes.Add(id, node);
                        break;
                    case "endpoint":
                        endpoint = value;
                        break;
                    case "icon":
                        nodes[parentId].alias = value;
                        break;
                    case "column":
                    case "button":
                        aux.Add(node);
                        nodes.Add(id, node);
                        break;
                    default:
                        break;
                }
            }
            foreach (MakeListNode component in aux)
            {
                switch (component.type)
                {
                    case "button":
                        buttons.Add(component.Get());
                        break;
                    case "column":
                        columns.Add(component.Get());
                        break;
                }
            }
            Manager.Response.Set("columns", columns);
            Manager.Response.Set("buttons", buttons);
            Manager.Response.Set("endpoint", endpoint);
            Manager.Response.Set("method", "get");
        }

        private static object FromDB(DataRow row) //, string[] columns)
        {
            //return row.Table.Columns.Cast<DataColumn>().ToDictionary(c => c.ColumnName, c => row[c]);
            object[] Item = row.ItemArray;
            Dictionary<string, object> obj = new Dictionary<string, object>();
            //for (int i = 0; i < columns.Length; i++)
            for (int i = 0; i < row.Table.Columns.Count; i++)
                obj[row.Table.Columns[i].ColumnName] = Item[i];
                //obj[columns[i]] = Item[i];
            return obj;
        }
        
        public static void List(WebManager Manager, string table, string userLevel) //, string[] columns)
        {
            int paginate = 4000; 
            if (Manager.QueryParameters.All().Keys.Contains("paginate"))
            {
                if (!int.TryParse(Manager.QueryParameters.All()["paginate"], out paginate))
                    throw new Exception("Must be numeric value");
                Manager.QueryParameters.Remove("paginate");
            }

            int page = 1;
            if (Manager.QueryParameters.All().Keys.Contains("page"))
            {
                if (!int.TryParse(Manager.QueryParameters.All()["page"], out page))
                    throw new Exception("Must be numeric value");
                Manager.QueryParameters.Remove("page");
            }
            
            Report db = new Report(Manager, table, true);
            try
            {
                db = new ReportTree(Manager, table, userLevel);
            }
            catch (Exception e)
            {
                //Manager.Response.Set("error", e.Message);
            }

            if (Manager.QueryParameters.All().Count > 0)
                foreach (KeyValuePair<string, string> filter in Manager.QueryParameters.All())
                {
                    string type = filter.Key;
                    string value = filter.Value;
                    switch (type.ToUpper())
                    {
                        case "SORT":
                            db.Order(value);
                            break;
                        default:
                            string oper = "EQUAL";
                            string column = type;
                            if (type.Split('.').Count() > 1)
                            {
                                oper = type.Split('.')[0].ToUpper();
                                column = type.Split('.')[1];
                            }
                            db.Filter(column, (Report.FilterType)Enum.Parse(typeof(Report.FilterType), oper), value);
                            break;
                    }
                }
            DataSet results;
            if (paginate < 1)
                results = db.Execute();
            else
                results = db.Paginate(page, paginate);

            if (results != null)
            {
                object[] objects = Array.ConvertAll(results.Tables[0].AsEnumerable().ToArray(), (p => FromDB(p))); //, columns)));
                Manager.Response.Set(table, objects);
            }
        }

        public static void Add(WebManager Manager)
        {
            string parent = Manager.Parameters.Get("parent", "0").ToString();
            string type = (Manager.Parameters.Get("type", "").ToString().Equals("Select tipo 1") ||
                           Manager.Parameters.Get("type", "").ToString().Equals("Select tipo 2"))
                           ? "Select" : Manager.Parameters.Get("type", "").ToString();
            if (type.Equals(""))
            {
                Manager.Response.Set("Error", "No se puede guardar un nodo sin tipo");
                return;
            }
            string alias = Manager.Parameters.Get("alias", "").ToString();
            string value = Manager.Parameters.Get("value", "").ToString();
            string order = Manager.Parameters.Get("order", "").ToString();
            string hidden = Manager.Parameters.Get("hidden", "").ToString();
            string required = Manager.Parameters.Get("required", "").ToString();
            string editable = Manager.Parameters.Get("editable", "").ToString();
            string jsonNodes = Manager.Parameters.Get("nodes", "").ToString();
            List<Dictionary<string, object>> nodes = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(jsonNodes);

            dynamic Node = Manager.InternalORM("MakeListNodes");
            Node.Parent = parent;
            Node.Type = type;
            Node.Alias = alias;
            Node.Value = value;
            Node.Order = order;
            Node.Hidden = hidden;
            Node.Required = required;
            Node.Editable = editable;
            if (!Node.Save())
                throw new Exception("No se pudo guardar el nodo");
            if (!Node.Load())
                throw new Exception("No se pudo cargar el nodo");

            string root;
            if (Node.Type.ToString().Equals("Form") || Node.Type.ToString().Equals("List"))
            {
                root = Node.Id.ToString();
                dynamic Makelist = Manager.InternalORM("MakeList");
                Makelist.Root = root;
                Makelist.Name = Manager.Parameters.Get("makelistName", "").ToString();
                if (!Manager.Parameters.Get("userLevel", null).Equals(""))
                    Makelist.UserLevel = Manager.Parameters.Get("userLevel", null);
                Makelist.Type = type.ToLower();
                if (!Manager.Parameters.Get("comments", null).Equals(""))
                    Makelist.Comments = Manager.Parameters.Get("comments", null);
                if (!Makelist.Save())
                    throw new Exception("No se pudo guardar el listado o formulario");
            }

            else
                root = Manager.Parameters.Get("root", "").ToString();

            Dictionary<string, object> node = new Dictionary<string, object>
            {
                { "id", Node.Id.ToString() },
                { "parent", parent },
                { "type", type },
                { "alias", alias },
                { "value", value },
                { "order", order },
                { "hidden", hidden },
                { "required", required },
                { "editable", editable } };

            nodes.Add(node);

            Manager.Response.Set("nodes", nodes);
            Manager.Response.Set("types", GetTypes(type));
            Manager.Response.Set("parent", Node.Id.ToString());
            Manager.Response.Set("root", root);
            Manager.Response.Set("links", Node.FullIndex.ToString());
        }

        public static void Details(WebManager Manager)
        {
            if (!int.TryParse(Manager.Parameters.Get("id", "").ToString(), out int id))
                throw new Exception("Id incorrecto");

            dynamic Node = Manager.InternalORM("MakeListNodes");
            Node.Id = id;
            if (!Node.Load())
                throw new Exception("No se pudo cargar el nodo");

            List<Dictionary<string, object>> nodes = new List<Dictionary<string, object>>();
            Dictionary<string, object> dict;

            Report report = new Report(Manager, "VGF_MakeListNodes");
            report.Filter("Parent", Report.FilterType.EQUAL, id);
            DataTable dt = report.Execute().Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                dict = new Dictionary<string, object> {
                    { "id", row["Id"] },
                    { "parent", row["Parent"] },
                    { "type", row["Type"] },
                    { "alias", row["Alias"] },
                    { "value", row["Value"] },
                    { "order", row["Order"] },
                    { "hidden", row["Hidden"] },
                    { "required", row["Required"] },
                    { "editable", row["Editable"] },
                    { "label", row["Label"] },
                    { "placeholder", row["Placeholder"] },
                    { "mask", row["Mask"] } };
                nodes.Add(dict);
            }

            if(Manager.isMethod("post"))
            {
                Save(Manager, Node);
            }

            Manager.Response.Set("node", Node.ToDictionary());
            Manager.Response.Set("nodes", nodes);
            Manager.Response.Set("types", GetTypes(Node.Type));
            if (Node.FullIndex != null)
                Manager.Response.Set("tree", Node.FullIndex.ToString().Split('.'));
        }

        public static void Save(OAuthManager Manager)
        {
            Save(Manager, null);
        }

        private static void Save(WebManager Manager, dynamic Node = null)
        {
            string redirect = "makelist";
            if (Node == null)
            {
                Node = Manager.InternalORM("MakeListNodes");
                Node.IgnoreInsertedData();
            }

            Node.Parent = Manager.Parameters.Get("parent", Node.Parent);
            Node.Type = Manager.Parameters.Get("type", Node.Type);
            Node.Alias = Manager.Parameters.Get("alias", Node.Alias);
            Node.Value = Manager.Parameters.Get("value", Node.Value);
            Node.Order = Manager.Parameters.Get("order", Node.Order);
            Node.Hidden = Manager.Parameters.Get("hidden", Node.Hidden);
            Node.Required = Manager.Parameters.Get("required", Node.Required);
            Node.Editable = Manager.Parameters.Get("editable", Node.Editable);
            Node.Label = Manager.Parameters.Get("label", Node.Label);
            Node.Placeholder = Manager.Parameters.Get("placeholder", Node.Placeholder);
            Node.Mask = Manager.Parameters.Get("mask", Node.Mask);

            if (Node.Empty())
            {
                //es nuevo
                redirect += (Node.Parent != null && !Node.Parent.ToString().Equals("") ? "/" + Node.Parent : "");
            }
            else
            {
                redirect += "/" + Node.Id;
            }

            try
            {
                Node.Save();
            }
            catch (Exception e)
            {
                if (e.InnerException == null)
                {
                    throw new Exception("makelist_save", e);
                }

                SqlException inner = (SqlException)e.InnerException;
                switch (inner.Number)
                {
                    case 2627:
                        throw new Exception("makelist_save_duplicated", inner);
                    default:
                        throw new Exception("makelist_save", inner);
                }
            }

            Manager.Response.Set("redirect", redirect);
        }

        public static void Delete(WebManager Manager)
        {
            dynamic Node = Manager.InternalORM("MakeListNodes");
            Node.Id = Manager.Parameters.Get("id", "");

            if (!Node.Load())
                throw new Exception("No se pudo cargar el nodo");

            if (!Node.Delete())
                throw new Exception("No se pudo eliminar el nodo " + Node.Id.ToString());

            if(Node.Parent != null && Node.Parent > 0)
                Manager.Response.Set("redirect", "makelist/" + Node.Parent);
            else
                Manager.Response.Set("redirect", "makelist");
        }

        public static void ListForms(WebManager Manager)
        {
            List<Dictionary<string, object>> nodes = new List<Dictionary<string, object>>();
            Report report = new Report(Manager, "VGF_MakeListNodes");
            report.Filter("Parent", Report.FilterType.EQUAL, 0);
            DataTable dt = report.Execute().Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> dict = new Dictionary<string, object> {
                    { "id", row["Id"] },
                    { "parent", row["Parent"] },
                    { "type", row["Type"] },
                    { "alias", row["Alias"] },
                    { "value", row["Value"] },
                    { "order", row["Order"] },
                    { "hidden", row["Hidden"] },
                    { "required", row["Required"] },
                    { "editable", row["Editable"] },
                    { "label", row["Label"] },
                    { "placeholder", row["Placeholder"] },
                    { "mask", row["Mask"] } };
                nodes.Add(dict);
            }

            Manager.Response.Set("nodes", nodes);
            Manager.Response.Set("types", GetTypes("Root"));
        }

        private static List<string> GetTypes(string type)
        {
            var types = new List<string>();
            switch (type)
            {
                case "Root":
                    types.Add("Form");
                    types.Add("List");
                    break;
                case "Form":
                    types.Add("Section");
                    break;
                case "List":
                    types.Add("Column");
                    types.Add("Button");
                    break;
                case "Section":
                    types.Add("Number");
                    types.Add("Text");
                    types.Add("TextArea");
                    types.Add("Password");
                    types.Add("Button");
                    types.Add("Select");
                    types.Add("Date");
                    types.Add("DateTime");
                    types.Add("Time");
                    types.Add("File");
                    break;
                case "Select":
                    types.Add("Option");
                    types.Add("Data");
                    types.Add("DataKey");
                    types.Add("DataValue");
                    types.Add("DataMethod");
                    break;
                case "Button":
                    types.Add("Icon");
                    break;
                default:
                    break;
            }
            return types;
        }
    }
}