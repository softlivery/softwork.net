﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using System.Text;

namespace VGFramework.Helpers
{
    class Users
    {
        internal static dynamic Login(string user, string password)
        {
            dynamic User = OAuthManager.Instance.InternalORM("Users");

            if (OAuthManager.Instance.GetConfig("ldap_enabled") != null && OAuthManager.Instance.GetConfig("ldap_enabled").Equals("true"))
            {
                string ldap_host = OAuthManager.Instance.GetConfig("ldap_host");
                string ldap_dn = OAuthManager.Instance.GetConfig("ldap_dn");
                string ldap_domain = OAuthManager.Instance.GetConfig("ldap_domain");
                string ldap_query = OAuthManager.Instance.GetConfig("ldap_query");

                //user = string.Concat(user, ldap_domain);

                LdapConnection ldap = null;
                try
                {
                    ldap = new LdapConnection(ldap_host)
                    {
                        //Credential = new NetworkCredential(user, password, ldap_domain),
                        AuthType = AuthType.Basic,
                        AutoBind = false
                    };
                }
                catch (Exception e)
                {
                    throw new Exception("LDAP disabled");
                }

                if (ldap == null)
                    throw new Exception("Unable to connect LDAP Server");

                try
                {
                    ldap.Bind(new NetworkCredential(string.Format("{0}@{1}", user, ldap_domain), password));
                    OAuthManager.Instance.LogDebug("User authenticated: " + user);
                    /*
                    //string filter = "(UserPrincipalName=" + user + ")";
                    string filter = "(sAMAccountName=" + user + ")";
                    string[] attr = new string[] { "displayname", "mail" };
                    SearchRequest searchRequest = new SearchRequest(ldap_query, filter, SearchScope.Subtree, attr);
                    SearchResponse result = (SearchResponse)ldap.SendRequest(searchRequest);

                    if (result == null)
                        throw new Exception("Unable to search LDAP Server");

                    if (result.Entries.Count == 0)
                        throw new Exception("Unable to get user info");

                    //string mail = result.Entries[0].Attributes["mail"][0].ToString();
                    //string name = result.Entries[0].Attributes["displayname"][0].ToString();
                    */
                    User.Name = user;

                    if (!User.Load())
                    {
                        //generate new user
                        Random random = new Random();
                        User.Name = user;
                        User.Password = new string(Enumerable.Repeat("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789", 20).Select(s => s[random.Next(s.Length)]).ToArray());
                        User.Level = OAuthManager.Instance.GetConfig("ldap_default_userlevel");
                        if (!User.Save())
                            throw new Exception("User cannot be saved");
                    }
                    return User;
                }
                catch (Exception e)
                {
                    OAuthManager.Instance.LogError("LDAP bind: " + e.Message);
                }
            }

            User.Name = user;
            User.Password = password;

            if (!User.Load())
            {
                OAuthManager.Instance.LogError("Users - Login - Credenciales invalidas");
                return null;
            }

            return User;
        }
        public static void ME(OAuthManager Manager)
        {
            dynamic User = Manager.InternalORM("Users");
            User.Id = Manager.ClientUser.User;
            if (!User.Load())
                throw new Exception("User cannot be loaded");

            Manager.Response.Set("User", User.Name);
            dynamic Level = Manager.InternalORM("UserLevels");
            Level.Id = User.Level;
            Level.Load();
            Manager.Response.Set("Level", Level.Name);
        }
        
        public static void List(OAuthManager Manager)
        {
            string userLevel = null;
            if (Manager.User != null)
                userLevel = Manager.User.Level.ToString();
            Makelist.List(Manager, "VGF_Users", userLevel);
        }

        public static void Add(OAuthManager Manager)
        {
            dynamic newUser = Manager.InternalORM("Users");
            newUser.Name = Manager.Parameters.Get("name", "");
            newUser.Password = Manager.Parameters.Get("password", "");
            newUser.Level = Manager.Parameters.Get("level", "");
            newUser.Group = Manager.Parameters.Get("group", "");
            newUser.Comments = Manager.Parameters.Get("comments", "");
            if (!newUser.Save())
                throw new Exception("User cannot be saved");
            Dictionary<string, object> columns = newUser.ToDictionary();
            foreach (KeyValuePair<string, object> entry in columns)
            {
                Manager.Response.Set(entry.Key.ToLower(), entry.Value);
            }
        }

        public static void Details(OAuthManager Manager)
        {
            if (Manager.isMethod("get"))
            {
                try
                {
                    string Id = Manager.QueryParameters.Get("id", "").ToString();
                    if (Id.Equals(""))
                        throw new Exception("Must specify a user");

                    if (!int.TryParse(Id, out int user))
                        throw new Exception("Invalid user id");
                    dynamic User = Manager.InternalORM("Users");
                    User.Id = user;
                    User.Load();
                    Dictionary<string, object> columns = User.ToDictionary();
                    foreach (KeyValuePair<string, object> entry in columns)
                    {
                        Manager.Response.Set(entry.Key.ToLower(), entry.Value);
                    }
                }
                catch (Exception e)
                {
                    Manager.Response.Set("Error", e.Message);
                }
            }

            if (Manager.isMethod("post"))
            {
                string Id = Manager.Parameters.Get("id", "").ToString();
                if (Id.Equals(""))
                    throw new Exception("Must specify a user");

                if (!int.TryParse(Id, out int user))
                    throw new Exception("Invalid user id");
                dynamic User = Manager.InternalORM("Users");
                User.Id = user;
                if (!User.Load())
                    throw new Exception("Must specify an existent user");

                if (Manager.Parameters.Get("name", "").ToString() != "")
                    User.Name = Manager.Parameters.Get("name", "");
                if (Manager.Parameters.Get("password", "").ToString() != "")
                    User.Password = Manager.Parameters.Get("pass", "");
                if (Manager.Parameters.Get("level", "").ToString() != "")
                    User.Level = Manager.Parameters.Get("level", "").ToString();
                User.Save();
                Dictionary<string, object> columns = User.ToDictionary();
                foreach (KeyValuePair<string, object> entry in columns)
                {
                    Manager.Response.Set(entry.Key.ToLower(), entry.Value);
                }
            }
        }

        public static void Delete(OAuthManager Manager)
        {
            string Id = Manager.Parameters.Get("id", "").ToString();
            if (Id.Equals(""))
                throw new Exception("Must specify a user");

            int user;
            if (!int.TryParse(Id, out user))
                throw new Exception("Invalid user id");
            dynamic User = Manager.InternalORM("Users");
            User.Id = user;
            User.Load();
            dynamic UserLevel = Manager.InternalORM("UserLevels");
            UserLevel.Id = User.Level;
            UserLevel.Load();
            Dictionary<string, object> columns = User.ToDictionary();
            columns["Level"] = UserLevel.Name;
            foreach (KeyValuePair<string, object> entry in columns)
            {
                Manager.Response.Set(entry.Key.ToLower(), entry.Value);
            }
            User.Delete();
        }

        public static void Menu(OAuthManager Manager)
        {
            Data.Procedure procedure = new Data.Procedure(Manager, "VGF_GetMenu");
            procedure.AddParameter((int) Manager.ClientUser.User);

            DataSet ds = procedure.Execute();
            List<Dictionary<string, object>> items = new List<Dictionary<string, object>>();
            Dictionary<string, Item> menuItems = new Dictionary<string, Item>();
            Dictionary<string, Item> allItems = new Dictionary<string, Item>();

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                string Id = ds.Tables[0].Rows[i]["Id"].ToString();
                string permission = ds.Tables[0].Rows[i]["Permission"].ToString();
                string label = ds.Tables[0].Rows[i]["Label"].ToString();
                string icon = ds.Tables[0].Rows[i]["Icon"].ToString();
                string to = ds.Tables[0].Rows[i]["Action"].ToString();
                int size = ds.Tables[0].Rows[i]["FullIndex"].ToString().Split('.').Count();
                string parent;
                Item item;
                if (size > 1)
                {
                    parent = ds.Tables[0].Rows[i]["FullIndex"].ToString().Split('.')[size - 2];
                    item = new Item(permission, label, icon, to, allItems[parent]);
                }
                else
                {
                    item = new Item(permission, label, icon, to, null);
                    menuItems.Add(Id, item);
                }
                allItems.Add(Id, item);
            }

            foreach (KeyValuePair<string, Item> entry in menuItems)
            {
                if (entry.Value.permission != "" || entry.Value.HasChildrens()) //devuelve sólo los menus y submenus que tienen asociado un permiso o que tienen hijos
                    items.Add(entry.Value.Get());
            }
            Manager.Response.Set("menu", items);
        }

        public static void Permissions(OAuthManager Manager)
        {
            Data.Procedure procedure = new Data.Procedure(Manager, "VGF_SearchPermissions");
            procedure.AddParameter(Manager.ClientUser.User.ToString());
            DataSet ds = procedure.Execute();

            List<string> permissions = new List<string>();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                permissions.Add(ds.Tables[0].Rows[i][0].ToString());
            Manager.Response.Set("permissions", permissions);
        }

        public static void Routes(OAuthManager Manager)
        {
            List<Dictionary<string, string>> routes = new List<Dictionary<string, string>>();
            Data.Report makelist = new Data.Report(Manager, "VGF_MakeList");
            DataSet ds = makelist.Execute();
            Dictionary<string, string> route;
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                route = new Dictionary<string, string>();
                route.Add("url", ds.Tables[0].Rows[i][2].ToString()/*.ToLower()*/);
                route.Add("component", ds.Tables[0].Rows[i][4].ToString().ToLower());
                routes.Add(route);
            }
            Manager.Response.Set("routes", routes);
        }

        public static bool Can(OAuthManager Manager)
        {
            string permission = Manager.QueryParameters.Get("permission", "");
            dynamic Permission = Manager.InternalORM("Permissions");
            Permission.Name = permission;
            if (!Permission.Load())
                throw new Exception("Permission cannot be loaded");
            Data.Procedure procedure = new Data.Procedure(Manager, "VGF_SearchPermissions");
            procedure.AddParameter(Manager.ClientUser.User.ToString());
            procedure.AddParameter(permission);
            DataSet ds = procedure.Execute();

            int count = int.Parse(ds.Tables[0].Rows[0][0].ToString());
            return (count > 0);
        }
    }
}
