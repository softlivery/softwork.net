﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;


public class Pattern
{
    private string name;
    private string pattern;
    private string color;
    private bool bold;
    private bool show;
    private string data;
    private string date;
    private string server;
    private string type;
    private string callid;

    /***
        * 
        * 
        * 
        * */
    public Pattern(string pName, string pPattern, string pColor)
    {
        this.name = pName;
        this.pattern = pPattern;
        this.color = pColor;
        this.bold = false;
        this.show = true;
    }

    public string toCSS()
    {
        string ret = ".class_" + this.name + ", " + this.name + " {\n";
        ret += "   color: " + this.color + ";\n";
        if (this.bold)
        {
            ret += "   font-weight: bold;\n";
        }
        if (!this.show)
        {
            ret += "   display: none;\n";
        }
        ret += "}\n";
        return ret;
    }

    public string toHTML(string line)
    {
        string ret = "<span class=\"class_" + this.name + "\">" + line + "\n</span><br />";
        return ret;
    }

    public string toHTML()
    {
        string ret = "<span class=\"class_" + this.name + "\">" + this.date + " " + this.callid + " " + this.type + " " + this.data + "</span><br />";
        return ret;
    }

    public string toJson(string line)
    {
        string ret = "{\"event\":\"" + this.name + "\",\"data\":\"" + line + "\"}";
        return ret;
    }

    public string toJson()
    {
        string ret = "{\"event\":\"" + this.name + "\",\"data\":\"" + this.data.Replace("\"", "\\\"") + "\",\"date\":\"" + this.date + "\",\"server\":\"" + this.server + "\",\"type\":\"" + this.type + "\",\"callid\":\"" + this.callid + "\"}";
        return ret;
    }

    public string toXML(string line)
    {
        string ret = "<" + this.name + ">" + line + "</" + this.name + ">";
        return ret;
    }

    public bool matches(string line)
    {
        Match res = Regex.Match(line, this.pattern, RegexOptions.IgnoreCase);
        if (res.Success)
        {
            string[] tmp = Regex.Split(line, @"[\s>]+", RegexOptions.IgnoreCase);
            this.date = tmp[0] + " " + tmp[1];
            this.callid = tmp[2];
            this.type = tmp[3];
            this.data = "";
            for (int i = 4; i < tmp.Length; i++)
                this.data += tmp[i] + " ";
            return true;
        }
        return false;
    }
}