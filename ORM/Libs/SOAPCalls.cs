﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;


class SOAPCalls
{
    public void CallMethod(VGFramework.WebManager Manager)
    {
        //obtener parametros
        /*string distriid = Manager.BodyParameters.Get("distriID", "");
        string pass = Manager.BodyParameters.Get("password", "");
        var param = Manager.BodyParameters.All();
        var keys = param.Keys.ToArray();*/


        string url = Manager.Headers.Get("Url", "");

        string ns = "vscwsadmin";

        string distriID = "1";
        string password = "1234";

        string user = "sup";
        string pass = "x";

        string authkey = "";


        if(url=="")
        {
            throw new Exception("Invalid Url");
        }

        XmlDocument soapenv = new XmlDocument();
        XNode xml = JsonConvert.DeserializeXNode(Manager.Parameters.GetJsonParameter(), "");
        soapenv.LoadXml(xml.ToString());

        string[] soapaction = Manager.Headers.Get("SOAPAction", "").Split('/');

        HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
        webRequest.Headers.Add("SOAPAction", soapaction[0] + "/" + soapaction[1]);
        webRequest.ContentType = "text/xml;charset=\"utf-8\"";
        webRequest.Accept = "text/xml";
        webRequest.Method = "POST";

        using (Stream stream = webRequest.GetRequestStream())
        {
            soapenv.Save(stream);
        }

        IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);
        asyncResult.AsyncWaitHandle.WaitOne();
        string soapResult;

        using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
        {
            using (StreamReader r = new StreamReader(webResponse.GetResponseStream()))
            {
                soapResult = r.ReadToEnd();
            }
        }

        XDocument res = XDocument.Parse(soapResult);
        //string jsontext = JsonConvert.SerializeXNode(f);
        dynamic jsonres = JObject.Parse(JsonConvert.SerializeXNode(res));

        Manager.Response.Set("WebServiceResponse", System.Web.Helpers.Json.Decode(jsonres.ToString(Newtonsoft.Json.Formatting.None)));





    }
}

