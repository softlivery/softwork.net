﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Dynamic;
using System.Threading;

namespace VGFramework
{
    public class TemplateParser
    {

        public int uid = 1;
        public Dictionary<int, Dictionary<string, string>> tokens;
        public Dictionary<string, string> modifiers;
        public WebManager Manager;
        private Translator translator;

        public TemplateParser(WebManager manager)
        {
            this.Manager = manager;
            //this.translator = new Translator(request.lang);
            modifiers = new Dictionary<string, string>();
            modifiers.Add("MLcolumns2html", "ColumnHTML");
            modifiers.Add("MLsection2html", "SectionHTML");
            modifiers.Add("MLitem2html", "ItemHTML");
            modifiers.Add("AuditChanges2HTML", "AuditChanges2HTML");
            modifiers.Add("ParseTemplate", "ParseTemplate");
            modifiers.Add("Money", "Money");
            modifiers.Add("RoundedMoney", "RoundedMoney");
            modifiers.Add("Date", "Date");

            this.reset();
        }

        public void reset()
        {
            this.uid = 1;
            this.tokens = new Dictionary<int, Dictionary<string, string>>();
        }

        public string parse(string template, object data)
        {
            this.reset();
            return this.parseInternal(template, data);
        }

        private string parseInternal(string template, object data)
        {
            if (template.Equals(""))
                return "";

            try
            {
                template = this.processMarkers(template);
                //template = this.processControls(template);
                template = this.process(template, data);
                return template;
            }
            catch (Exception e)
            {
                throw new Exception("TemplateParser - ParseInternal: " + e.Message);
            }
        }

        public string parseFile(string file, object data)
        {
            try
            {
                Manager.LogDebug("Template Parser - ParseFile: " + file);
                //this.translator.cargar_seccion(file);
                file = File.ReadAllText(Manager.GetConfig("SiteRoot") + @"sections\" + file + ".html");
                return this.parse(file, data);
            }
            catch (Exception ex)
            {
                Manager.LogError("Template Parser - ParseFile: " + ex.Message);
                return "";
            }
        }

        private string processMarkers(string template)
        {
            //procesar {{ data }}
            template = Regex.Replace(template, @"{{\s*([\'\w_\.]+)(\|([\w-_\(\)\,\.]+))*\s*}}", delegate (Match m)
            {
                int id = this.uid++;
                List<string> modif = new List<string>();
                Dictionary<string, string> token = new Dictionary<string, string>();
                string code = m.Groups[1].ToString().Trim();
                string modifiers = m.Groups[2].ToString().Trim().Trim('|');

                token.Add("buffer", code);
                token.Add("modifs", modifiers);

                this.tokens.Add(id, token);
                return "{_" + id + "}";
            }, RegexOptions.IgnoreCase);

            //procesar {% set var %}
            template = Regex.Replace(template, @"{%\s+set\s+([a-z0-9_\.']+)\s+%}([^%]+)?{%\s+endSet\s+%}", delegate (Match m)
            {
                Dictionary<string, string> token = new Dictionary<string, string>();
                int id = this.uid++;
                string var = m.Groups[1].ToString().Trim();
                string cont = m.Groups[2].ToString().Trim();
                token.Add("type", "set");
                token.Add("var", var);
                token.Add("cont", cont);
                this.tokens.Add(id, token);
                return "{_" + id + "}";
            }, RegexOptions.IgnoreCase);

            //procesar {% if v.ar_1 != va.r_2 %} {% else %} {% endIf %}
            //template = Regex.Replace(template, @"{%\s*if\s+(.+)\s*%}", delegate(Match m)
            template = Regex.Replace(template, @"{%\s*if\s+([^%}]+)\s*%}([\w\s""'|<>=\:\(\)\#\!\*$\+\/&\?;.,{}-]+)({%\s*else\s*%}([\w\s""'|<>=\:\(\)\#\!\*$\+\/&\?;.,{}-]+))?{%\s*endIf\s*%}", delegate (Match m)
            {
                Dictionary<string, string> token = new Dictionary<string, string>();
                int id = this.uid++;
            //string if_ = m.Groups[0].ToString().Trim();
            //string[] parts = Regex.Split(if_, @"{%\s*(else|endIf)\s*%}", RegexOptions.IgnoreCase);
            Match m2 = Regex.Match(m.Groups[1].ToString().Trim(), @"([\w\.\-_'""]+)\s*([=!<>]+)\s*([\w\.\-_'""]+)", RegexOptions.IgnoreCase);
                if (m2.Success)
                {
                    token.Add("type", "if");
                    token.Add("var1", m2.Groups[1].ToString());
                    token.Add("oper", m2.Groups[2].ToString());
                    token.Add("var2", m2.Groups[3].ToString());
                //parts[0] = Regex.Replace(parts[0], @"{%[^%]+%}", "");
                //token.Add("True", parts[0]);
                //token.Add("False", parts[2]);
                token.Add("True", m.Groups[2].ToString().Trim());
                    token.Add("False", m.Groups[4].ToString().Trim());
                    this.tokens.Add(id, token);
                    return "{_" + id + "}";
                }
                return m.Groups[0].ToString();
            }, RegexOptions.IgnoreCase);

            //procesar {% for v.ar_1 != va.r_2 %} {% endFor %}
            template = Regex.Replace(template, @"{%\s+for\s+(([a-z0-9_\.']+)\s*,)?\s*([a-z0-9_\.']+)\s+in\s+([a-z0-9_\.']+)\s+%}([^%]+)?({%\s*else\s*%}([\w\s""<>=\:\(\)\#\!\*$\+\/&;.,{}-]+))?{%\s+endFor\s+%}", delegate (Match m)
            {
                Dictionary<string, string> token = new Dictionary<string, string>();
                int id = this.uid++;
                token.Add("type", "for");
                token.Add("key", m.Groups[2].ToString());
                token.Add("elem", m.Groups[3].ToString());
                token.Add("list", m.Groups[4].ToString());
                token.Add("content", m.Groups[5].ToString());
                token.Add("else", m.Groups[7].ToString());
                this.tokens.Add(id, token);
                return "{_" + id + "}";
            }, RegexOptions.IgnoreCase);

            //procesar {% include section %}
            template = Regex.Replace(template, @"{%\s*include\s*([\w_-]+)\s*%}", delegate (Match m)
            {
                Dictionary<string, string> token = new Dictionary<string, string>();
                int id = this.uid++;
                string include = m.Groups[1].ToString().Trim();
                token.Add("type", "include");
                token.Add("section", include);
                this.tokens.Add(id, token);
                return "{_" + id + "}";
            }, RegexOptions.IgnoreCase);

            //procesar [[ code ]]
            template = Regex.Replace(template, @"\[\[\s*([\'\w_\. ]+)\s*\]\]", delegate (Match m)
            {
                int id = this.uid++;
                List<string> modif = new List<string>();
                Dictionary<string, string> token = new Dictionary<string, string>();
                string code = m.Groups[1].ToString().Trim();
                token.Add("type", "translator");
                token.Add("code", code);

                this.tokens.Add(id, token);
                return "{_" + id + "}";
            }, RegexOptions.IgnoreCase);

            return template;
        }

        private string processControls(string template)
        {
            return template;
        }

        private string process(string template, object data)
        {
            //procesar {_id}
            return Regex.Replace(template, @"{_(\d+?)}", delegate (Match m)
            {
                if (!this.tokens.ContainsKey(int.Parse(m.Groups[1].ToString())))
                {
                    throw new Exception("TemplateParser - Process: " + m.Groups[1].ToString());
                }
                Dictionary<string, string> token = this.tokens[int.Parse(m.Groups[1].ToString())];
                string replace = "";

                if (!token.ContainsKey("type"))
                {
                    replace = this.eval(data, token["buffer"]).ToString();

                    if (!token["modifs"].Equals(""))
                    {
                        string[] modifs = token["modifs"].Split('|');
                        foreach (string modif in modifs)
                        {
                            string[] args = Regex.Split(modif, @"[\(\),]", RegexOptions.IgnoreCase);
                            if (args.Length > 1)
                            {
                                replace = modifier(replace, args[0], data, args);
                            }
                            else
                            {
                                replace = modifier(replace, modif, data);
                            }
                        }
                    }
                }
                else
                {
                //TemplateParser nParcer = new TemplateParser(ref this.request);
                switch (token["type"])
                    {
                        case "include":
                            TemplateParser nParcer = new TemplateParser(Manager);
                            replace = nParcer.parseFile(token["section"], data);
                            break;
                        case "if":
                            string var1 = this.eval(data, token["var1"]).ToString();
                            string var2 = this.eval(data, token["var2"]).ToString();
                            bool res;

                            switch (token["oper"])
                            {
                                case "==":
                                    res = this.compare_equal(var1, var2);
                                    break;
                                case "!=":
                                    res = this.compare_not_equal(var1, var2);
                                    break;
                                case "<=":
                                    res = this.compare_less_equal(var1, var2);
                                    break;
                                case "<":
                                    res = this.compare_less(var1, var2);
                                    break;
                                case ">=":
                                    res = this.compare_greater_equal(var1, var2);
                                    break;
                                case ">":
                                    res = this.compare_greater(var1, var2);
                                    break;
                                default:
                                    return replace;
                            }
                            if (token.ContainsKey(res.ToString()))
                            {
                            //replace = nParcer.parse(token[res.ToString()], data);
                            replace = this.parseInternal(token[res.ToString()], data);
                            }
                            break;
                        case "for":
                            try
                            {
                                IEnumerable list = (IEnumerable)this.eval(data, token["list"]);
                                dynamic new_data = (dynamic)data;
                                int key = 1;
                                foreach (var elem in list)
                                {
                                    try
                                    {
                                        if (!token["key"].Equals(""))
                                        {
                                            ((ParametersBag<object>)data).Set(token["key"], key);
                                        }
                                        ((ParametersBag<object>)data).Set(token["elem"], elem);
                                    }
                                    catch (Exception e1)
                                    {
                                        try
                                        {
                                            if (!token["key"].Equals(""))
                                            {
                                                ((Dictionary<string, object>)data).Add(token["key"], key);
                                            }
                                            ((Dictionary<string, object>)data).Add(token["elem"], elem);
                                        }
                                        catch (Exception e2)
                                        {
                                            try
                                            {
                                                if (!token["key"].Equals(""))
                                                {
                                                    ((Dictionary<string, object>)data)[token["key"]] = key;
                                                }
                                                ((Dictionary<string, object>)data)[token["elem"]] = elem;
                                            }
                                            catch (Exception e3)
                                            {

                                            }
                                        }
                                    }
                                    replace += this.parseInternal(token["content"], data);
                                    key++;
                                }
                                if (key == 1)
                                {
                                    replace = this.parseInternal(token["else"], data);
                                }
                            }
                            catch (Exception e)
                            {
                                Manager.LogError("Template Parser - Process: " + e.Message);
                                replace = "";
                            }
                            break;
                        case "set":
                            ((dynamic)data)[token["var"]] = this.parseInternal(token["cont"], data);
                            break;
                        case "translator":
                            replace = this.translator.traducir(token["code"]);
                            break;
                        default:
                            break;
                    }
                }

                return replace;
            });
        }

        private bool compare_greater(string var1, string var2)
        {
            return var1.CompareTo(var2) > 0;
        }

        private bool compare_greater_equal(string var1, string var2)
        {
            return this.compare_greater(var1, var2) || this.compare_equal(var1, var2);
        }

        private bool compare_less(string var1, string var2)
        {
            return var1.CompareTo(var2) < 0;
        }

        private bool compare_less_equal(string var1, string var2)
        {
            return this.compare_less(var1, var2) || this.compare_equal(var1, var2);
        }

        private bool compare_not_equal(string var1, string var2)
        {
            return !this.compare_equal(var1, var2);
        }

        private bool compare_equal(string var1, string var2)
        {
            //return var1.CompareTo(var2) == 0;
            return var1.Equals(var2);
        }

        public object eval(object data, string buffer)
        {
            Manager.LogDebug("Template Parser - Eval: " + buffer);
            string[] parts = buffer.Split('.');
            int i = 0;

            Match m = Regex.Match(buffer, @"'(.+)?'", RegexOptions.IgnoreCase);
            if (m.Success)
            {
                return m.Groups[1].ToString();
            }

            bool res;
            if (Boolean.TryParse(buffer, out res))
            {
                return res.ToString();
            }

            if (parts[0].Equals("route") && parts.Length == 1)
            {
                return Manager.Route.name;
            }

            if (parts[0].Equals("global_config") && parts.Length == 2)
            {
                return Manager.GetConfig(parts[1]);
            }

            if (parts[0].Equals("session"))
            {
                data = Manager.Session;
                i = 1;
            }

            /*if (parts[0].Equals("client"))
            {
                data = null;
                i = 1;
            }*/

            if (parts[0].Equals("request"))
            {
                //data = Manager.RequestHTTP;
                data = Manager;
                i = 1;
            }

            for (; (i < parts.Length && data != null); i++)
            {
                try
                {
                    dynamic dyn_data = (dynamic)data;
                    data = dyn_data[parts[i]];
                }
                catch (Exception e1)
                {
                    Manager.LogDebug("Template Parser - Eval - E1: " + e1.Message);
                    try
                    {
                        int ind = int.Parse(parts[i]);
                        dynamic dyn_data = (dynamic)data;
                        data = dyn_data[ind];
                    }
                    catch (Exception e2)
                    {
                        Manager.LogDebug("Template Parser - Eval - E2: " + e2.Message);
                        try
                        {
                            Type type = data.GetType();
                            System.Reflection.PropertyInfo property = type.GetProperty(parts[i]);
                            data = property.GetValue(data, null);
                        }
                        catch (Exception e3)
                        {
                            Manager.LogDebug("Template Parser - Eval - E3: " + e3.Message);
                            try
                            {
                                object result;
                                ParserGetMemberBinder binder = new ParserGetMemberBinder(parts[i], false);
                                dynamic dyn_data = (dynamic)data;
                                dyn_data.TryGetMember(binder, out result);
                                data = result;
                            }
                            catch (Exception e4)
                            {
                                Manager.LogDebug("Template Parser - Eval - E4: " + e4.Message);
                                return "";
                            }
                        }
                    }
                }
            }
            return (data != null ? data : "");
        }

        private string modifier(string input, string modifier, object data, string[] args = null)
        {
            Manager.LogDebug(string.Format("Template Parser - Modifiers: {0} to {1}", modifier, input));
            if (modifiers.ContainsKey(modifier))
            {
                return Manager.Dispatch(modifiers[modifier] + "::Execute", new object[] { this, input, modifier, data, args }).ToString();
            }

            switch (modifier)
            {
                case "Decimal":
                    double value;
                    if (Double.TryParse(input, out value))
                    {
                        //return value.ToString("F2");
                        //return value.ToString("F" + this.request.culture.NumberFormat.CurrencyDecimalDigits);
                        return value.ToString("N", Thread.CurrentThread.CurrentCulture);
                    }
                    return input;
                case "Date":
                    //verificar tiempo en DB de produccion MM/dd/yyyy h:mm:ss tt
                    if (!input.Equals(""))
                    {
                        //DateTime date1 = DateTime.ParseExact(replace.Replace(".", ""), @"dd/MM/yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                        //return date1.ToString(@"dd/MM/yyyy");
                        DateTime date1 = DateTime.Parse(input);
                        return date1.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
                    }
                    return input;
                case "Time":
                    if (!input.Equals(""))
                    {
                        //DateTime date2 = DateTime.ParseExact(replace.Replace(".", ""), @"dd/MM/yyyy hh:mm:ss tt", System.Globalization.CultureInfo.InvariantCulture);
                        //return date2.ToString("hh:mm:ss tt");
                        DateTime date1 = DateTime.Parse(input);
                        return date1.ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortTimePattern);
                    }
                    return input;
                case "LatLng":
                    return input.Replace(',', '.');
                case "fromDictionary":
                    if (args == null || args.Length == 0)
                    {
                        return input;
                    }
                    else
                    {
                        string key = args[1] + "." + input;
                        return this.eval(data, key).ToString();
                    }
                case "FileName":
                    if (input != null)
                    {
                        return Regex.Replace(Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(input), "[^a-zA-Z0-9]+", "_");
                    }
                    return input;
                case "inArray":
                    if (args != null && args.Length > 1)
                    {
                        IEnumerable list = (IEnumerable)this.eval(data, args[1]);
                        string key = "";
                        if (args.Length > 2)
                        {
                            for (int i = 2; i < args.Length - 1; i++)
                            {
                                if (!key.Equals(""))
                                {
                                    key += ".";
                                }
                                key += args[i];
                            }
                        }

                        foreach (var elem in list)
                        {
                            if (this.eval(elem, key).Equals(input))
                            {
                                return input;
                            }
                        }
                    }
                    return "";
                case "inArrayCI":
                    if (args != null && args.Length > 1)
                    {
                        IEnumerable list = (IEnumerable)this.eval(data, args[1]);
                        string key = "";
                        if (args.Length > 2)
                        {
                            for (int i = 2; i < args.Length - 1; i++)
                            {
                                if (!key.Equals(""))
                                {
                                    key += ".";
                                }
                                key += args[i];
                            }
                        }

                        string search = input.ToLower();
                        string current = null;

                        foreach (var elem in list)
                        {
                            current = this.eval(elem, key).ToString();
                            if (current.ToLower().Equals(search))
                            {
                                return current;
                            }
                        }
                    }
                    return "";
                case "replace":
                    if (args != null && args.Length > 1)
                    {
                        if (args.Length == 2)
                        {
                            return input.Replace(args[1], "");
                        }
                        else if (args.Length > 2)
                        {
                            return input.Replace(args[1], args[2]);
                        }
                    }
                    return input;
                case "ToUpper":
                    return input.ToUpper();
                case "ToLower":
                    return input.ToLower();
                case "FileExists":
                    if (File.Exists(input))
                        return input;
                    else
                        return "";
                case "Trim":
                    return input.Trim();
                default:
                    return input;
            }
        }
    }

    internal class ParserGetMemberBinder : GetMemberBinder
    {
        public ParserGetMemberBinder(string name, bool ignoreCase)
        : base(name, ignoreCase)
        {
        }

        public override DynamicMetaObject FallbackGetMember(DynamicMetaObject target, DynamicMetaObject errorSuggestion)
        {
            throw new NotImplementedException();
        }
    }
}