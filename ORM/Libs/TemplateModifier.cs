﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VGFramework.Libs
{
    public interface TemplateModifier
    {
        string Execute(TemplateParser parser, string input, string modifier, object data, string[] args);
    }
}
