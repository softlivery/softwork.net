-- Versionado de base de datos
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dbaGetVersion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[dbaGetVersion]
GO

CREATE PROCEDURE dbaGetVersion
WITH ENCRYPTION
AS
BEGIN
	DECLARE @NVersion NVARCHAR(50)
	
	IF EXISTS (SELECT * FROM Parameters WHERE Label = 'version')
		BEGIN
			SELECT @NVersion = Value FROM Parameters WHERE Label = 'version'
			PRINT 'Version: ' + @NVersion
		END
	ELSE
		PRINT 'VersionVGD not found'
END
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dbaPutVersion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[dbaPutVersion]
GO

CREATE PROCEDURE dbaPutVersion(@NVersion NVARCHAR(50))
WITH ENCRYPTION
AS
BEGIN
	SET NOCOUNT ON
	
	IF NOT EXISTS (SELECT * FROM Parametro WHERE Parametro = 'VersionVGD')
		BEGIN
			insert into Parametro select 'VersionVGD','VGD RTM' + @NVersion
		END
	ELSE
		BEGIN
			update Parametro set valordoparametro = 'VGD RTM' + @NVersion where Parametro = 'VersionVGD'
		END
	SET NOCOUNT OFF
END
GO

-- Agregar una columna a una tabla
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[dbaAddColumn]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
	drop procedure [dbo].[dbaAddColumn]
GO

CREATE PROCEDURE dbaAddColumn
(
	@TableName AS NVARCHAR(100),
	@ColumnName AS NVARCHAR(100),
	@ColumnType AS NVARCHAR(100),
	@AllowNulls AS BIT = 1,
	@Default AS NVARCHAR(100) = ''
)
WITH ENCRYPTION
AS
BEGIN
	DECLARE @TableName_ AS NVARCHAR(100)
	DECLARE @ColumnName_ AS NVARCHAR(100)
	DECLARE @ColumnType_ AS NVARCHAR(100)
	DECLARE @AllowNulls_ AS BIT 
	DECLARE @Default_ AS NVARCHAR(100) 
	
	DECLARE @SQL AS VARCHAR(8000)
	SET @SQL = ''
	SET @TableName_ = @TableName
	SET @ColumnName_ = @ColumnName
	SET @ColumnType_ = @ColumnType
	SET @AllowNulls_ = @AllowNulls
	SET @Default_ = @Default
	
	IF EXISTS(SELECT * FROM sysobjects WHERE NAME = @TableName_ AND type = 'U')
		IF NOT EXISTS(SELECT * FROM syscolumns WHERE name = @ColumnName_ AND ID = (SELECT ID FROM sysobjects WHERE NAME = @TableName_ AND type = 'U'))
			BEGIN
				SET @SQL = 'ALTER TABLE [' + @TableName_ + '] ADD ' + @ColumnName_ 
				SET @SQL = @SQL + ' ' + @ColumnType_ + ' '
				IF @AllowNulls_ = 0
					SET @SQL = @SQL + 'NOT NULL '
				ELSE
					SET @SQL = @SQL + 'NULL '
				IF @Default_ <> ''
					BEGIN
						SET @SQL = @SQL + 'CONSTRAINT DF_' + UPPER(@TableName_)
						SET @SQL = @SQL + '_' + UPPER(@ColumnName_) + ' '
						SET @SQL = @SQL + 'DEFAULT(' + @Default_ + ')'
					END	
				PRINT 'dbaAddColumn - ' + @SQL
				EXEC(@SQL)
			END
END
GO