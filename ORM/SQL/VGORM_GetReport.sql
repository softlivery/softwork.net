USE [MakeList]
GO

/****** Object:  StoredProcedure [dbo].[GetReport]    Script Date: 12/6/2019 1:12:50 PM ******/
DROP PROCEDURE [dbo].[GetReport]
GO

/****** Object:  StoredProcedure [dbo].[GetReport]    Script Date: 12/6/2019 1:12:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetReport]
	-- Add the parameters for the stored procedure here
	@ReportName nvarchar(50),
	@level int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
select * from VGF_ReportNodes where fullindex like convert(varchar(12), (select Root from VGF_Reports where name like @ReportName and (UserLevel = @level or UserLevel is null)), 106) + '%'
--select * from Node where fullindex like convert(varchar(12), (select Fullindex from Node where alias like @ReportName and Type = 'Report'), 106) + '%'

END

GO

