USE [MakeList]
GO

ALTER TABLE [dbo].[VGF_Reports] DROP CONSTRAINT [FK_Reports_ReportNodes]
GO

ALTER TABLE [dbo].[VGF_Reports] DROP CONSTRAINT [FK_Report_UserLevels]
GO

/****** Object:  Table [dbo].[VGF_Reports]    Script Date: 12/6/2019 1:11:39 PM ******/
DROP TABLE [dbo].[VGF_Reports]
GO

/****** Object:  Table [dbo].[VGF_Reports]    Script Date: 12/6/2019 1:11:39 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_Reports](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Root] [bigint] NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[UserLevel] [bigint] NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VGF_Reports]  WITH CHECK ADD  CONSTRAINT [FK_Report_UserLevels] FOREIGN KEY([UserLevel])
REFERENCES [dbo].[VGF_UserLevels] ([Id])
GO

ALTER TABLE [dbo].[VGF_Reports] CHECK CONSTRAINT [FK_Report_UserLevels]
GO

ALTER TABLE [dbo].[VGF_Reports]  WITH CHECK ADD  CONSTRAINT [FK_Reports_ReportNodes] FOREIGN KEY([Root])
REFERENCES [dbo].[VGF_ReportNodes] ([Id])
GO

ALTER TABLE [dbo].[VGF_Reports] CHECK CONSTRAINT [FK_Reports_ReportNodes]
GO

