USE [MakeList]
GO

ALTER TABLE [dbo].[VGF_Users] DROP CONSTRAINT [FK_Users_VGF_UsersGroups]
GO

ALTER TABLE [dbo].[VGF_Users] DROP CONSTRAINT [FK_Users_UserLevels]
GO

/****** Object:  Table [dbo].[VGF_Users]    Script Date: 12/6/2019 1:12:18 PM ******/
DROP TABLE [dbo].[VGF_Users]
GO

/****** Object:  Table [dbo].[VGF_Users]    Script Date: 12/6/2019 1:12:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_Users](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
	[Password] [nvarchar](250) NOT NULL,
	[Level] [bigint] NOT NULL,
	[Group] [bigint] NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VGF_Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_UserLevels] FOREIGN KEY([Level])
REFERENCES [dbo].[VGF_UserLevels] ([Id])
GO

ALTER TABLE [dbo].[VGF_Users] CHECK CONSTRAINT [FK_Users_UserLevels]
GO

ALTER TABLE [dbo].[VGF_Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_VGF_UsersGroups] FOREIGN KEY([Group])
REFERENCES [dbo].[VGF_UsersGroups] ([Id])
GO

ALTER TABLE [dbo].[VGF_Users] CHECK CONSTRAINT [FK_Users_VGF_UsersGroups]
GO

