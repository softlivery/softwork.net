USE [MakeList]
GO

/****** Object:  Table [dbo].[VGF_UserLevels]    Script Date: 12/6/2019 1:11:59 PM ******/
DROP TABLE [dbo].[VGF_UserLevels]
GO

/****** Object:  Table [dbo].[VGF_UserLevels]    Script Date: 12/6/2019 1:11:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_UserLevels](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_UserLevels] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

