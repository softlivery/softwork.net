﻿CREATE TRIGGER VGF_MakeListNode_Insert
ON VGF_MakeListNodes
AFTER INSERT, UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @id BIGINT;
	DECLARE @parent BIGINT;
	DECLARE @index NVARCHAR(500);
	
	SELECT @id = id, @parent = Parent FROM inserted;

	IF @parent = 0
		UPDATE VGF_MakeListNodes SET FullIndex = LTRIM(RTRIM(CONVERT(CHAR(20), @id))) WHERE id = @id;
	ELSE
		BEGIN
		SELECT @index = LTRIM(RTRIM([FullIndex])) FROM VGF_MakeListNodes WHERE Id = @parent;
		UPDATE VGF_MakeListNodes SET FullIndex = LTRIM(RTRIM(@index + '.' + CONVERT(CHAR(20), @id))) WHERE id = @id;
		END
END
GO