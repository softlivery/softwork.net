USE [MakeList]
GO

ALTER TABLE [dbo].[VGF_oAuthClientUsers] DROP CONSTRAINT [FK_oAuthClientUsers_Users]
GO

ALTER TABLE [dbo].[VGF_oAuthClientUsers] DROP CONSTRAINT [FK_oAuthClientUsers_oAuthClients]
GO

/****** Object:  Table [dbo].[VGF_oAuthClientUsers]    Script Date: 12/4/2019 3:02:52 PM ******/
DROP TABLE [dbo].[VGF_oAuthClientUsers]
GO

/****** Object:  Table [dbo].[VGF_oAuthClientUsers]    Script Date: 12/4/2019 3:02:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_oAuthClientUsers](
	[Client] [bigint] NOT NULL,
	[User] [bigint] NOT NULL,
	[Scope] [nvarchar](max) NULL,
	[Code] [nvarchar](200) NULL,
	[Access_token] [nvarchar](200) NULL,
	[Refresh_token] [nvarchar](200) NULL,
	[Delivered] [datetime] NOT NULL,
	[Expiration] [datetime] NOT NULL,
	[Address] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[VGF_oAuthClientUsers]  WITH CHECK ADD  CONSTRAINT [FK_oAuthClientUsers_oAuthClients] FOREIGN KEY([Client])
REFERENCES [dbo].[VGF_oAuthClients] ([Id])
GO

ALTER TABLE [dbo].[VGF_oAuthClientUsers] CHECK CONSTRAINT [FK_oAuthClientUsers_oAuthClients]
GO

ALTER TABLE [dbo].[VGF_oAuthClientUsers]  WITH CHECK ADD  CONSTRAINT [FK_oAuthClientUsers_Users] FOREIGN KEY([User])
REFERENCES [dbo].[VGF_Users] ([Id])
GO

ALTER TABLE [dbo].[VGF_oAuthClientUsers] CHECK CONSTRAINT [FK_oAuthClientUsers_Users]
GO


