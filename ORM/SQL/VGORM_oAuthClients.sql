USE [MakeList]
GO

/****** Object:  Table [dbo].[VGF_oAuthClients]    Script Date: 12/4/2019 3:02:29 PM ******/
DROP TABLE [dbo].[VGF_oAuthClients]
GO

/****** Object:  Table [dbo].[VGF_oAuthClients]    Script Date: 12/4/2019 3:02:29 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_oAuthClients](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[Client_id] [nvarchar](max) NOT NULL,
	[Client_secret] [nvarchar](max) NOT NULL,
	[Domain] [nvarchar](max) NULL,
	[Trusted_address] [nvarchar](max) NULL,
	[Response_url] [nvarchar](max) NULL,
	[Scope] [nvarchar](max) NULL,
	[Ttl] [bigint] NOT NULL,
	[Comments] [text] NULL,
 CONSTRAINT [PK_oAuthClients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO


