USE [MakeList]
GO

ALTER TABLE [dbo].[VGF_Menu] DROP CONSTRAINT [FK_Menu_Permissions]
GO

/****** Object:  Table [dbo].[VGF_Menu]    Script Date: 12/12/2019 12:31:18 PM ******/
DROP TABLE [dbo].[VGF_Menu]
GO

/****** Object:  Table [dbo].[VGF_Menu]    Script Date: 12/12/2019 12:31:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_Menu](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Permission] [bigint] NULL,
	[Label] [nvarchar](250) NOT NULL,
	[Icon] [nvarchar](250) NULL,
	[Action] [nvarchar](250) NULL,
	[FullIndex] [nvarchar](500) NOT NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VGF_Menu]  WITH CHECK ADD  CONSTRAINT [FK_Menu_Permissions] FOREIGN KEY([Permission])
REFERENCES [dbo].[VGF_Permissions] ([Id])
GO

ALTER TABLE [dbo].[VGF_Menu] CHECK CONSTRAINT [FK_Menu_Permissions]
GO

