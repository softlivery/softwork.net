USE [MakeList]
GO

ALTER TABLE [dbo].[VGF_Files] DROP CONSTRAINT [FK_Files_Users]
GO

/****** Object:  Table [dbo].[VGF_Files]    Script Date: 12/6/2019 1:10:34 PM ******/
DROP TABLE [dbo].[VGF_Files]
GO

/****** Object:  Table [dbo].[VGF_Files]    Script Date: 12/6/2019 1:10:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[VGF_Files](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Path] [nvarchar](500) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Extension] [nvarchar](50) NOT NULL,
	[MimeType] [nvarchar](100) NOT NULL,
	[Uploaded] [datetime] NOT NULL,
	[User] [bigint] NOT NULL,
	[Content] [varbinary](max) NOT NULL,
	[IdAssociated] [nvarchar](50) NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_Files] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[VGF_Files]  WITH CHECK ADD  CONSTRAINT [FK_Files_Users] FOREIGN KEY([User])
REFERENCES [dbo].[VGF_Users] ([Id])
GO

ALTER TABLE [dbo].[VGF_Files] CHECK CONSTRAINT [FK_Files_Users]
GO

