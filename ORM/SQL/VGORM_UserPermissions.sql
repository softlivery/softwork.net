USE [MakeList]
GO

ALTER TABLE [dbo].[VGF_UserPermissions] DROP CONSTRAINT [FK_UserPermission_Users]
GO

ALTER TABLE [dbo].[VGF_UserPermissions] DROP CONSTRAINT [FK_UserPermission_Permissions]
GO

/****** Object:  Table [dbo].[VGF_UserPermissions]    Script Date: 12/6/2019 1:12:09 PM ******/
DROP TABLE [dbo].[VGF_UserPermissions]
GO

/****** Object:  Table [dbo].[VGF_UserPermissions]    Script Date: 12/6/2019 1:12:09 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_UserPermissions](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[User] [bigint] NOT NULL,
	[Permission] [bigint] NOT NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_UserPermission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VGF_UserPermissions]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_Permissions] FOREIGN KEY([Permission])
REFERENCES [dbo].[VGF_Permissions] ([Id])
GO

ALTER TABLE [dbo].[VGF_UserPermissions] CHECK CONSTRAINT [FK_UserPermission_Permissions]
GO

ALTER TABLE [dbo].[VGF_UserPermissions]  WITH CHECK ADD  CONSTRAINT [FK_UserPermission_Users] FOREIGN KEY([User])
REFERENCES [dbo].[VGF_Users] ([Id])
GO

ALTER TABLE [dbo].[VGF_UserPermissions] CHECK CONSTRAINT [FK_UserPermission_Users]
GO

