USE [MakeList]
GO

/****** Object:  Table [dbo].[VGF_UsersGroups]    Script Date: 12/6/2019 1:12:26 PM ******/
DROP TABLE [dbo].[VGF_UsersGroups]
GO

/****** Object:  Table [dbo].[VGF_UsersGroups]    Script Date: 12/6/2019 1:12:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_UsersGroups](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_VGF_UsersGroups] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

