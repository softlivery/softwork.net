USE [MakeList]
GO

/****** Object:  Table [dbo].[VGF_MakeListNodes]    Script Date: 12/6/2019 1:10:54 PM ******/
DROP TABLE [dbo].[VGF_MakeListNodes]
GO

/****** Object:  Table [dbo].[VGF_MakeListNodes]    Script Date: 12/6/2019 1:10:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_MakeListNodes](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Parent] [bigint] NULL,
	[Value] [nvarchar](50) NULL,
	[Alias] [nvarchar](50) NULL,
	[Order] [nvarchar](50) NULL,
	[FullIndex] [nvarchar](500) NOT NULL,
	[Operator] [nvarchar](50) NULL,
	[Connector] [nvarchar](50) NULL,
	[Hidden] [nvarchar](50) NULL,
 CONSTRAINT [PK_Node] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

