USE [MakeList]
GO

ALTER TABLE [dbo].[VGF_AccessLog] DROP CONSTRAINT [FK_AccessLog_Users]
GO

/****** Object:  Table [dbo].[VGF_AccessLog]    Script Date: 12/6/2019 1:09:48 PM ******/
DROP TABLE [dbo].[VGF_AccessLog]
GO

/****** Object:  Table [dbo].[VGF_AccessLog]    Script Date: 12/6/2019 1:09:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_AccessLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Date] [datetime] NOT NULL,
	[User] [bigint] NULL,
	[Client] [bigint] NULL,
	[Path] [nvarchar](500) NOT NULL,
	[Params] [nvarchar](1000) NULL,
	[ResultCode] [nvarchar](50) NULL,
	[RequestId] [nvarchar](250) NOT NULL,
	[IP] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_VGF_AccessLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VGF_AccessLog]  WITH CHECK ADD  CONSTRAINT [FK_AccessLog_Users] FOREIGN KEY([User])
REFERENCES [dbo].[VGF_Users] ([Id])
GO

ALTER TABLE [dbo].[VGF_AccessLog] CHECK CONSTRAINT [FK_AccessLog_Users]
GO


