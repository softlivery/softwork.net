ALTER PROCEDURE [dbo].[VGF_SearchPermissions] 
	@user int,
	@permission int = null
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @level int

	SELECT @level = [Level] FROM VGF_Users WHERE [Id] = @user

	IF @permission is null
	BEGIN
		SELECT [Name]
		FROM [VGF_Permissions] P
		LEFT JOIN [VGF_UserPermissions] UP ON [UP].[Permission] = P.[Id]
		LEFT JOIN [VGF_UserLevelPermissions] ULP ON [ULP].[Permission] = P.[Id]
		WHERE [ULP].[UserLevel] = @level
		OR [UP].[User] = @user
	END
	ELSE
	BEGIN
		SELECT COUNT(1)
		FROM [VGF_Permissions] P
		LEFT JOIN [VGF_UserPermissions] UP ON [UP].[Permission] = P.[Id]
		LEFT JOIN [VGF_UserLevelPermissions] ULP ON [ULP].[Permission] = P.[Id]
		WHERE [P].[Id] = @permission
		AND ([ULP].[UserLevel] = @level OR [UP].[User] = @user)
	END
END

