USE [MakeList]
GO

ALTER TABLE [dbo].[VGF_UserLevelPermissions] DROP CONSTRAINT [FK_UserLevelPermissions_UserLevels]
GO

ALTER TABLE [dbo].[VGF_UserLevelPermissions] DROP CONSTRAINT [FK_UserLevelPermissions_Permissions]
GO

/****** Object:  Table [dbo].[VGF_UserLevelPermissions]    Script Date: 12/6/2019 1:14:28 PM ******/
DROP TABLE [dbo].[VGF_UserLevelPermissions]
GO

/****** Object:  Table [dbo].[VGF_UserLevelPermissions]    Script Date: 12/6/2019 1:14:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[VGF_UserLevelPermissions](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserLevel] [bigint] NOT NULL,
	[Permission] [bigint] NOT NULL,
	[Comments] [nvarchar](500) NULL,
 CONSTRAINT [PK_UserLevelPermissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[VGF_UserLevelPermissions]  WITH CHECK ADD  CONSTRAINT [FK_UserLevelPermissions_Permissions] FOREIGN KEY([Permission])
REFERENCES [dbo].[VGF_Permissions] ([Id])
GO

ALTER TABLE [dbo].[VGF_UserLevelPermissions] CHECK CONSTRAINT [FK_UserLevelPermissions_Permissions]
GO

ALTER TABLE [dbo].[VGF_UserLevelPermissions]  WITH CHECK ADD  CONSTRAINT [FK_UserLevelPermissions_UserLevels] FOREIGN KEY([UserLevel])
REFERENCES [dbo].[VGF_UserLevels] ([Id])
GO

ALTER TABLE [dbo].[VGF_UserLevelPermissions] CHECK CONSTRAINT [FK_UserLevelPermissions_UserLevels]
GO

