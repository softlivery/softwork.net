﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace VGFramework.Configuration
{
    class ConfigDB : Data.ORM
    {
        public ConfigDB(Manager Manager) : base(Manager, Manager.GetConfig("ParametersTableName"))
        {
            string keyName = Manager.GetConfig("ParametersKeyName");
            string valueName = Manager.GetConfig("ParametersValueName");
            string key;
            object value;
            DataTable configs = Load(string.Format("SELECT * FROM [{0}]", table));
            if(configs.Rows.Count > 0)
            {
                foreach(var row in configs.AsEnumerable())
                {
                    Manager.SetConfig(row[keyName].ToString(), row[valueName].ToString());
                    //key = row[keyName].ToString();
                    //value = Data.Column.ValueFromDB("nvarchar", row[valueName]);
                    //Manager.SetConfig(key, (string)value);
                }
            }
            /*
            SqlDataReader reader = Query(string.Format("SELECT * FROM [{0}]", table));

            //parse rows
            if (reader != null)
            {
                while (!reader.IsClosed && reader.Read())
                {
                    key = reader[keyName].ToString();
                    value = Data.Column.ValueFromDB("nvarchar", reader[valueName]);
                    Manager.SetConfig(key, (string)value);
                }
                reader.Close();
            }
            */
        }
    }
}
