﻿using System;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using System.Linq;

namespace VGFramework.Renders
{
    class HTML : Render
    {
        public HTML()
        {
        }

        public object Decode(string encoded)
        {
            return HttpUtility.ParseQueryString(encoded);
        }

        public string Encode(object decoded)
        {
            return "";
        }

        public void Error(string msg)
        {
            Regex internal_errors = new Regex(@"^[a-zA-Z]{3}[0-9]{4}$");
            if (internal_errors.IsMatch(msg) && System.IO.File.Exists(WebManager.Instance.GetConfig("SiteRoot") + @"locale\es_errors.csv"))
            {
                //Logger.Debug("Traducir mensaje: " + msg);
                var reader = new StreamReader(File.OpenRead(WebManager.Instance.GetConfig("SiteRoot") + @"locale\es_errors.csv"));
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    if (values[0].Equals(msg) && !values[1].Equals(""))
                    {
                        //msg = String.Format("({0}) {1}", msg, values[1]);
                        msg = values[1];
                    }
                }
            }

            WebManager.Instance.Response.Set("error", msg);
            if (WebManager.Instance.Template == null)
            {
                WebManager.Instance.Template = "error";
            }

            Render(WebManager.Instance);
        }

        public void Render(WebManager Manager)
        {
            if (WebManager.Instance.StatusCode >= 400)
            {
                WebManager.Instance.Template = "errors/" + WebManager.Instance.StatusCode;
                WebManager.Instance.StatusCode = 200;
            }

            if (!File.Exists(WebManager.Instance.GetConfig("SiteRoot") + @"sections\" + WebManager.Instance.Template + ".html"))
            {
                Manager.LogError("Template no encontrado: sections/" + WebManager.Instance.Template + ".html");
                WebManager.Instance.Template = null;
                throw new Exception("GEN0003");
            }

            if (WebManager.Instance.Template != null)
            {
                WebManager.Instance.ResponseHTTP.Write(ParseFile(WebManager.Instance.Template));
            }
        }

        protected string ParseFile(string template)
        {
            TemplateParser parser = new TemplateParser(WebManager.Instance);
            return parser.parseFile(template, WebManager.Instance.Response);
        }
    }
}