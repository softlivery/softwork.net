﻿using System;
using System.IO;
using System.Text;

namespace VGFramework.Renders
{
    internal class PDF : Render
    {
        public PDF() : base()
        {
        }

        public object Decode(string encoded)
        {
            throw new System.NotImplementedException();
        }

        public string Encode(object decoded)
        {
            throw new System.NotImplementedException();
        }

        public void Error(string msg)
        {
            throw new System.NotImplementedException();
        }

        public void Render(WebManager Manager)
        {
            if (WebManager.Instance.StatusCode >= 400)
            {
                WebManager.Instance.Template = "errors/" + WebManager.Instance.StatusCode;
                WebManager.Instance.StatusCode = 200;
            }

            if (!File.Exists(WebManager.Instance.GetConfig("SiteRoot") + @"sections\" + WebManager.Instance.Template + ".html"))
            {
                Manager.LogError("Template no encontrado: sections/" + WebManager.Instance.Template + ".html");
                WebManager.Instance.Template = null;
                throw new Exception("GEN0003");
            }

            if (WebManager.Instance.Template != null)
            {
                WebManager.Instance.ResponseHTTP.ContentType = "application/pdf";
                WebManager.Instance.ResponseHTTP.BinaryWrite(ParsePDF(WebManager.Instance.Template));
                //WebManager.Instance.ResponseHTTP.Flush();
                //WebManager.Instance.ResponseHTTP.Close();
            }
        }

        private byte[] ParsePDF(string template)
        {
            //https://www.nrecosite.com/pdf_generator_net.aspx
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            return htmlToPdf.GeneratePdf(ParseFile(template));
        }

        protected string ParseFile(string template)
        {
            TemplateParser parser = new TemplateParser(WebManager.Instance);
            return parser.parseFile(template, WebManager.Instance.Response);
        }
    }
}
