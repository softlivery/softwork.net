﻿using Newtonsoft.Json;
using System;
using System.Dynamic;
using Newtonsoft.Json.Linq;

namespace VGFramework.Renders
{
    class Json : Render
    {

        public Json()
        {
        }
        public object Decode(string encoded)
        {
            return JObject.Parse(encoded);
        }

        public string Encode(object decoded)
        {
            return JsonConvert.SerializeObject(decoded);
        }

        public void Error(string msg)
        {
            WebManager.Instance.Response.Set("error", msg);
            string res = Encode(WebManager.Instance.Response.All());
            try
            {
                WebManager.Instance.ResponseHTTP.Write(res);
            }
            catch (Exception e)
            {
                WebManager.Instance.LogError(e.Message);
            }
        }

        public void Render(WebManager Manager)
        {
            WebManager.Instance.ResponseHTTP.ContentType = "application/json";
            string res = Encode(WebManager.Instance.Response.All());
            try
            {
                WebManager.Instance.ResponseHTTP.Write(res);
            }
            catch (Exception e)
            {
                WebManager.Instance.LogError(e.Message);
                Error(e.Message);
            }
        }
    }

    class JsonMemberBinder : GetMemberBinder
    {
        public JsonMemberBinder(string name, bool ignoreCase)
            : base(name, ignoreCase)
        {
        }

        public override DynamicMetaObject FallbackGetMember(DynamicMetaObject target, DynamicMetaObject errorSuggestion)
        {
            throw new NotImplementedException();
        }
    }
}
