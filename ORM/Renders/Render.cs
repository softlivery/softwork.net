﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.Renders
{
    public interface Render
    {
        object Decode(string encoded);

        string Encode(object decoded);

        void Render(WebManager Manager);

        void Error(string msg);
    }
}
