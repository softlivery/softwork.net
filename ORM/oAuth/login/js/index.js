document.getElementById("iconMostrar").addEventListener("click", function () {
    let elementInput = document.getElementById("inputPassword");
    let elementIcon = document.getElementById("iconMostrar");
    if (elementIcon.classList.contains("active")) {
        elementIcon.classList.remove("active");
        elementIcon.innerHTML = "visibility";
        elementInput.type = "password";
    } else {
        elementIcon.classList.add("active");
        elementIcon.innerHTML = "visibility_off";
        elementInput.type = "text";
    }
});

const user = mdc.textField.MDCTextField.attachTo(document.querySelector('.userS'));
const pass = mdc.textField.MDCTextField.attachTo(document.querySelector('.passS'));

const button = document.getElementById("submitButton")
button.disabled = true

function checkform() {
    if (user.foundation_.isValid() == true && pass.foundation_.isValid() == true) {
        button.disabled = false
    } else {
        button.disabled = true
    }
}