﻿// Get Querystring Parameters
function getQueryStringParam(key) {
    var vars = [], hash;
    var q = document.URL.split('?')[1];
    if (q != undefined) {
        q = q.split('&');
        for (var i = 0; i < q.length; i++) {
            hash = q[i].split('=');
            vars.push(hash[1]);
            vars[hash[0]] = hash[1];
        }
    }
    if (vars[key] != undefined) { return vars[key]; }
    else { return "" }
};

function setValues(...ids) {
    for (var i = 0; i < arguments.length; i++) {

        document.getElementById(arguments[i]).value = getQueryStringParam(arguments[i]);
    };
};
