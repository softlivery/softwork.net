﻿namespace VGFramework.oAuth
{
    public class ClientUser : Data.InternalORM
    {
        public ClientUser(int client, string user) : base(OAuthManager.Instance, "oAuthClientUsers", true)
        {
            columns["Client"].Value = client;
            columns["User"].Value = user;

            //keys.Add("Client");
            //keys.Add("User");

            Load();
        }

        public ClientUser(string token, bool refresh = false) : base(OAuthManager.Instance, "oAuthClientUsers", true)
        {
            if(refresh)
                columns["Refresh_token"].Value = token;
            else
                columns["Access_token"].Value = token;

            //keys.Add("Client");
            //keys.Add("User");

            Load();
        }

        public ClientUser(string code) : base(OAuthManager.Instance, "oAuthClientUsers", true)
        {
            columns["Code"].Value = code;

            //keys.Add("Client");
            //keys.Add("User");

            Load();
        }

        public void AddKeys()
        {
            keys.Add("Client");
            keys.Add("User");
        }
    }
}
