﻿namespace VGFramework.oAuth
{
    internal class Client : Data.InternalORM
    {
        public Client(int id) : base(OAuthManager.Instance, "oAuthClients", true)
        {
            columns["Id"].Value = id;
            Load();
        }

        public Client(string client_id) : base(OAuthManager.Instance, "oAuthClients", true)
        {
            columns["Client_id"].Value = client_id;
            Load();
        }
    }
}
