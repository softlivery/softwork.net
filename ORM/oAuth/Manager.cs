﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework.oAuth
{
    /*
    * Parameters:
    * -	(required) client_id: client identifier
    * -	(optional) state: used for CSRF Protection
    * -	(required) scope: https://tools.ietf.org/html/rfc6749#section-3.3
    * - (required) grant_type, https://tools.ietf.org/html/rfc6749#section-1.3 https://tools.ietf.org/html/rfc6749#section-4:
        * - authorization_code: code for exchange an access_token https://tools.ietf.org/html/rfc6749#section-4.1
        * - implicit: obtain an access_token directly (NOT RECOMENDED) https://tools.ietf.org/html/rfc6749#section-4.2
        * - resource owner password (password): obtain an access_token directly from user/password https://tools.ietf.org/html/rfc6749#section-4.3
        * - client credentials (client): obtain an access_token with client_id and client_secret https://tools.ietf.org/html/rfc6749#section-4.4
    * -	(required) response_type, https://tools.ietf.org/html/rfc6749#section-3.1.1:
        * - code: used for authorization_code grant type.
        * - token: used for implicit grant type.
    * -	(optional) redirect_uri: registered redirection endpoint
    *
    * Success Response:
        * -	code or access_token: authorization_code or access_token
        * - token_type: "bearer" https://tools.ietf.org/html/rfc6749#section-7.1
        * - expires_in: lifetime of access_token, default 3600 secs
        * - refresh_token: https://tools.ietf.org/html/rfc6749#section-6
        * - scope: granted scopes
        * -	(optional) state: request state variable
    *
    * Error Response, https://tools.ietf.org/html/rfc6749#section-5.2:
    * - error:
     * - invalid_request
     * - invalid_client
     * - invalid_grant
     * - unauthorized_client
     * - unsupported_grant_type
     * - invalid_scope
    * - error_description
    * - error_uri
    */
    class Manager
    {
        private static Random random = new Random();

        public static void Validate()
        {
            OAuthManager.Instance.LogActivity("oAuth - Validate");

            string auth = OAuthManager.Instance.Route.auth.ToString();
            string token = OAuthManager.Instance.QueryParameters.Get("access_token", "").ToString();
            string client_id = OAuthManager.Instance.QueryParameters.Get("client_id", "").ToString();

            if (token.Equals(""))
            {
                OAuthManager.Instance.LogError("oAuth - Token no encontrado");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //verificar cliente
            if (client_id.Equals(""))
            {
                OAuthManager.Instance.LogError("oAuth - Client_id requerido");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic client = new Client(client_id);
            if (client.Empty())
            {
                OAuthManager.Instance.LogError("oAuth - client_id no valido " + client_id);
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            OAuthManager.Instance.Client = client;
            OAuthManager.Instance.LogActivity("oAuth Client loaded " + client.Description.ToString());

            //verificar scope a nivel cliente
            string[] clientScopes = client.Scope.Split(' ');
            if (!client.Scope.ToString().Equals("All") && !clientScopes.Any(x => x.Equals(OAuthManager.Instance.Route.name)))
            {
                OAuthManager.Instance.LogError("oAuth - scope no valido");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //verificar Domain
            string domain = OAuthManager.Instance.RequestHTTP.UserHostAddress;
            if (client.Domain != null && !client.Domain.ToString().Equals("") && client.Domain.Split('/')[0] != domain)
            {
                OAuthManager.Instance.LogError("oAuth - domain no valido " + domain + " | request " + client.Domain);
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            switch (auth.ToLower())
            {
                case "user":
                    ValidateUser(client, token);
                    break;
                case "client":
                    ValidateClient(client, token);
                    break;
                case "both":
                    if (!validateClientToken(client, token)) ValidateUser(client, token);
                    break;
            }

            OAuthManager.Instance.QueryParameters.Remove("access_token");
            OAuthManager.Instance.QueryParameters.Remove("client_id");
        }

        private static void ValidateUser(dynamic client, string token)
        {
            OAuthManager.Instance.LogActivity("oAuth - ValidateUser");

            dynamic cu = new ClientUser(token, false);
            if (cu.Empty())
            {
                OAuthManager.Instance.LogError("oAuth - token no encontrado " + token);
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            OAuthManager.Instance.ClientUser = cu;
            OAuthManager.Instance.User = OAuthManager.Instance.InternalORM("Users");
            OAuthManager.Instance.User.Id = OAuthManager.Instance.ClientUser.User;
            if (!OAuthManager.Instance.User.Load())
                throw new Exception("User cannot be loaded");

            //verificar scope a nivel usuario
            string[] userScopes = cu.Scope.Split(' ');
            if (OAuthManager.Instance.Route.auth.Equals("user") && !cu.Scope.ToString().Equals("All") && !userScopes.Any(x => x.Equals(OAuthManager.Instance.Route.name)))
            {
                OAuthManager.Instance.LogError("oAuth - scope no valido");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //verificar combinacion cliente-token
            if ((int)client.Id != (int)OAuthManager.Instance.ClientUser.Client)
            {
                OAuthManager.Instance.LogError(string.Format("oAuth - access_token (para client_id {1}) no pertenece a client_id {0}", OAuthManager.Instance.ClientUser.Client, client.Id));
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //check expiration
            DateTime expiration = DateTime.ParseExact(OAuthManager.Instance.ClientUser.Expiration, OAuthManager.Instance.GetConfig("DateTimeFormat"), null);
            if (expiration < DateTime.Now)
            {
                OAuthManager.Instance.LogError("oAuth - token expirado " + OAuthManager.Instance.ClientUser.Expiration.ToString());
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            OAuthManager.Instance.Session.Replace((Dictionary<string, string>)OAuthManager.Instance.User.ToSession());
        }

        private static void ValidateClient(dynamic client, string token)
        {
            OAuthManager.Instance.LogActivity("oAuth - ValidateClient");
            
            if (!validateClientToken(client, token))
            {
                OAuthManager.Instance.LogError("oAuth - token invalido");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }
        }

        private static bool validateClientToken(dynamic client, string token)
        {
            string expected = GenerateSaltedHash(client.Client_id.ToString(), client.Client_secret.ToString());

            return expected.TrimEnd('=').Equals(token.TrimEnd('='));
        }

        /*
        * Parameters:
        * -	(required) client_id: client identifier
        * -	(optional) state: used for CSRF Protection
        * -	(required) scope: https://tools.ietf.org/html/rfc6749#section-3.3
        * - (required) grant_type, https://tools.ietf.org/html/rfc6749#section-1.3 https://tools.ietf.org/html/rfc6749#section-4:
        *   - authorization_code: code for exchange an access_token https://tools.ietf.org/html/rfc6749#section-4.1
        *   - implicit: obtain an access_token directly (NOT RECOMENDED) https://tools.ietf.org/html/rfc6749#section-4.2
        *   - resource owner password (password): obtain an access_token directly from user/password https://tools.ietf.org/html/rfc6749#section-4.3
        *   - client credentials (client): obtain an access_token with client_id and client_secret https://tools.ietf.org/html/rfc6749#section-4.4
        *   - refresh_token: renew an access_token https://tools.ietf.org/html/rfc6749#section-6
        * -	(required) response_type, https://tools.ietf.org/html/rfc6749#section-3.1.1:
        *   - code: used for authorization_code grant type.
        *   - token: used for implicit grant type.
        *   - refresh_token: used for refresh_token grant type
        * -	(optional) redirect_uri: registered redirection endpoint
        */
        public static void Authorize(OAuthManager manager)
        {
            OAuthManager.Instance.LogDebug("oAuth.Manager.Login");

            dynamic client = ValidateParameters();
            
            if (OAuthManager.Instance.isMethod("get"))
            {
                OAuthManager.Instance.Response.Set("state", OAuthManager.Instance.QueryParameters.Get("state", ""));
                OAuthManager.Instance.Response.Set("client_id", OAuthManager.Instance.QueryParameters.Get("client_id", ""));
                OAuthManager.Instance.Response.Set("scope", OAuthManager.Instance.QueryParameters.Get("scope", "All"));
                OAuthManager.Instance.Response.Set("response_type", OAuthManager.Instance.QueryParameters.Get("response_type", ""));
                OAuthManager.Instance.Response.Set("redirect_uri", OAuthManager.Instance.QueryParameters.Get("redirect_uri", ""));
                OAuthManager.Instance.Response.Set("grant_type", OAuthManager.Instance.QueryParameters.Get("grant_type", ""));
            }
            
            if (OAuthManager.Instance.isMethod("post"))
            {
                string state = OAuthManager.Instance.Parameters.Get("state", "").ToString();
                string grant_type = OAuthManager.Instance.Parameters.Get("grant_type", "").ToString();
                switch (grant_type.ToLower())
                {
                    case "authorization_code":
                        AuthorizationCode(client, state);
                        break;
                    case "resource_owner_password":
                        throw new Exception("Not Implementd");
                    case "client_credentials":
                        Client(client, state);
                        break;
                    case "implicit":
                        Implicit(client, state);
                        break;
                    case "refresh_token":
                        Refresh(client);
                        break;
                }
                
                string response_url = client.Response_url != null ? client.Response_url : "";
                if(!OAuthManager.Instance.Parameters.Get("redirect_uri", response_url).ToString().Equals(""))
                    response_url = OAuthManager.Instance.Parameters.Get("redirect_uri", response_url).ToString();
                if (response_url != null && !response_url.Equals("") && grant_type != "refresh_token")
                {
                    OAuthManager.Instance.Response.Set("redirect", response_url);
                }
            }
        }

        public static void Token(OAuthManager manager)
        {
            string state = OAuthManager.Instance.Parameters.Get("state", "").ToString();
            string code = OAuthManager.Instance.Parameters.Get("code", "").ToString();
            string client_id = OAuthManager.Instance.Parameters.Get("client_id", "").ToString();
            string client_secret = OAuthManager.Instance.Parameters.Get("client_secret", "").ToString();

            if (client_id.Equals(""))
            {
                OAuthManager.Instance.LogError("oAuth - client_id no especificado");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic client = new Client(client_id);
            if (client.Empty())
            {
                OAuthManager.Instance.LogError("oAuth - client_id no valido " + client_id);
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            if (client_secret != client.Client_secret)
            {
                OAuthManager.Instance.LogError("oAuth - Authorization code: client_secret incorrecto");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic cu = new ClientUser(code);

            DateTime expiration = DateTime.ParseExact(OAuthManager.Instance.ClientUser.Expiration, OAuthManager.Instance.GetConfig("DateTimeFormat"), null);
            if (expiration < DateTime.Now)
            {
                OAuthManager.Instance.LogError("oAuth - code expirado " + cu.Expiration.ToString());
                throw new Exception("invalid_request");
            }

            if (code != cu.Code)
            {
                OAuthManager.Instance.LogError("oAuth - Authorization code: code no coincide");
                throw new Exception("invalid_request");
            }
            
            if (client.Client_id.ToString() != client_id)
            {
                OAuthManager.Instance.LogError("oAuth - Authorization code: code no pertenece a client_id");
                throw new Exception("invalid request");
            }

            cu.Client = client.Id;
            cu.Code = null;
            cu.Delivered = DateTime.Now;
            cu.Expiration = getExpiration(cu.Delivered.ToString("yyyyMMddHHmmssffff"), client.Ttl.ToString());
            cu.Access_token = RandomString(150);
            cu.Refresh_token = RandomString(150);
            cu.AddKeys();
            if (!cu.Save())
            {
                OAuthManager.Instance.LogError("oAuth - Authorization code: client_user no guardado");
                throw new Exception("invalid_request");
            }

            OAuthManager.Instance.Response.Set("client_id", client.Client_id.ToString());
            //OAuthManager.Instance.Response.Set("delivered", cu.delivered.ToString());
            //OAuthManager.Instance.Response.Set("expire", cu.expiration.ToString());
            OAuthManager.Instance.Response.Set("life_time", client.Ttl);
            OAuthManager.Instance.Response.Set("access_token", cu.Access_token.ToString());
            OAuthManager.Instance.Response.Set("refresh_token", cu.Refresh_token.ToString());
            OAuthManager.Instance.Response.Set("token_type", "bearer");
            OAuthManager.Instance.Response.Set("scope", cu.Scope.ToString());
            OAuthManager.Instance.Response.Set("state", state);
            OAuthManager.Instance.Response.Set("redirect", client.Response_url);
        }

        private static Client ValidateParameters()
        {
            string client_id = "";
            string[] requestedScopes = new string[] { "" };
            string uri = "";
            string grant_type = "";
            string response_type = "";

            if (OAuthManager.Instance.isMethod("get"))
            {
                client_id = OAuthManager.Instance.QueryParameters.Get("client_id", "");
                requestedScopes = OAuthManager.Instance.QueryParameters.Get("scope", "All").Split(' ');
                uri = OAuthManager.Instance.QueryParameters.Get("redirect_uri", "");
                grant_type = OAuthManager.Instance.QueryParameters.Get("grant_type", "");
                response_type = OAuthManager.Instance.QueryParameters.Get("response_type", "");
            }

            if (OAuthManager.Instance.isMethod("post"))
            {
                client_id = OAuthManager.Instance.Parameters.Get("client_id", "").ToString();
                requestedScopes = OAuthManager.Instance.Parameters.Get("scope", "All").ToString().Split(' ');
                uri = OAuthManager.Instance.Parameters.Get("redirect_uri", "").ToString();
                grant_type = OAuthManager.Instance.Parameters.Get("grant_type", "").ToString();
                response_type = OAuthManager.Instance.Parameters.Get("response_type", "").ToString();
            }

            //validar cliente (requerido)
            if (client_id.Equals(""))
            {
                OAuthManager.Instance.LogError("oAuth - client_id no especificado");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic client = new Client(client_id);
            if (client.Empty())
            {
                OAuthManager.Instance.LogError("oAuth - client_id no valido " + client_id);
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //validar dominio
            string domain = OAuthManager.Instance.RequestHTTP.UserHostAddress;
            if (client.Domain != null && !client.Domain.ToString().Equals("") && client.Domain.Split('/')[0] != domain)
            {
                OAuthManager.Instance.LogError("oAuth - domain no valido " + domain + " | request " + client.Domain);
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            //validar scope (requerido)
            string[] scopes = client.Scope.ToString().Split(' ');
            foreach (string scope in requestedScopes)
            {
                if (!scope.Equals("") && !scope.Equals("All") && !scopes.Any(x => x.ToString() == scope))
                {
                    OAuthManager.Instance.LogError("oAuth - scope no valido");
                    OAuthManager.Instance.StatusCode = 407;
                    throw new Exception("invalid_request");
                }
            }

            //validar redirect Uri
            Uri requestedRedirectURI = null;
            if (client.Domain != null && !client.Domain.ToString().Equals("") && uri != "")
            {
                requestedRedirectURI = new Uri(uri);
                if (client.Domain.Split('/')[0] != requestedRedirectURI.Host)
                {
                    OAuthManager.Instance.LogError("oAuth - redirectURI no valido");
                    OAuthManager.Instance.StatusCode = 407;
                    throw new Exception("invalid_request");
                }
            }

            //validar grant_type (requerido)
            if (grant_type != "authorization_code" && 
                grant_type != "resource_owner_password" &&
                grant_type != "client_credentials" &&
                grant_type != "implicit" &&
                grant_type != "refresh_token")
            {
                OAuthManager.Instance.LogError("oAuth - grant_type no válido");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid request");
            }

            //validar response_type (requerido)
            if (response_type != "code" && response_type != "token" && response_type != "refresh_token")
            {
                OAuthManager.Instance.LogError("oAuth - response_type no válido");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid request");
            }

            return client;
        }

        private static void AuthorizationCode(dynamic client, string state)
        {
            OAuthManager.Instance.LogActivity("oAuth - Authorization-Code");
            string response_type = OAuthManager.Instance.Parameters.Get("response_type", "code").ToString();

            string user = OAuthManager.Instance.Parameters.Get("user", "").ToString();
            string password = OAuthManager.Instance.Parameters.Get("password", "").ToString();

            if (user.Equals(""))
            {
                OAuthManager.Instance.LogError("oAuth - user no especificado");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            if (!response_type.Equals("code"))
            {
                OAuthManager.Instance.LogError("oAuth - Response Type debe ser: code");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }
            
            dynamic User = OAuthManager.Instance.InternalORM("Users");
            User.Name = user;
            User.Password = password;
            if (!User.Load())
            {
                OAuthManager.Instance.LogError("oAuth - Usuario no válido");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }
            
            dynamic cu = new ClientUser((int)client.Id, (User.Id).ToString());

            if (!cu.Empty())
            {
                OAuthManager.Instance.ClientUser = cu;
                //verifico si el code guardado aun esta activo
                DateTime expiration = DateTime.ParseExact(OAuthManager.Instance.ClientUser.Expiration, OAuthManager.Instance.GetConfig("DateTimeFormat"), null);
                if (expiration > DateTime.Now && cu.Code != null)
                {
                    OAuthManager.Instance.LogDebug("oAuth - Authorization code: code vigente");
                    OAuthManager.Instance.Response.Set("code", cu.Code.ToString());
                    OAuthManager.Instance.Response.Set("state", state);
                    return;
                }
            }
            
            cu.Client = client.Id;
            cu.User = (User.Id).ToString();
            cu.Scope = client.Scope;
            cu.Delivered = DateTime.Now;
            cu.Expiration = getExpiration(cu.Delivered.ToString("yyyyMMddHHmmssffff"), "5");
            cu.Code = RandomString(150);
            cu.Access_token = null;
            cu.Refresh_token = null;
            cu.Address = OAuthManager.Instance.RequestHTTP.UserHostAddress;
            if (!cu.Save())
            {
                OAuthManager.Instance.LogError("oAuth - Authorization code: client_user no guardado");
                throw new Exception("invalid_request");
            }

            OAuthManager.Instance.ClientUser = cu;
            OAuthManager.Instance.Response.Set("code", cu.Code.ToString());
            OAuthManager.Instance.Response.Set("state", state);
        }

        private static void Implicit(dynamic client, string state)
        {
            OAuthManager.Instance.LogActivity("oAuth - Implicit");
            string response_type = OAuthManager.Instance.Parameters.Get("response_type", "token").ToString();

            string user = OAuthManager.Instance.Parameters.Get("user", "").ToString();
            string password = OAuthManager.Instance.Parameters.Get("password", "").ToString();

            if (user.Equals("") || password.Equals(""))
            {
                OAuthManager.Instance.LogError("oAuth - user no especificado");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            if (!response_type.Equals("token"))
            {
                OAuthManager.Instance.LogError("oAuth - Response Type debe ser: token");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic User = Helpers.Users.Login(user, password);

            if(User == null)
            {
                OAuthManager.Instance.LogError("oAuth - Invalid credentials");
                OAuthManager.Instance.StatusCode = 401;
                throw new Exception("invalid_client");
            }

            dynamic cu = new ClientUser((int)client.Id, (User.Id).ToString());

            if (!cu.Empty())
            {
                OAuthManager.Instance.ClientUser = cu;
                //verifico si el token guardado aun esta activo
                DateTime expiration = DateTime.ParseExact(OAuthManager.Instance.ClientUser.Expiration, OAuthManager.Instance.GetConfig("DateTimeFormat"), null);
                if (expiration.AddMinutes(-5) > DateTime.Now && cu.Access_token != null)
                {
                    OAuthManager.Instance.LogDebug("oAuth - Implicit: token vigente");
                    OAuthManager.Instance.Response.Set("client_id", client.Client_id.ToString());
                    OAuthManager.Instance.Response.Set("life_time", client.Ttl);
                    OAuthManager.Instance.Response.Set("access_token", cu.Access_token.ToString());
                    OAuthManager.Instance.Response.Set("refresh_token", cu.Refresh_token.ToString());
                    OAuthManager.Instance.Response.Set("token_type", "bearer");
                    OAuthManager.Instance.Response.Set("scope", cu.Scope.ToString());
                    OAuthManager.Instance.Response.Set("state", state);

                    return;
                }
                cu.AddKeys();
            }

            DateTime delivered = DateTime.Now;

            cu.Client = client.Id;
            cu.User = User.Id.ToString();
            cu.Scope = client.Scope;
            cu.Delivered = delivered;
            cu.Expiration = getExpiration(delivered.ToString("yyyyMMddHHmmssffff"), client.Ttl.ToString());
            cu.Code = null;
            cu.Access_token = RandomString(150);
            cu.Refresh_token = RandomString(150);
            cu.Address = OAuthManager.Instance.RequestHTTP.UserHostAddress;

            if (!cu.Save())
            {
                OAuthManager.Instance.LogError("oAuth - Implicit: client_user no guardado");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            OAuthManager.Instance.ClientUser = cu;
            OAuthManager.Instance.Response.Set("client_id", client.Client_id.ToString());
            //OAuthManager.Instance.Response.Set("delivered", cu.delivered.ToString());
            //OAuthManager.Instance.Response.Set("expire", cu.expiration.ToString());
            OAuthManager.Instance.Response.Set("life_time", client.Ttl);
            OAuthManager.Instance.Response.Set("access_token", cu.Access_token.ToString());
            OAuthManager.Instance.Response.Set("refresh_token", cu.Refresh_token.ToString());
            OAuthManager.Instance.Response.Set("token_type", "bearer");
            OAuthManager.Instance.Response.Set("scope", cu.Scope.ToString());
            OAuthManager.Instance.Response.Set("state", state);
        }

        private static void Client(dynamic client, string state)
        {
            string client_secret = OAuthManager.Instance.Parameters.Get("client_secret", "").ToString();

            //validar secret (requerido)
            if (!client_secret.Equals(client.Client_secret.ToString()))
            {
                OAuthManager.Instance.LogError("oAuth - client_secret invalido");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            OAuthManager.Instance.Response.Set("client_id", client.Client_id.ToString());
            OAuthManager.Instance.Response.Set("access_token", GenerateSaltedHash(client.Client_id.ToString(), client.Client_secret.ToString()));
            OAuthManager.Instance.Response.Set("token_type", "bearer");
            OAuthManager.Instance.Response.Set("scope", "client");
            OAuthManager.Instance.Response.Set("state", state);
        }

        private static void Refresh(dynamic client)
        {
            OAuthManager.Instance.LogActivity("oAuth - Refresh");
            string state = OAuthManager.Instance.Parameters.Get("state", "").ToString();
            string token = OAuthManager.Instance.Parameters.Get("refresh_token", "").ToString();

            if (token.Equals(""))
            {
                OAuthManager.Instance.LogError("oAuth - Token no encontrado");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            dynamic cu = new ClientUser(token, true);

            if (cu.Empty())
            {
                OAuthManager.Instance.LogError("oAuth - token no encontrado " + token);
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            if (!cu.Client.ToString().Equals(client.Id.ToString()))
            {
                OAuthManager.Instance.LogError("oAuth - Token no corresponde con Cliente");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }
        
            cu.Client = client.Id;
            cu.User = cu.User;
            cu.Scope = client.Scope;
            cu.Delivered = DateTime.Now;
            cu.Expiration = getExpiration(DateTime.Now.ToString("yyyyMMddHHmmssffff"), client.Ttl.ToString());
            cu.Access_token = RandomString(150);
            cu.Refresh_token = RandomString(150);
            cu.Address = OAuthManager.Instance.RequestHTTP.UserHostAddress;
            cu.AddKeys();
            if (!cu.Save())
            {
                OAuthManager.Instance.LogError("oAuth - Refresh: client_user no guardado");
                OAuthManager.Instance.StatusCode = 407;
                throw new Exception("invalid_request");
            }

            OAuthManager.Instance.ClientUser = cu;
            OAuthManager.Instance.Response.Set("client_id", client.Client_id.ToString());
            //OAuthManager.Instance.Response.Set("delivered", cu.delivered.ToString());
            //OAuthManager.Instance.Response.Set("expire", cu.expiration.ToString());
            OAuthManager.Instance.Response.Set("life_time", client.Ttl);
            OAuthManager.Instance.Response.Set("access_token", cu.Access_token.ToString());
            OAuthManager.Instance.Response.Set("refresh_token", cu.Refresh_token.ToString());
            OAuthManager.Instance.Response.Set("token_type", "bearer");
            OAuthManager.Instance.Response.Set("scope", cu.Scope.ToString());
            OAuthManager.Instance.Response.Set("state", state);
        }

        private static DateTime getExpiration(string delivered, string ttl)
        {
            DateTime delivered_date = DateTime.ParseExact(delivered, "yyyyMMddHHmmssffff", System.Globalization.CultureInfo.InvariantCulture);
            double ttl_num;
            if (!double.TryParse(ttl, out ttl_num))
            {
                ttl_num = 0;
            }
            return delivered_date.AddMinutes(ttl_num);
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GenerateSaltedHash(string text, string salt)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(text + salt);
            return Convert.ToBase64String(plainTextBytes);
        }
    }
}
