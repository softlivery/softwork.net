﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VGFramework
{
    public class ParametersBag<T>
    {
        private Dictionary<string, T> parameters;

        public int Count => parameters.Count;

        public ParametersBag()
        {
            parameters = new Dictionary<string, T>();
        }

        public object this[string key]
        {
            get => this.Get(key, null);
            set => this.Set(key, (T)value);
        }

        public bool Set(string key, T value)
        {
            try
            {
                if (parameters.ContainsKey(key))
                {
                    parameters[key] = value;
                }
                else
                {
                    parameters.Add(key, value);
                }
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public T Get(string key, T alternative)
        {
            if(parameters.ContainsKey(key))
            {
                return parameters[key];
            }
            return alternative;
        }

        public object Get(string key, object alternative)
        {
            if (parameters.ContainsKey(key))
            {
                return parameters[key];
            }
            return alternative;
        }

        public bool Remove(string key)
        {
            return parameters.Remove(key);
        }

        public bool Has(string key)
        {
            return parameters.ContainsKey(key);
        }

        public Dictionary<string, T> All()
        {
            return parameters;
        }

        public Dictionary<string, T>.KeyCollection Keys()
        {
            return parameters.Keys;
        }

        public bool Replace(Dictionary<string, T> values)
        {
            try
            {
                parameters = values;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public void Clear()
        {
            parameters.Clear();
        }
    }
}
