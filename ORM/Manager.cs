﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace VGFramework
{
    public class Manager
    {
        private static volatile Manager instance;
        protected string dir;
        private Logger logger;
        private NameValueCollection CurrentSettings;
        private NameValueCollection DefaultSettings;
        private Configuration.ConfigDB DBSettings;

        public static Manager Instance
        {
            get
            {
                if (instance == null)
                    instance = new Manager();

                return instance;
            }
        }

        public Manager(string Dir)
        {
            dir = Dir.Trim();
            Initialize();
        }

        public Manager()
        {
            dir = AppDomain.CurrentDomain.BaseDirectory;
            Initialize();
        }

        protected void LoadDefaultSettings()
        {
            DefaultSettings = new NameValueCollection
            {
                { "ConnectionString", "data source=.;initial catalog=VGFramework;user id=sa;password=123456;Persist Security Info=True" },
                { "ParametersTableName", "Parameters" },
                { "ParametersKeyName", "key" },
                { "ParametersValueName", "value" },
                { "DateFormat", "MM/dd/yyyy" },
                { "DateTimeFormat", "MM/dd/yyyy HH:mm:ss" },
                { "LogLevels", "EADS" }
            };
        }

        private void Initialize()
        {
            LoadDefaultSettings();

            try
            {
                CurrentSettings = new NameValueCollection(ConfigurationManager.AppSettings);
            }
            catch (Exception e)
            {
                throw new Exception("manager_configuration_file", e);
            }

            if (!dir.EndsWith(Path.DirectorySeparatorChar.ToString()))
            {
                dir = dir + Path.DirectorySeparatorChar;
            }

            logger = new Logger(GetDirPath("logs"), GetConfig("LogLevels"));

            try
            {
                DBSettings = new Configuration.ConfigDB(this);
            }
            catch (Exception e)
            {
                LogError("manager_configuration_database");
            }

            logger.Levels = GetConfig("LogLevels");
        }

        public string GetConfig(string key)
        {
            string value = CurrentSettings.Get(key);

            if (value == null)
            {
                value = DefaultSettings.Get(key);
            }
            return value;
        }

        public void SetConfig(string key, string value)
        {
            if (CurrentSettings[key] != null)
            {
                CurrentSettings[key] = value;
            }
            else
            {
                CurrentSettings.Add(key, value);
            }
        }

        public string GetDirPath(string type)
        {
            type = type.ToLower();
            switch(type)
            {
                default:
                    return dir + type + Path.DirectorySeparatorChar;
            }
        }

        public Data.ORM ORM(string Table, bool Hydrate = true)
        {
            return new Data.ORM(this, Table, Hydrate);
        }

        public Data.InternalORM InternalORM(string Table, bool Hydrate = true)
        {
            return new Data.InternalORM(this, Table, Hydrate);
        }

        public void LogActivity(string msg)
        {
            logger.Log("A", msg);
        }

        public void LogDebug(string msg)
        {
            logger.Log("D", msg);
        }

        public void LogError(string msg)
        {
            logger.Log("E", msg);
        }

        public void LogDB(string msg)
        {
            logger.Log("S", msg);
        }

        public void LogTiming(string msg)
        {
            logger.Log("T", msg);
        }
        
        public string GetGuid()
        {
            return logger.GetGuid();
        }

        public object Dispatch(string target)
        {
            return Dispatch(target, new object[] { this });
        }

        public object Dispatch(string target, object[] args)
        {
            string[] seps = { "::" };
            string[] parts = target.Split(seps, StringSplitOptions.None);

            if (parts.Length > 1)
            {
                string controller_name = parts[0];
                string action_name = parts[1];

                object controller = null;
                Type controller_type = null;
                MethodInfo action = null;

                controller = SearchController(controller_name);
                if (controller == null)
                {
                    return null;
                }

                controller_type = controller.GetType();
                action = controller_type.GetMethod(action_name);

                if (action == null)
                {
                    return null;
                }

                return action.Invoke(controller, args);
            }
            return null;
        }

        private object SearchController(string name)
        {
            Assembly[] assems = AppDomain.CurrentDomain.GetAssemblies();
            Type type = null;
            foreach (Assembly assem in assems)
            {
                type = assem.GetType(name, false, false);
                if (type != null)
                {
                    return assem.CreateInstance(name, true);
                }
            }
            return null;
        }
    }
}
