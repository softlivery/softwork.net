﻿namespace VGFramework
{
    public class Validator : Validation.Validator
    {
        public enum Type
        {
            Integer4,
            Integer9,
            Integer11,
            Integer20,
            Email,
            PostalCode,
            Text,
            Text25,
            Text30,
            Text40,
            Text50,
            Text150,
            URL,
            IPAddress,
            MACAddress,
            Decimal4,
            Decimal2Positive,
            Decimal4Positive,
            Decimal2Negative,
            Decimal4Negative,
            IntegerList,
            IntegerQuotedList,
            Gender,
            TypeCostList,
            IsDIDRegistered,
            Billing,
            LevelCostList,
            UserStatus,
            VSCRoute,
            Prefix,
            Destination,
            /*
Amount,
Description, //25,30,150,
DescWithoutSpecialChars,
Destination,
Filter,
FontColor,
NamesWithoutSpecialChars,
NegativeAmount,
NegativeCreditTwoDecimals,
Password,
PasswordWithoutSpecialChars,
PhoneCodeNumber,
PositiveAmount,
Prefix,
Restriction0To9AndT,
SQLInjection,
TextValue, //40,50,
TwoDecimalAmount,
UAlias,
WebSite,
ListNumbers,
TransactionTypes,
QuoteNumberList,
*/
        }

        public static bool Check(Type type, string value)
        {
            switch (type)
            {
                case Type.Integer4:
                    //4 - Result
                    return Check(MasterType.Integer, value, 1, 4);
                case Type.Integer9:
                    //9 - countryCode; areaCode; iddPrefix; nddPrefix; defaultPhone
                    return Check(MasterType.Integer, value, 1, 9);
                case Type.Integer11:
                    //11 - Result
                    return Check(MasterType.Integer, value, 1, 11);
                case Type.Integer20:
                    return Check(MasterType.Integer, value, 1, 20);
                case Type.IntegerList:
                    return Check(MasterType.List, value, @"\d+", ",");
                case Type.IntegerQuotedList:
                    return Check(MasterType.List, value, @"'\d+'", ",");
                case Type.Email:
                    //Mail; UserMail; DistriMailDir
                    return Check(MasterType.Email, value);
                case Type.Text:
                    //destinationValue; PhoneNumber; phoneNumberId; pnRef; DID; sourceValue, Pager, credCardNumber
                    return Check(MasterType.Text, value);
                case Type.Text40:
                    //address; DistriName; authcode; DNIS; DistriMailServer
                    return Check(MasterType.Text, value, 1, 40);
                case Type.Text150:
                    //SendMail_Link; SendMail_Pass; SendMail_Request; SendMail_User; User description; Comment; DistriMailName
                    return Check(MasterType.Text, value, 1, 150);
                case Type.Text50:
                    //City; State; CountryDesc; FirstName; LastName; WebPageAlias
                    return Check(MasterType.Text, value, 1, 50);
                case Type.Text25:
                    //CreditCard Description; language; respMsg; PhoneTypeDesc
                    return Check(MasterType.Text, value, 1, 25);
                case Type.PostalCode:
                    //Postal code
                    return Check(MasterType.Text, value, 3, 10);
                case Type.Text30:
                    //SendMail_Link; SendMail_Pass; SendMail_Request; SendMail_User; User description; Comment; DistriMailName
                    //falta escapara comilla simple
                    return Check(MasterType.Text, value, 1, 30);
                case Type.IPAddress:
                    return Check(MasterType.IPAddress, value);
                case Type.MACAddress:
                    return Check(MasterType.MACAddress, value);
                case Type.Gender:
                    string[] Genres = { "M", "F" };
                    return Check(MasterType.Option, value.ToUpper(), Genres);
                case Type.TypeCostList:
                    string[] CostTypes = { "T", "O", "P" };
                    return Check(MasterType.Option, value.ToUpper(), CostTypes);
                case Type.LevelCostList:
                    string[] CostLevels = { "D", "U", "T" };
                    return Check(MasterType.Option, value.ToUpper(), CostLevels);
                case Type.Billing:
                    string[] BillingTypes = { "C", "L", "D" };
                    return Check(MasterType.Option, value.ToUpper(), BillingTypes);
                case Type.UserStatus:
                    //Status 'enabled','enabled in','enabled out','disabled'
                    string[] UserStatuses = { "enabled", "enabled in", "enabled out", "disabled" };
                    return Check(MasterType.Option, value.ToLower(), UserStatuses);
                case Type.IsDIDRegistered:
                    string[] DIDStatuses = { "X", "Y", "N" };
                    return Check(MasterType.Option, value.ToUpper(), DIDStatuses);
                case Type.URL:
                    return Check(MasterType.URL, value);
                case Type.Decimal4:
                    //Amount
                    return Check(MasterType.Float, value, 1, 4);
                case Type.Decimal2Positive:
                    //Amount; Tax
                    return Check(MasterType.FloatPositive, value, 1, 2);
                case Type.Decimal4Positive:
                    //Amount; Tax
                    return Check(MasterType.FloatPositive, value, 1, 4);
                case Type.Decimal2Negative:
                    //Amount
                    return Check(MasterType.FloatNegative, value, 1, 2);
                case Type.Decimal4Negative:
                    //Amount
                    return Check(MasterType.FloatNegative, value, 1, 4);
                case Type.VSCRoute:
                    //permite 0 al 9 T y . (punto)
                    return Check(MasterType.Regex, value, @"^[T.0-9]+$");
                case Type.Prefix:
                    return Check(MasterType.Regex, value, @"^[0-9#\*]+$");
                case Type.Destination:
                    return Check(MasterType.Regex, value, @"^[0-9\.]*T$");

                    /*
                        case Type.FontColor:
                            //HeaderBgCol1; FontColor
                            validator = new Regex(("^[a-zA-Z0-9'#'|a-zA-z]{2,10}$"));
                            return validator.IsMatch(value);

                        case Type.Password:
                            //Distributor Description; password
                            validator = new Regex(("^[^%\\" + Chr(34) + "\\'\\+&\\-\\s]{1,30}$"));
                            return validator.IsMatch(value);

                        case Type.NamesWithoutSpecialChars:
                            //User Names & Passwords
                            validator = new Regex(("^[^-&+%'\\" + Chr(34) + "\\s]*$"));
                            return validator.IsMatch(value);

                        case Type.PasswordWithoutSpecialChars:
                            //Passwords
                            validator = new Regex(("^[^-&+%'\\" + Chr(34) + "]*$"));
                            return validator.IsMatch(value);
                            
                        case Type.SQLInjection:
                            //Result
                            validator = new Regex(("^[^'-,;]*$"));
                            return validator.IsMatch(value);

                        case Type.Filter:
                            //Result
                            validator = new Regex((@"^[^\'\-\;]*$"));
                            return validator.IsMatch(value);

                        case Type.TransactionTypes:
                            //Transaction Types 1,2,3,4,5,7,8,9,10,12,17
                            validator = new Regex(("^(1|2|3|4|5|7|8|9|10|12|17)((,){1}(1|2|3|4|5|7|8|9|10|12|17))*$"));
                            return validator.IsMatch(value);


                    */
            }
            return false;
        }
    }
}
