﻿namespace VGFramework.Routing
{
    public static partial class Extensions
    {
        public static string ToString(this RouteAuth method)
        {
            switch (method)
            {
                case RouteAuth.NONE:
                    return "none";
                case RouteAuth.CLIENT:
                    return "client";
                case RouteAuth.USER:
                    return "user";
                case RouteAuth.BOTH:
                    return "both";
            }

            throw new System.Exception("Unkown auth");
        }
    }

    public enum RouteAuth
    {
        NONE,
        USER,
        CLIENT,
        BOTH
    }
}
