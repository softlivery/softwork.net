﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace VGFramework.Routing
{
    class RouterDB : Router
    {
        public RouterDB() : base()
        {
            DataSet routesTMP = GetRoutes();
            if (routesTMP != null)
            {
                foreach (dynamic route in routesTMP.Tables[0].Rows)
                {
                    Route objRoute = new Route();
                    string[] atribs = { "Name", "Method", "Pattern", "Target", "Auth", "Template" };

                    foreach (string attr in atribs)
                    {
                        if (route[attr].GetType() != DBNull.Value.GetType())
                        {
                            objRoute.Add(attr.ToLower(), route[attr]);
                            continue;
                        }
                        
                        if (attr.Equals("Method"))
                            objRoute.Add(attr.ToLower(), string.Empty);
                        else
                            objRoute.Add(attr.ToLower(), null);
                    }
                    Routes.Add(objRoute);
                }
            }
        }

        private static DataSet GetRoutes()
        {
            Data.Report report = new Data.Report(new Manager(), "VGF_Permissions");
            return report.Execute();
        }
    }
}
