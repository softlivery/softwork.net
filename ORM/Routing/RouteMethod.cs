﻿namespace VGFramework.Routing
{
    public static partial class Extensions
    {
        public static string ToString(this RouteMethod method)
        {
            switch(method)
            {
                case RouteMethod.ALL:
                    return "GET|POST";
                case RouteMethod.GET:
                    return "GET";
                case RouteMethod.POST:
                    return "POST";
            }

            throw new System.Exception("Unkown method");
        }
    }

    public enum RouteMethod
    {
        GET,
        POST,
        ALL
    }
}
