﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web;

namespace VGFramework.Routing
{
    class RouterXML : Router
    {
        public RouterXML() : base()
        {
            dynamic routesTMP = GetRoutes();
            if (routesTMP != null)
            {
                foreach (dynamic route in routesTMP.route)
                {
                    Route objRoute = new Route();
                    string[] atribs = { "name", "method", "pattern", "target", "auth", "template" };

                    foreach (string attr in atribs)
                    {
                        if (route.get(attr) != null)
                        {
                            objRoute.Add(attr, route.get(attr));
                        }
                    }
                    Routes.Add(objRoute);
                }
            }
        }

        private static DynamicXml GetRoutes()
        {
            string filename = HttpContext.Current.Request.PhysicalApplicationPath + "routing.config";

            try
            {
                return Configuration.ConfigXML.GetXml("routes", filename);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
