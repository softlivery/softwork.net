﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Text.RegularExpressions;
using System.Web;

namespace VGFramework.Routing
{

    public class Router
    {
        protected const string KEY = "Router";
        protected Dictionary<string, string> types;
        protected ArrayList Routes;
        public static Router Instance
        {
            get
            {
                if (HttpContext.Current.Items[KEY] == null)
                    HttpContext.Current.Items[KEY] = new Router();

                return HttpContext.Current.Items[KEY] as Router;
            }
        }

        public Router()
        {
            Routes = new ArrayList();
            LoadDefaultSettings();
        }

        private void LoadDefaultSettings()
        {
            types = new Dictionary<string, string>
            {
                { "i", "[0-9]+" },
                { "a", "[0-9A-Za-z-_]+" },
                { "h", "[0-9A-Fa-f]+" },
                { "*", ".?" },
                { "**", ".+" },
                { "", @"[^/\.]+" }
            };
        }

        private string Compile(dynamic route)
        {
            string pattern = route.pattern + "$";
            Regex r = new Regex(@"(?<pre>/|\.|)\[(?<type>[^:\]]*)(:(?<name>[^:\]]*))?\](?<opt>\?|)");
            MatchCollection ms = r.Matches(pattern);
            ArrayList names = new ArrayList();

            foreach (Match m in ms)
            {
                string pre = m.Groups["pre"].Value;
                pre = pre.Equals(".") ? @"\." : pre;
                pre = pre.Equals("") ? null : pre;
                string name = m.Groups["name"].Value;
                if (!name.Equals(""))
                {
                    name = "?<" + name + ">";
                    names.Add(name);
                }
                string opt = m.Groups["opt"].Value.Equals("") ? null : "?";
                string type = this.types[m.Groups["type"].Value];
                string replace = "(" + pre + "(" + name + type + "))" + opt;
                pattern = pattern.Replace(m.Value, replace);
            }

            route.names = names;
            return pattern;
        }

        internal int Add(Route route)
        {
            return Routes.Add(route);
        }

        internal Route Match(string url, string method)
        {
            foreach (dynamic route in Routes)
            {
                string[] methods = route.method.Split('|');
                bool method_match = false;

                for (int j = 0; j < methods.Length; j++)
                {
                    if (method.Equals(methods[j], StringComparison.OrdinalIgnoreCase))
                    {
                        method_match = true;
                        break;
                    }
                }

                if (!method_match)
                    continue;

                if (route.pattern.Equals("*"))
                {
                    return route;
                }
                else if (route.pattern.Length > 0 && route.pattern.Substring(0, 1).Equals("@"))
                {
                    Regex r = new Regex(route.pattern.Substring(1), RegexOptions.IgnoreCase);
                    Match m = r.Match(url);

                    if (m.Success)
                    {
                        //https://msdn.microsoft.com/es-es/library/system.text.regularexpressions.match.groups(v=vs.110).aspx
                        return route;
                    }
                }
                else
                {
                    Regex r = new Regex(route.Compile(types), RegexOptions.IgnoreCase & RegexOptions.ExplicitCapture);
                    Match match = r.Match(url);

                    if (match.Success)
                    {
                        GroupCollection groups = match.Groups;
                        string[] groupNames = r.GetGroupNames();

                        if (route.names.Count > 0)
                        {
                            Dictionary<string, string> parameters = new Dictionary<string, string>();
                            foreach (string groupName in groupNames)
                            {
                                if (route.names.Contains(groupName))
                                {
                                    parameters.Add(groupName, groups[groupName].Value);
                                }
                            }
                            route.parameters = parameters;
                        }
                        return route;
                    }
                }
            }
            return null;
        }

        public int AddRoute(RouteMethod method, string pattern, string name, string target, RouteAuth auth, string template)
        {
            Route route = new Route();
            route.Add("method", method.ToString());
            route.Add("pattern", pattern);
            route.Add("name", name);
            route.Add("target", target);
            route.Add("auth", auth.ToString());
            route.Add("template", template);
            return Add(route);
        }
    }

    internal class Route : DynamicObject
    {
        readonly Dictionary<string, object> props;

        public Route()
        {
            this.props = new Dictionary<string, object>();
        }

        public void Add(string key, object value)
        {
            this.props.Add(key, value);
        }

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = null;
            if (this.props.ContainsKey(binder.Name))
            {
                result = this.props[binder.Name];
            }
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            this.props[binder.Name] = value;
            return true;
        }

        public string Compile(Dictionary<string, string> types)
        {
            dynamic route = this;
            //string pattern = route.pattern + "$";
            string pattern = "^" + route.pattern + "$";
            Regex r = new Regex(@"(?<pre>/|\.|)\[(?<type>[^:\]]*)(:(?<name>[^:\]]*))?\](?<opt>\?|)");
            MatchCollection ms = r.Matches(pattern);
            ArrayList names = new ArrayList();

            foreach (Match m in ms)
            {
                string pre = m.Groups["pre"].Value;
                pre = pre.Equals(".") ? @"\." : pre;
                pre = pre.Equals("") ? null : pre;
                string name = m.Groups["name"].Value;
                if (!name.Equals(""))
                {
                    names.Add(name);
                    name = "?<" + name + ">";
                }
                string opt = m.Groups["opt"].Value.Equals("") ? null : "?";
                string type = types[m.Groups["type"].Value];
                string replace = "(" + pre + "(" + name + type + "))" + opt;
                pattern = pattern.Replace(m.Value, replace);
            }

            route.names = names;
            return pattern;
        }
    }
}
