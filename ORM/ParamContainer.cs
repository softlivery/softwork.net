﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;

namespace VGFramework
{
    public class ParamContainer
    {
        private dynamic objValue;
        //public dynamic Resource
        //{
        //    get { return objValue; }
        //}


        /// <summary>
        /// Set Dynamic object resource
        /// </summary>
        /// <param name="res"></param>
        public void SetResource(dynamic res)
        {
            objValue = res;
        }

        /// <summary>
        /// Get string representation of Json parameter
        /// </summary>
        /// <returns></returns>
        public string GetJsonParameter()
        {
            return objValue.ToString();
        }


        public Dictionary<string, string> All()
        {
            Dictionary<string, string> res = new Dictionary<string, string>();

            //define temporal variables
            JToken tmpProperty;
            tmpProperty = objValue;

            //search every property level in the hierachy, in case of any invalid property set null to temporal 
            if (tmpProperty != null)
            {
                foreach (JProperty entity in tmpProperty)
                {
                    res.Add(entity.Name, entity.Value.ToString());
                }
            }
            return res;
        }

        /// <summary>
        /// Remove a property of the current parameter object
        /// </summary>
        /// <param name="property"></param>
        public void Remove(string property)
        {
            JToken member;
            int i = 0;
            //get properties hierarchy
            var entities = property.Split('.');

            //define temporal variables
            JToken tmpProperty;
            tmpProperty = objValue;

            //search every property level in the hierachy, in case of any invalid property set null to temporal 
            foreach (var entity in entities)
            {
                member = tmpProperty[entities[i]];
                if (member != null)
                {
                    tmpProperty = member;
                }
                else
                {
                    tmpProperty = null;
                    break;
                }
                i++;
            }


            //if property was found retrieved corresponding value to the caller (true/false)
            if (tmpProperty != null)
            {
                tmpProperty.Parent.Remove();
            }


        }


        /// <summary>
        /// Retrieve data value from property hierarchy pass through parameters, on success returns property value otherwise return selected default value 
        /// </summary>
        /// <param name="property">Property name or properties hierarchy</param>
        /// <param name="defaultvalue">Default value returned on reading failure</param>
        /// <returns></returns>
        public object Get(string property, object defaultvalue = null)
        {
            if (objValue == null)
            {
                return defaultvalue;
            }

            JToken member;
            int i = 0;
            //get properties hierarchy
            var entities = property.Split('.');

            //define temporal variables
            JToken tmpProperty;
            tmpProperty = objValue;

            //search every property level in the hierachy, in case of any invalid property set null to temporal 
            foreach (var entity in entities)
            {
                member = tmpProperty[entities[i]];
                if (member != null)
                {
                    tmpProperty = member;
                }
                else
                {
                    tmpProperty = null;
                    break;
                }
                i++;
            }

            if (tmpProperty == null)
            {
                return defaultvalue;
            }

            if(defaultvalue == null)
            {
                return tmpProperty;
            }

            return ConvertType(defaultvalue.GetType().Name, tmpProperty.ToString());
        }

        public object GetConverted(string property, string type)
        {
            object tmpProperty = Get(property);

            if (tmpProperty == null)
            {
                return null;
            }

            return ConvertType(type, tmpProperty.ToString());
        }

        public static object ConvertType(string type, string value)
        {
            try
            {
                if (value == null)
                {
                    return null;
                }
                else
                {
                    switch (type.ToLower())
                    {
                        case "datetime":
                            if (value.Equals(""))
                                return null;
                            return DateTime.ParseExact(value, WebManager.Instance.GetConfig("DateTimeFormat"), null);
                        case "date":
                            if (value.Equals(""))
                                return null;
                            return DateTime.ParseExact(value, WebManager.Instance.GetConfig("DateFormat"), null);
                        case "bigint":
                            if (value.Equals(""))
                                return null;
                            return long.Parse(value);
                        case "int":
                        case "int64":
                            if (value.Equals(""))
                                return null;
                            return int.Parse(value);
                        case "money":
                        case "decimal":
                        case "double":
                            if (value.Equals(""))
                                return null;
                            return double.Parse(value.Replace(".", "").Replace(',', '.'));
                        case "char":
                        case "nchar":
                        case "varchar":
                        case "nvarchar":
                        case "strin":
                        default:
                            return value;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("ConvertType", e);
            }
        }

        /// <summary>
        /// Set a property inside the parameters container, if the parameter exist the value will be updated otherwise a new element
        /// with the parameter value will be added to the variable
        /// </summary>
        /// <param name="property">Property or properties hierarchy to be set</param>
        /// <param name="value">Property value</param>
        public void Set(string property, object value)
        {

            JToken member = null;
            int i = 0;
            //get properties hierarchy
            var entities = property.Split('.');

            //define temporal variables
            JToken tmpProperty;
            tmpProperty = objValue;

            //search every property level in the hierachy, in case of any invalid property set null to temporal 
            foreach (var entity in entities)
            {
                if (tmpProperty != null)
                {
                    member = tmpProperty[entities[i]];
                    if (member != null)
                    {
                        tmpProperty = member;
                    }
                    else
                    {
                        //tmpProperty = null;
                        break;
                    }
                    i++;
                }
            }

            //if property was found retrieved corresponding value to the caller, else return default value
            if (tmpProperty != null && member != null)
            {
                tmpProperty = (value != null) ? value.ToString() : null;
            }
            else
            {
                if(tmpProperty==null)
                {
                    tmpProperty = new JObject();
                    ((JObject)tmpProperty).Add(entities[i], value.ToString());
                    objValue = tmpProperty;
                    return;
                }
                ((JObject)tmpProperty).Add(entities[i], value.ToString());
            }
        }

        /// <summary>
        /// Retrieve if the property exist in the hierarchy inside the parameters container, on success returns true otherwise false
        /// </summary>
        /// <param name="property">Property or properties hierarchy to search</param>
        /// <returns>true/false</returns>
        public bool Has(string property)
        {

            JToken member;
            bool res = false;
            int i = 0;
            //get properties hierarchy
            var entities = property.Split('.');

            //define temporal variables
            JToken tmpProperty;
            tmpProperty = objValue;

            if (tmpProperty == null)
            {
                return false;
            }

            //search every property level in the hierachy, in case of any invalid property set null to temporal 
            foreach (var entity in entities)
            {
                member = tmpProperty[entities[i]];
                if (member != null)
                {
                    tmpProperty = member;
                }
                else
                {
                    tmpProperty = null;
                    break;
                }
                i++;
            }

            //if property was found retrieved corresponding value to the caller (true/false)
            if (tmpProperty != null)
            {
                res = true;
            }

            return res;
        }
    }
}
