﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Helpers;
using System.Reflection;
using VGFramework.Renders;
using System.Linq;
using System.Xml.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VGFramework
{
    public class WebManager : Manager
    {
        protected const string KEY = "Manager";

        //protected bool Run;
        protected Render render;

        public string RedirectURL;
        public HttpResponse ResponseHTTP;
        public HttpRequest RequestHTTP;
        public bool Mobile;
        private bool embedded;
        public string Method;
        public ParametersBag<dynamic> Response;
        public ParametersBag<string> Headers;
        public ParamContainer Parameters = new ParamContainer();
        public ParametersBag<string> QueryParameters = new ParametersBag<string>();
        public ParametersBag<string> Session;
        public string Url;
        public string RequestType;
        public string ResponseType;
        public string Body;
        public dynamic Route;
        public string Template;

        public Render Render { get => render; set => render = value; }
        public int StatusCode { get => ResponseHTTP.StatusCode; set => ResponseHTTP.StatusCode = value; }

        public static new WebManager Instance
        {
            get
            {
                if (HttpContext.Current.Items[KEY] == null)
                    HttpContext.Current.Items[KEY] = new WebManager();

                return HttpContext.Current.Items[KEY] as WebManager;
            }
        }

        public bool Embedded { get => embedded; }

        protected WebManager() : base()
        {
        }

        public void Run(HttpRequest pRequest, HttpResponse pResponse)
        {
            try
            {
                bool run = true;
                Initialize(pRequest, pResponse);
                ParseRequest();
                ParseParameters();
                run = SearchRoute();

                if (run && Route.target != null && Route.target != "")
                {
                    try
                    {
                        LogActivity("Execute " + Route.target);
                        LogActivity("Request: " + Body);
                        Dispatch((string)Route.target);
                    }
                    catch (Exception e)
                    {

                        if (e.InnerException != null)
                        {
                            Response.Set("error", e.InnerException.Message);
                        }
                        else
                        {
                            Response.Set("error", e.Message);
                        }
                    }
                }

                RenderResponse();
            }
            catch(Exception e)
            {
                LogError(e.Message);
                StatusCode = 500;
                ResponseHTTP.StatusCode = StatusCode;
                ResponseHTTP.StatusDescription = StatusMsg();
            }
            //LogAccess(Url, RequestHTTP.QueryString.ToString(), this.StatusCode);
        }

        protected void Redirect(string redirect)
        {
            if (StatusCode == 200)
            {
                StatusCode = 303;
            }

            redirect += "?" + string.Join("&", Response.All().Select(kvp => string.Format("{0}={1}", kvp.Key, kvp.Value)));
            if (Embedded)
                redirect += "&embedded=1";

            ResponseHTTP.Redirect(redirect, false);
            return;
        }

        protected void RenderResponse()
        {
            string redirect = (string)Response.Get("redirect", "");
            if (!redirect.Equals(""))
            {
                string[] param = { redirect };
                Response.Remove("redirect");
                Redirect(redirect);
                return;
            }

            DoRender();
        }

        protected void DoRender()
        {
            ResponseHTTP.StatusCode = StatusCode;
            ResponseHTTP.StatusDescription = StatusMsg();

            switch (ResponseType)
            {
                case "":
                    Render = null;
                    break;
                case "text/xml":
                case "application/xml":
                    Render = new XML();
                    break;
                case "text/html":
                    Render = new HTML();
                    break;
                case "application/pdf":
                    Render = new PDF();
                    break;
                case "application/json":
                default:
                    Render = new Renders.Json();
                    break;
            }

            if (Render == null)
            {
                return;
            }

            if (Response.Has("error"))
            {
                Render.Error(Response.Get("error", "").ToString());
            }
            else
            {
                Render.Render(this);
            }
        }

        protected bool SearchRoute()
        {
            //if (Headers.Get("SOAPAction", "") == "")
            //{
            Routing.RouterDB router = new Routing.RouterDB();
            dynamic route = router.Match(Url.ToLower(), Method);

                if (route == null)
                {
                    LogError("No route found:" + Url.ToLower());
                    StatusCode = 404;
                    //Run = false;
                    return false;
                }

                LogDebug(string.Format("Route founded: {0} {1}", route.name, route.pattern));

                Route = route;
                if (Route.parameters != null)
                {
                    foreach (KeyValuePair<string, string> param in Route.parameters)
                    {
                        Parameters.Set(param.Key, param.Value);
                    }
                }
                Template = Route.template;
            //}
            /*else
            {
                LogDebug(string.Format("Route founded: {0}", Url.ToLower()));
                Routing.Route soapRoute = new Routing.Route();
                soapRoute.add("name", "index");
                soapRoute.add("method", "POST");
                soapRoute.add("pattern", Url.ToLower());
                soapRoute.add("target", "SOAPCalls::CallMethod");
                soapRoute.add("auth", "none");
                Route = soapRoute;
                Template = Route.template;

            }*/
            return true;
        }

        protected void ParseParameters()
        {
            //get request body content
            try
            {
                StreamReader sr = new StreamReader(RequestHTTP.InputStream);
                Body = sr.ReadToEnd();
            }
            catch (Exception e)
            {
                Body = "";
            }

            //PARSE PARAMETERS
            //query string parameters
            if(RequestHTTP.Headers["SOAPAction"]!=null)
            {
                Headers.Set("SOAPAction", RequestHTTP.Headers["SOAPAction"].ToString());
                //BodyParameters.Set("SOAPAction", RequestHTTP.Headers["SOAPAction"].ToString());
            }
            if (RequestHTTP.Headers["Url"] != null)
            {
                Headers.Set("Url", RequestHTTP.Headers["Url"].ToString());
                //BodyParameters.Set("SOAPAction", RequestHTTP.Headers["SOAPAction"].ToString());
            }

            foreach (string param in RequestHTTP.QueryString)
            {
                QueryParameters.Set(param, RequestHTTP.QueryString[param]);
                //BodyParameters.Set(param, RequestHTTP.QueryString[param]);
            }

            if (QueryParameters.Has("embedded"))
            {
                embedded = true;
                QueryParameters.Remove("embedded");
            }
            //BodyParameters.Remove("nocache");

            //body parameters
            if (Body.Length > 0)
            {
                switch (RequestType)
                {
                    case "application/json":
                        object parameters_json = Newtonsoft.Json.JsonConvert.DeserializeObject(Body);
                        Render parser = new Renders.Json();
                        dynamic tmp2 = parser.Decode(Body);
                        Parameters.SetResource(tmp2);

                        /*DynamicJsonObject tmp2 = (DynamicJsonObject)parser.Decode(Body);
                        IEnumerator<String> i = tmp2.GetDynamicMemberNames().GetEnumerator();
                        while (i.MoveNext())
                        {
                            object value;
                            Renders.JsonMemberBinder binder = new Renders.JsonMemberBinder(i.Current, false);
                            tmp2.TryGetMember(binder, out value);
                            BodyParameters.Set(i.Current, value.ToString());
                        }*/
                        break;
                    case "text/html":
                    case "application/x-www-form-urlencoded":
                        parser = new HTML();
                        NameValueCollection tmp = (NameValueCollection)parser.Decode(Body);
                        //Parameters.SetResource(tmp);
                        foreach (string param in tmp)
                        {
                            Parameters.Set(param, tmp[param]);
                        }
                        break;
                    case "text/xml":
                    case "application/xml":
                        parser = new XML();   
                        //if (Headers.Get("SOAPAction", "")!= null)
                        //{
                            dynamic xmlresult = parser.Decode(Body);
                            Parameters.SetResource(xmlresult);
                        //}
                        break;
                    case "multipart/form-data":
                        foreach (string param in RequestHTTP.Params)
                        {
                            Parameters.Set(param, RequestHTTP.Params[param]);
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        protected void ParseRequest()
        {
            string BasePath = RequestHTTP.Url.AbsolutePath;
            BasePath = BasePath.ToLower().Replace("/default.aspx", "");
            Method = RequestHTTP.HttpMethod;
            Url = RequestHTTP.RawUrl.ToLower().Replace("default.aspx", "");
            //Url = Url.ToLower().StartsWith(GetConfig("SiteBasePath").ToLower()) ? Url.Substring(GetConfig("SiteBasePath").Length) : Url;
            Url = Url.ToLower().StartsWith(BasePath) ? Url.Substring(BasePath.Length) : Url;
            Url = Url.IndexOf('?') > -1 ? Url.Remove(Url.IndexOf('?')) : Url;
            if (RequestHTTP.UserAgent != null && (RequestHTTP.UserAgent.Contains("Android") || RequestHTTP.UserAgent.Contains("iPhone")))
            {
                Mobile = true;
            }

            RequestType = RequestHTTP.ContentType.ToLower();
            RequestType = RequestType.IndexOf(';') > -1 ? RequestType.Remove(RequestType.IndexOf(';')) : RequestType;

            ResponseType = "application/json";
            //get response type
            if (RequestHTTP.AcceptTypes != null && RequestHTTP.AcceptTypes.Length > 0)
            {
                ResponseType = string.Join(";", RequestHTTP.AcceptTypes).ToLower();
                ResponseType = ResponseType.IndexOf(';') > -1 ? ResponseType.Remove(ResponseType.IndexOf(';')) : ResponseType;
            }
        }

        public string GetUrl(string type)
        {
            type = type.ToLower();
            return RequestHTTP.Url.Scheme + "://" + RequestHTTP.Url.Host + RequestHTTP.Url.AbsolutePath.ToLower().Replace("default.aspx", "") + type;
        }

        protected void Initialize(HttpRequest pRequest, HttpResponse pResponse)
        {
            dir = AppDomain.CurrentDomain.BaseDirectory;
            //Run = true;
            ResponseHTTP = pResponse;
            RequestHTTP = pRequest;
            RedirectURL = "";
            StatusCode = 200;
            Mobile = false;
            CultureInfo Culture = Thread.CurrentThread.CurrentCulture;
            Response = new ParametersBag<dynamic>();
            Headers = new ParametersBag<string>();
            //BodyParameters = new ParametersBag<string>();
            Session = new ParametersBag<string>();

            SetConfig("SiteBase", GetUrl(""));
        }
        
        protected void AccessLog(dynamic ClientUser = null)
        {
            dynamic Log = InternalORM("AccessLog");
            Log.RequestId = GetGuid();

            Renders.Json json = new Renders.Json();
            Dictionary<string, string> parameters = QueryParameters.All().ToDictionary(entry => entry.Key, entry => entry.Value);
            foreach (KeyValuePair<string, string> entry in Parameters.All())
            {
                if (parameters.ContainsKey(entry.Key))
                {
                    parameters[entry.Key] = entry.Value;
                }
                else
                {
                    parameters.Add(entry.Key, entry.Value);
                }
            }

            if (!Log.Load())
            {
                Log.Date = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                Log.Path = Url;
                Log.Params = json.Encode(parameters).Replace("\'", "\'\'");
                Log.IP = RequestHTTP.UserHostAddress;
            }
            int? userId = null;
            int? client = null;

            if (ClientUser != null)
            {
               userId = int.Parse(ClientUser.User.ToString());
               client = int.Parse(ClientUser.Client.ToString());
            }

            Log.User = userId;
            Log.Client = client;
            Log.ResultCode = StatusCode;
            Log.Save();
        }

        protected string StatusMsg()
        {
            switch(StatusCode)
            {
                case 100:
                    return "Continue";
                case 101:
                    return "Switching Protocol";
                case 102:
                    return "Processing";
                case 103:
                    return "Checkpoint";
                case 200:
                    return "OK";
                case 201:
                    return "Created";
                case 202:
                    return "Accepted";
                case 203:
                    return "Non-Authoritative Information";
                case 204:
                    return "No Content";
                case 205:
                    return "Reset Content";
                case 206:
                    return "Partial Content";
                case 207:
                    return "Multi-Status";
                case 208:
                    return "Already Reported";
                case 300:
                    return "Multiple Choices";
                case 301:
                    return "Moved Permanently";
                case 302:
                    return "Found";
                case 303:
                    return "See Other";
                case 304:
                    return "Not Modified";
                case 305:
                    return "Use Proxy";
                case 306:
                    return "Switch Proxy";
                case 307:
                    return "Temporary Redirect";
                case 308:
                    return "Permanent Redirect";
                case 400:
                    return "Bad Request";
                case 401:
                    return "Unauthorized";
                case 402:
                    return "Payment Required";
                case 403:
                    return "Forbidden";
                case 404:
                    return "Not Found";
                case 405:
                    return "Method Not Allowed";
                case 406:
                    return "Not Acceptable";
                case 407:
                    return "Proxy Authentication Required";
                case 408:
                    return "Request Timeout";
                case 409:
                    return "Conficlt";
                case 410:
                    return "Gone";
                case 411:
                    return "Length Required";
                case 412:
                    return "Precondition Failed";
                case 413:
                    return "Request Entity Too Large";
                case 414:
                    return "Request-URI Too Long";
                case 415:
                    return "Unsupported Media Type";
                case 416:
                    return "Request Range Not Satisfiable";
                case 417:
                    return "Expectation Failed";
                case 418:
                    return "I'm a teapot";
                case 422:
                    return "Unprocessable Entity";
                case 423:
                    return "Locked";
                case 424:
                    return "Failed Dependency";
                case 425:
                    return "Unassigned";
                case 426:
                    return "Upgrade Required";
                case 428:
                    return "Precondition Required";
                case 429:
                    return "Too Many Request";
                case 431:
                    return "Request Header Fields Too Large";
                case 449:
                    return "";
                case 451:
                    return "Unavailable for Legal Reasons";
                case 500:
                    return "Internal Server Error";
                case 501:
                    return "Not Implemented";
                case 502:
                    return "Bag Gateway";
                case 503:
                    return "Service Unavailable";
                case 504:
                    return "Gateway Timeout";
                case 505:
                    return "HTTP Version Not supported";
                case 506:
                    return "Variant Also Negotiates";
                case 507:
                    return "Insufficient Storage";
                case 508:
                    return "Loop Detected";
                case 509:
                    return "Bandwidth Limit Exceeded";
                case 510:
                    return "Not Extended";
                case 511:
                    return "Network Authentication Required";
                case 512:
                    return "Not updated";
                case 521:
                    return "Version Mismatch";
            }
            throw new Exception("Unrecognizable Status Code");
        }

        public bool isMethod(string method)
        {
            return RequestHTTP.HttpMethod.Equals(method.ToUpper());
        }

        public bool isHTML()
        {
            return ResponseType.Equals("text/html");
        }
    }
}
