﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace VGFramework
{
    public class OAuthManager : WebManager
    {
        public dynamic Client;
        public dynamic ClientUser;
        public dynamic User;
        public new static OAuthManager Instance
        {
            get
            {
                if (HttpContext.Current.Items[KEY] == null)
                    HttpContext.Current.Items[KEY] = new OAuthManager();

                return HttpContext.Current.Items[KEY] as OAuthManager;
            }
        }

        public new void Run(HttpRequest pRequest, HttpResponse pResponse)
        {
            try
            {
                bool run = true;
                Initialize(pRequest, pResponse);
                ParseRequest();
                ParseParameters();
                run = SearchRoute();
                AccessLog();

                if (Route != null && Route.auth != null && Route.auth != "" && Route.auth != "none" && Route.auth != "NONE")
                {
                    try
                    {
                        LogActivity("Validating");
                        oAuth.Manager.Validate();
                    }
                    catch (Exception e)
                    {
                        run = false;
                        if (e.InnerException != null)
                        {
                            Response.Set("error", e.InnerException.Message);
                        }
                        else
                        {
                            Response.Set("error", e.Message);
                        }
                    }
                }

                if (run && Route.target != null && Route.target != "")
                {
                    try
                    {
                        LogActivity("Execute " + Route.target);
                        Dispatch((string)Route.target);
                    }
                    catch (Exception e)
                    {

                        if (e.InnerException != null)
                        {
                            Response.Set("error", e.InnerException.Message);
                        }
                        else
                        {
                            Response.Set("error", e.Message);
                        }
                    }
                }
                RenderResponse();
            }
            catch (Exception e)
            {
                LogError(e.Message);
                StatusCode = 500;
                ResponseHTTP.StatusCode = StatusCode;
                ResponseHTTP.StatusDescription = StatusMsg();
            }
            AccessLog(ClientUser);
        }

        protected new bool SearchRoute()
        {
            //load oAuth routes
            Routing.Route login = new Routing.Route();
            login.Add("method", "GET|POST");
            login.Add("pattern", "/oauth/authorize");
            login.Add("name", "authorize");
            login.Add("target", "VGFramework.oAuth.Manager::Authorize");
            login.Add("auth", "none");
            login.Add("template", "oauth/login");
            Routing.Router.Instance.Add(login);

            //Routing.Route consent = new Routing.Route();
            //consent.add("method", "GET|POST");
            //consent.add("pattern", "/oauth/consent");
            //consent.add("name", "consent");
            //consent.add("target", "VGFramework.oAuth.Manager::Consent");
            //consent.add("auth", "none");
            //router.Add(consent);

            Routing.Route token = new Routing.Route();
            token.Add("method", "GET|POST");
            token.Add("pattern", "/oauth/token");
            token.Add("name", "token");
            token.Add("target", "VGFramework.oAuth.Manager::Token");
            token.Add("auth", "none");
            Routing.Router.Instance.Add(token);

            dynamic route = Routing.Router.Instance.Match(Url.ToLower(), Method);

            if (route == null)
            {
                return base.SearchRoute();
            }

            LogDebug(string.Format("oAuth Route founded: {0} {1}", route.name, route.pattern));
            
            Route = route;
            if (Route.parameters != null)
            {
                foreach (KeyValuePair<string, string> param in Route.parameters)
                {
                    Parameters.Set(param.Key, param.Value);
                }
            }
            Template = Route.template;
            return true;
        }

        protected new void Redirect(string redirect)
        {
            if (StatusCode != 407 && Route != null && Route.auth != null && Route.auth.ToString().ToLower() == "user")
                Response.Set("access_token", ClientUser.Access_token);

            if (Client != null && Route != null && Route.auth != null && (Route.auth.ToString().ToLower() == "user" || Route.auth.ToString().ToLower() == "client"))
                Response.Set("client_id", Client.Client_id);

            redirect += "?" + string.Join("&", Response.All().Select(kvp => string.Format("{0}={1}", kvp.Key, kvp.Value)));
            if (Embedded)
                redirect += "&embedded=1";

            redirect = GetConfig("SiteBase") + (redirect.StartsWith("/") ? redirect.Substring(1) : redirect);
            ResponseHTTP.Redirect(redirect, false);
            return;
        }

        protected new void RenderResponse()
        {
            string redirect = (string)Response.Get("redirect", "");
            if (!redirect.Equals(""))
            {
                Response.Remove("redirect");
                Redirect(redirect);
                return;
            }

            DoRender();
        }
    }
}
