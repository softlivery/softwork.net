﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Helpers;
using System.Dynamic;
using System.Reflection;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Linq;
using System.Globalization;
using System.Threading;
using System.Diagnostics;

public partial class _Default : System.Web.UI.Page
{
    private readonly string VERSION = "0.0.1";

    protected void Page_Load(object sender, EventArgs e)
    {
        bool run = true;
        Stopwatch timer = Stopwatch.StartNew();
        Stopwatch timer2 = Stopwatch.StartNew();
        Logger.Debug(Thread.CurrentThread.CurrentCulture.Name);
        //Thread.CurrentThread.CurrentCulture = new CultureInfo("es-AR");
        ConfigManager.Initialize();
        RequestWrapper request = new RequestWrapper(Request, Response);
        request.lang = "ES";
        Router router = new Router(request);
        timer2.Stop();
        Logger.Timing("Tiempo inicializacion: " + timer2.ElapsedMilliseconds + "ms");

        try
        {
            if (request.parser != null)
            {
                Response.Clear();
                request.timer_start("route_match");
                dynamic route = router.match();
                request.timer_stop("route_match");

                if (route != null)
                {
                    Logger.Activity("Route Match " + route.name + " " + route.target);
                    request.timer_start("route_params");
                    if (route.parameters != null)
                    {
                        foreach (KeyValuePair<string, string> param in route.parameters)
                        {
                            if (request.parameters.ContainsKey(param.Key))
                            {
                                request.parameters[param.Key] = param.Value;
                            }
                            else
                            {
                                request.parameters.Add(param.Key, param.Value);
                            }
                        }
                    }
                    request.timer_stop("route_params");
                    request.template = route.template;

                    //valido oAuth
                    request.timer_start("oauth_validate");

                    if (!oAuth.Validate(request) && route.auth != "none")
                    {
                        //no es una sesion valida
                        request.timer_stop("oauth_validate");
                        if (route.auth == "user")
                        {
                            request.redirect = "/";
                            request.response.Add("response_url", Server.UrlEncode(request.full_page));
                            if (request.client != null)
                                request.response.Add("client_id", request.client.client_id);
                            throw new Exception("WEB_ERROR_USERS_1");
                        }
                        else if (route.auth == "client")
                        {
                            //request.redirect = "login";
                            //request.response.Add("redirect", Server.UrlEncode(request.full_page));
                            throw new Exception("WEB_ERROR_USERS_1");
                        }
                        throw new Exception("WEB_ERROR_USERS_1");
                    }

                    request.timer_stop("oauth_validate");

                    //mantener sesion activa
                    if (oAuth.RecalcExpire(request))
                    {
                        run = false;
                    }
                    else
                    {
                        if (route.auth == "user")
                        {
                            //cargo usuario
                            //if (!oAuth.SetSession(request) || request.user == null)
                            //verifico usuario cargado
                            if (request.user == null)
                            {
                                request.redirect = "/";
                                request.response.Add("response_url", Server.UrlEncode(request.page));
                                if(request.client != null)
                                    request.response.Add("client_id", request.client.client_id);
                                throw new Exception("WEB_ERROR_USERS_1");
                            }
                        }
                    }

                    if (run && route.target != null)
                    {
                        request.timer_start("dispatch");
                        Dispatcher.dispatch(request, (string)route.target);
                        request.timer_stop("dispatch");
                    }
                }
                else
                {
                    //Ruta no encontrada
                    //request.template = "error";
                    //throw new Exception("GEN0002");
                    request.status_code = 404;
                    request.status_msg = "Not Found";
                }
            }
            else
            {
                //Response Type desconocido
                //throw new Exception("GEN0001");
                request.status_code = 406;
                request.status_msg = "Not Acceptable";
            }

            //render page
            request.timer_start("render");
            request.render();
            request.timer_stop("render");

            //include headers
            Logger.Activity(Response.StatusCode + " " + Response.StatusDescription);
            Response.AddHeader("WS-Version", VERSION);
            Response.AddHeader("WS-Response-Duration", request.close() + "ms");
            timer.Stop();
            Logger.Timing("Tiempo total: " + timer.ElapsedMilliseconds + "ms");
        }
        catch (Exception ex)
        {
            while (ex.InnerException != null)
            {
                ex = ex.InnerException;
            }

            request.error(ex.Message);
        }
    }
}