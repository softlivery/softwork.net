﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Dynamic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

/// <summary>
/// Dynamic Model from query result
/// </summary>
public class ORM : DynamicObject
{
    protected string table;
    protected Dictionary<string, object> values;
    protected Dictionary<string, object> defaults;
    protected List<string> keys;
    protected bool is_new;
    //private Dictionary<string, Dictionary<string, object>> columns;

    public override bool TryGetMember(GetMemberBinder binder, out object result)
    {
        result = null;
        if (this.values.ContainsKey(binder.Name))
        {
            result = this.values[binder.Name];
            return true;
        }
        /*
        if (this.columns.ContainsKey(binder.Name))
        {
            result = this.columns[binder.Name]["value"];
            return true;
        }
        */
        return false;
    }

    public override bool TrySetMember(SetMemberBinder binder, object value)
    {
        if (this.values.ContainsKey(binder.Name))
        {
            this.values[binder.Name] = value;
            return true;
        }
        return false;
        /*
        if (this.columns.ContainsKey(binder.Name))
        {
            this.columns[binder.Name]["value"] = value;
        }
        else
        {
            this.columns.Add(binder.Name, new Dictionary<string, object>());
            this.columns[binder.Name].Add("name", binder.Name);
            this.columns[binder.Name].Add("value", value);
        }
        return true;
        */
    }

    public ORM(string table, bool hydrate = true)
	{
        this.initialize(table);
        if (hydrate && !this.hydrate_empty())
        {
            throw new Exception("Imposible crear objeto");
        }
    }

    private void initialize(string table)
    {
        this.table = table;
        this.values = new Dictionary<string, object>();
        this.defaults = new Dictionary<string, object>();
        this.keys = new List<string>();
        this.is_new = true;
        //this.columns = new Dictionary<string, Dictionary<string, object>>();
    }

    private static object value_fromDB(string type, object value)
    {
        if (value != DBNull.Value)
        {
            Logger.Debug(string.Format("ORM - Convert type DB {0}: {1}", type, value));
            switch (type.ToLower())
            {
                case "bigint":
                    value = value.ToString().Replace("(", "").Replace(")", "");
                    return Int64.Parse(value.ToString());
                case "int":
                    value = value.ToString().Replace("(", "").Replace(")", "");
                    return int.Parse(value.ToString());
                case "char":
                case "nvarchar":
                default:
                    return value.ToString();
            }
        }
        else
        {
            return null;
        }
    }

    private static string value_toDB(object value)
    {
        if (value == null)
        {
            return "NULL";
        }

        Logger.Debug(string.Format("ORM - Format type {0} to SQL: {1}", value.GetType().Name, value));
        switch (value.GetType().Name.ToLower())
        {
            case "string":
                return "'" + value + "'";
            case "datetime":
                //return "'" + value + "'";
                return "'" + ((DateTime)value).ToString("yyyy-MM-ddTHH:MM:ss") + "'";
            case "double":
                if (Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator.Equals(","))
                {
                    //return "'" + value.ToString() + "'";
                    return value.ToString().Replace(',','.');
                }
                return value.ToString();
            default:
                return value.ToString();
        }
    }

    protected bool hydrate_empty()
    {
        //leer columnas y valores desde la DB
        SqlDataReader reader = DBManager.query(string.Format("SELECT * FROM information_schema.columns where TABLE_NAME = '{0}'", this.table));
        if (reader != null && reader.HasRows)
        {
            try
            {
                while (reader.Read())
                {
                    this.defaults[reader.GetString(3)] = value_fromDB(reader.GetString(7), reader.GetValue(5));
                    this.values[reader.GetString(3)] = this.defaults[reader.GetString(3)];
                    Logger.Debug(string.Format("ORM - loaded column {0} ({1}): {2}", reader.GetString(3), this.values[reader.GetString(3)] != null ? this.values[reader.GetString(3)].GetType().Name : "null", this.values[reader.GetString(3)]));
                    //this.columns[reader.GetString(3)] = new Dictionary<string, object>();
                    //this.columns[reader.GetString(3)]["name"] = reader.GetString(3);
                    //this.columns[reader.GetString(3)]["nullable"] = reader.GetString(6).Equals("YES") ? true : false;
                    //this.columns[reader.GetString(3)]["type"] = reader.GetString(7);
                    //this.columns[reader.GetString(3)]["default"] = value_fromDB(reader.GetString(7), reader.GetString(5));
                    //this.columns[reader.GetString(3)]["value"] = value_fromDB(reader.GetString(7), reader.GetString(5));
                    //Logger.Debug(string.Format("ORM - loaded column {0} ({1}): {2}", reader.GetString(3), this.columns[reader.GetString(3)]["type"], this.columns[reader.GetString(3)]["value"]));
                }
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
            }
        }
        return false;
    }

    public static ORM Load(string table, Dictionary<string, object> keys)
    {
        ORM obj = new ORM(table, false);
        if (obj.hydrate_fromDB(keys))
        {
            return obj;
        }
        return null;
    }

    protected bool hydrate_fromDB(Dictionary<string, object> filters)
    {
        //seteo keys
        if (filters == null || filters.Count == 0)
        {
            return this.hydrate_empty();
        }

        string where = "";
        foreach (string column in filters.Keys)
        {
            where += " [" + column + "]=" + value_toDB(filters[column]) + " AND";
        }
        where = where.Substring(0, where.Length - 4);
        Logger.Debug("ORM - Get information for " + where);

        //obtener columnas y valores desde una consulta where keys
        SqlDataReader reader = DBManager.query(string.Format("SELECT * FROM [{0}] WHERE {1}", this.table, where));
        if (reader!=null && reader.HasRows)
        {
            try
            {
                reader.Read();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    this.defaults[reader.GetName(i)] = reader.GetValue(i) != DBNull.Value ? reader.GetValue(i) : null;
                    this.values[reader.GetName(i)] = this.defaults[reader.GetName(i)];
                    Logger.Debug(string.Format("ORM - loaded column {0} ({1}): {2}", reader.GetName(i), this.values[reader.GetName(i)] != null ? this.values[reader.GetName(i)].GetType().Name : "null", this.values[reader.GetName(i)]));
                    //this.columns[reader.GetName(i)] = new Dictionary<string, object>();
                    //this.columns[reader.GetName(i)]["name"] = reader.GetName(i);
                    //this.columns[reader.GetName(i)]["nullable"] = false;
                    //this.columns[reader.GetName(i)]["type"] = reader.GetValue(i).GetType().Name.ToLower();
                    //this.columns[reader.GetName(i)]["default"] = reader.GetValue(i);
                    //this.columns[reader.GetName(i)]["value"] = reader.GetValue(i);
                    //Logger.Debug(string.Format("ORM - loaded column {0} ({1}): {2}", reader.GetName(i), this.columns[reader.GetName(i)]["type"], this.columns[reader.GetName(i)]["value"]));
                }
                this.is_new = false;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e.Message);
            }
        }
        return false;
    }

    public bool loaded()
    {
        return !this.is_new;
    }

    public bool save()
    {
        if (this.is_new)
        {
            return this.insert();
        }
        return this.update();
    }

    private bool insert()
    {
        try
        {
            string values = "";
            string columns = "";
            /*
            foreach (string column in this.columns.Keys)
            {
                columns += "[" +column + "],";
                values += "'" + this.columns[column]["value"] + "',";
            }
            */
            foreach (string column in this.values.Keys)
            {
                if (!this.keys.Contains(column))
                {
                    columns += "[" + column + "],";
                    values += value_toDB(this.values[column]) + ",";
                }
            }

            if (columns == "")
                return false;

            columns = columns.Substring(0, columns.Length - 1);
            values = values.Substring(0, values.Length - 1);

            if (this.keys.Count == 1)
            {
                object inserted;
                SqlDataReader reader = DBManager.query(string.Format("INSERT INTO [{0}] ({1}) OUTPUT INSERTED.{2} VALUES ({3})", this.table, columns, this.keys[0], values), out inserted);
                if (reader != null)
                {
                    Logger.Debug("ORM - Last inserted id: " + inserted.ToString());
                    this.values[this.keys[0]] = inserted;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                SqlDataReader reader = DBManager.query(string.Format("INSERT INTO [{0}] ({1}) VALUES ({2})", this.table, columns, values));
                if (reader != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        catch (Exception e)
        {
            Logger.Error("ORM - " + e.Message);
            return false;
        }
    }

    private bool update()
    {
        try
        {
            string columns = "";
            string key = "";

            if (this.keys.Count == 0)
            {
                return false;
            }

            /*
            foreach (string column in this.keys)
                if (this.columns[column]["value"] != null)
                    key += " [" + column + "]='" + this.columns[column]["value"] + "' AND";
            */
            foreach (string column in this.keys)
            {
                if (this.values[column] != null)
                {
                    key += " [" + column + "]=" + value_toDB(this.values[column]) + " AND";
                }
            }

            key = key.Substring(0, key.Length - 4);
            Logger.Debug("Update information for " + key);

            /*
            foreach (Dictionary<string, object> column in this.columns.Values)
                if (!this.keys.Contains(column["name"]))
                    columns += "[" + column["name"] + "]='" + column["value"] + "',";
            */
            foreach (string column in this.values.Keys)
            {
                if (!this.keys.Contains(column))
                {
                    columns += " [" + column + "]=" + value_toDB(this.values[column]) + ",";
                }
            }
            columns = columns.Substring(0, columns.Length - 1);

            SqlDataReader reader = DBManager.query(string.Format("UPDATE [{0}] SET {1} WHERE {2}", this.table, columns, key));
            if (reader != null)
                return true;
            else
                return false;
        }
        catch (Exception e)
        {
            Logger.Error("ORM - " + e.Message);
            return false;
        }
    }

    public void reset()
    {
        this.values = new Dictionary<string, object>();
        foreach (string column in this.defaults.Keys)
        {
            this.values[column] = this.defaults[column];
        }
    }

    /// <summary>
    /// first_name like bob and age>30 order by last_name ASC, age DESC
    /// /PEOPLE?first_name=bob min-age=30 sort=last_name.asc,age.desc page=3 per_page=100
    /// </summary>
    /// <param name="parameters">
    /// filters using modifier:
    ///  less-: less than
    ///  less_equal-: less than or equal
    ///  greater-: greater than
    ///  greater_equal-: grater than or equal
    ///  left_like-: %value
    ///  right_like-: value%
    ///  like-: %value%
    ///  between-: min-max
    ///  in-: v1,v2,v3,v4
    ///  (equal): without modifier
    /// embed: embebed_table.embebed_table_column.current_table_column
    /// fields: columns to select
    /// sort using modifiers:
    ///  asc-: ascending
    ///  desc-: descending
    /// </param>
    /// <returns></returns>
    public List<Dictionary<string, object>> search(Dictionary<String, Object> parameters = null)
    {
        if (parameters == null)
        {
            parameters = new Dictionary<String, Object>();
        }
        List<Dictionary<string, object>> related;
        return this.search(parameters, out related);
    }

    /// <summary>
    /// first_name like bob and age>30 order by last_name ASC, age DESC
    /// 
    /// /PEOPLE?first_name=bob min-age=30 sort=last_name.asc,age.desc page=3 per_page=100
    /// </summary>
    /// <param name="parameters">
    /// filters using modifier:
    ///  less-: less than
    ///  less_equal-: less than or equal
    ///  greater-: greater than
    ///  greater_equal-: grater than or equal
    ///  left_like-: %value
    ///  right_like-: value%
    ///  like-: %value%
    ///  between-: min-max
    ///  in-: v1,v2,v3,v4
    ///  (equal): without modifier
    /// embed: embebed_table.embebed_table_column.current_table_column
    /// fields: columns to select
    /// sort using modifiers:
    ///  asc-: ascending
    ///  desc-: descending
    ///  include:  n+1 problem resolved with query-and-stitch
    ///     1 query to load the master set, a collection of rows.
    ///     1 query to load the related set, a collection of rows using IN filter in master set IDs and ordered by the same ID col.
    // GET /oauth_clients?include=oauth_users_clients.client.id
    // Master Set: SELECT * FROM oauth_clients t; Ex: client.id=1
    // Related Set: SELECT * FROM oauth_users_clients t WHERE t.client IN ('1');
    /// </param>
    /// <param name="related_results">
    /// OUT List of related rows
    /// </param>
    /// <returns></returns>
    public List<Dictionary<string, object>> search(Dictionary<string, object> parameters, out List<Dictionary<string, object>> related_results)
    {
        List<Dictionary<string, object>> results = new List<Dictionary<string, object>>();
        related_results = null;

        //Sorting
        var order = "";
        if (parameters.ContainsKey("sort"))
        {
            var sort = parameters["sort"].ToString();
            Logger.Debug("Object (" + this.table + "): search sorting " + sort);
            StringBuilder builder = new StringBuilder();
            foreach (string column in sort.ToString().Split(','))
            {
                var col = column.Substring(0, column.IndexOf("."));
                var val = column.Substring(column.IndexOf(".") + 1).ToUpper();
                if (val == "ASC" || val == "DESC")
                    builder.Append("'" + col + "' " + val + ",");
            }
            if (builder.Length > 0)
                builder.Remove((builder.Length - 1), 1);
            order = " ORDER BY " + builder.ToString();
            parameters.Remove("sort");

            //Pagination
            if (!parameters.ContainsKey("page"))
                parameters["page"] = 0;

            if (!parameters.ContainsKey("per_page"))
                parameters["per_page"] = 20;

            Logger.Debug("Object (" + this.table + "): search pagination " + parameters["page"] + ", " + parameters["per_page"]);
            int offset = int.Parse(parameters["page"].ToString()) * int.Parse(parameters["per_page"].ToString());
            order += string.Format(" OFFSET {0} ROWS FETCH NEXT {1} ROWS ONLY", offset, parameters["per_page"]);
        }
        parameters.Remove("page");
        parameters.Remove("per_page");

        //Join
        var joins = "";
        var join_tables = new Dictionary<string, string>();
        if (parameters.ContainsKey("embed"))
        {
            // GET /users?embed=oauth_users_clients.user.id
            // SELECT * FROM users t LEFT JOIN oauth_users_clients t1 ON t1.[user] = t.[id];

            var join = parameters["embed"].ToString().Split('.');
            if (join.Length == 3)
            {
                if (joins.Equals(""))
                {
                    joins = " LEFT JOIN";
                }
                joins += string.Format(" {0} t1 ON t1.[{1}]=t.[{2}]", join);
                join_tables.Add(join[0], "t1");
            }

            parameters.Remove("embed");
        }

        //Fields selection
        var fields = "*";
        if (parameters.ContainsKey("fields"))
        {
            if (Regex.IsMatch(parameters["fields"].ToString(), @"^((\w+.)?\w+(\s+[asAS]+\s+\w+)?,?\s*)+$"))
            {
                // GET /users/1?fields=id,name
                // SELECT id,name FROM users t WHERE t.[id] = '1'
                StringBuilder builder = new StringBuilder();
                var fields_array = parameters["fields"].ToString().Split(',');
                foreach (string field in fields_array)
                {
                    var field_table = "t";
                    var field_name = field;
                    if (Regex.IsMatch(field_name, @"^\w+\."))
                    {
                        var field_array = field_name.Split('.');
                        if (field_array.Length == 2)
                        {
                            if (join_tables.ContainsKey(field_array[0]))
                                field_table = join_tables[field_array[0]];
                            field_name = field_array[1];
                        }
                    }
                    if (Regex.IsMatch(field_name, @"^(\w+\s+[asAS]+\s+\w+)+$"))
                    {
                        var field_array = field_name.Split(' ');
                        if (field_array.Length == 3)
                            builder.Append(field_table + ".[" + field_array[0] + "] AS [" + field_array[2] + "],");
                    }
                    else
                    {
                        builder.Append(field_table + ".[" + field_name + "],");
                    }
                }
                if (builder.Length > 0)
                {
                    builder.Remove((builder.Length - 1), 1);
                    fields = builder.ToString();
                }
            }
            parameters.Remove("fields");
        }

        //Include
        var include = "";
        if (parameters.ContainsKey("include"))
        {
            if (Regex.IsMatch(parameters["include"].ToString(), @"^(\w+.?)+$"))
            {
                include = parameters["include"].ToString();
            }
            parameters.Remove("include");

        }

        //Filters
        var filters = this.params2filters(parameters);
        Logger.Debug("Object (" + this.table + "): search filters " + string.Join(" ", filters.Values));
        var where = "";
        if (filters.Count > 0)
            where = " WHERE " + string.Join(" AND ", filters.Values);

        SqlDataReader reader = DBManager.query(string.Format("SELECT {1} FROM [{0}] t{2}{3}{4}", this.table, fields, joins, where, order));

        //parse rows
        if (reader != null)
        {
            while (!reader.IsClosed && ((dynamic)reader).Read())
            {
                Dictionary<string, object> row = new Dictionary<string, object>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    row.Add(reader.GetName(i), reader.GetValue(i));
                }
                results.Add(row);
            }
            reader.Close();

            //Include, n+1 problem resolved with query-and-stitch
            if (!include.Equals(""))
            {
                // 1 query to load the master set, a collection of rows.
                // 1 query to load the related set, a collection of rows using IN filter in master set IDs and ordered by the same ID col.

                // GET /oauth_clients?include=oauth_users_clients.client.id
                // Master Set: SELECT * FROM oauth_clients t; Ex: client.id=1
                // Related Set: SELECT * FROM oauth_users_clients t WHERE t.client IN ('1');
                var include_array = include.Split('.');
                if (include_array.Length == 3)
                {
                    StringBuilder builder = new StringBuilder();
                    foreach (Dictionary<string, object> row in results)
                    {
                        builder.Append(row[include_array[2]].ToString() + ",");
                    }
                    if (builder.Length > 0)
                    {
                        builder.Remove((builder.Length - 1), 1); //remove last ,
                    }

                    Dictionary<String, Object> related_params = new Dictionary<string, object>();
                    related_params.Add("in-" + include_array[1], builder.ToString());
                    if (!builder.ToString().Equals(""))
                    {
                        ORM related_model = new ORM(include_array[0]);
                        List<Dictionary<string, object>> related_results_tmp;
                        related_results = related_model.search(related_params, out related_results_tmp);
                    }
                }
            }
        }

        return results;
    }

    private Dictionary<string, string> params2filters(Dictionary<String, Object> parameters)
    {
        Dictionary<string, string> filters = new Dictionary<string, string>();

        foreach (KeyValuePair<string, object> objColumn in parameters)
        {
            string modifier = "";
            string column = objColumn.Key;

            if (column.IndexOf("-") > -1)
            {
                modifier = column.Substring(0, column.IndexOf("-")).ToLower();
                column = column.Substring(column.IndexOf("-") + 1);
            }

            //if (this.columns.ContainsKey(column))
            {
                switch (modifier)
                {
                    case "less":
                        filters.Add(column, string.Format("t.[{0}] < '{1}'", column, objColumn.Value));
                        break;
                    case "less_equal":
                        filters.Add(column, string.Format("t.[{0}] <= '{1}'", column, objColumn.Value));
                        break;
                    case "greater":
                        filters.Add(column, string.Format("t.[{0}] > '{1}'", column, objColumn.Value));
                        break;
                    case "greater_equal":
                        filters.Add(column, string.Format("t.[{0}] >= '{1}'", column, objColumn.Value));
                        break;
                    case "postfix":
                        filters.Add(column, string.Format("t.[{0}] LIKE '%{1}'", column, objColumn.Value));
                        break;
                    case "prefix":
                        filters.Add(column, string.Format("t.[{0}] LIKE '{1}%'", column, objColumn.Value));
                        break;
                    case "like":
                        filters.Add(column, string.Format("t.[{0}] LIKE '%{1}%'", column, objColumn.Value));
                        break;
                    case "between":
                        var min = objColumn.Value.ToString().Substring(0, objColumn.Value.ToString().IndexOf("-"));
                        var max = objColumn.Value.ToString().Substring(objColumn.Value.ToString().IndexOf("-") + 1);
                        filters.Add(column, string.Format("t.[{0}] BETWEEN '{1}' AND '{2}'", column, min, max));
                        break;
                    case "in":
                        if (Regex.IsMatch(objColumn.Value.ToString(), @"^(\w+,?)+$"))
                        {
                            StringBuilder builder = new StringBuilder();
                            string[] values = objColumn.Value.ToString().Split(',');
                            foreach (string value in values)
                            {
                                builder.Append("'" + value + "',");
                            }
                            if (builder.Length > 0)
                            {
                                builder.Remove((builder.Length - 1), 1); //remove last ,
                            }
                            filters.Add(column, string.Format("t.[{0}] IN ({1})", column, builder.ToString()));
                        }
                        break;
                    default:
                        filters.Add(column, string.Format("t.[{0}] = '{1}'", column, objColumn.Value));
                        break;
                }
            }
        }

        return filters;
    }

    /*
    public object get_value(string property)
    {
        if (this.columns.ContainsKey(property))
        {
            return this.columns[property]["value"];
        }
        return null;
    }

    private static List<string> GetColumnNames(SqlDataReader reader)
    {
        List<string> columns = new List<string>();
        if (reader != null && !reader.IsClosed)
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                columns.Add(reader.GetName(i));
            }
        }

        return columns;
    }

    public bool load(List<string> ids)
    {
        string key = "";
        if (ids.Count == this.keys.Count)
        {
            int i = 0;
            foreach (string column in this.keys)
            {
                key += " [" + column + "]='" + ids[i] + "' AND";
                i++;
            }
            key = key.Substring(0, key.Length - 4);
            Logger.Debug("Get information for " + key);

            string query = string.Format("SELECT * FROM [{0}] WHERE {1}", this.table, key);
            SqlDataReader reader = DBManager.query(query);

            //parse rows
            if (reader.HasRows)
            {
                try
                {
                    while (reader.Read())
                    {
                        i = 0;
                        foreach (string column in this.columns.Keys)
                        {
                            //Logger.Debug(string.Format("Loading column: {0}", column));
                            this.columns[column]["value"] = reader.GetValue(i);
                            i++;
                        }
                        return true;
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e.Message);
                }
            }
        }
        return false;
    }

    public bool update()
    {
        string columns = "";
        string key = "";

        if (this.keys.Count == 0)
        {
            return false;
        }

        foreach (string column in this.keys)
            if (this.columns[column]["value"] != null)
                key += " [" + column + "]='" + this.columns[column]["value"] + "' AND";
        key = key.Substring(0, key.Length - 4);
        Logger.Debug("Update information for " + key);

        foreach (Dictionary<string, object> column in this.columns.Values)
            if (!this.keys.Contains(column["name"]))
                columns += "[" + column["name"] + "]='" + column["value"] + "',";

        columns = columns.Substring(0, columns.Length - 1);

        string query = "UPDATE " + this.table + " SET " + columns + " WHERE " + key;

        try
        {
            SqlDataReader reader = DBManager.query(query);
            if (reader != null)
                return true;
            else
                return false;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public bool delete(Dictionary<string, object> filters)
    {
        string key = "";

        foreach (string property in filters.Keys)
        {
            key += " [" + property + "]='" + filters[property] + "' AND";
        }
        key = key.Substring(0, key.Length - 4);
        Logger.Debug("Delete " + key);

        string query = "DELETE FROM " + this.table + " WHERE " + key;

        try
        {
            SqlDataReader reader = DBManager.query(query);
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public List<string> getPropertiesNames()
    {
        List<string> names = new List<string>();
        if (this.columns != null)
            foreach (string key in this.columns.Keys)
                names.Add(key);
        return names;
    }

    public Dictionary<string, object> getProperties()
    {
        Dictionary<string, object> names = new Dictionary<string, object>();
        foreach (string key in this.columns.Keys)
            names.Add(key,this.columns[key]["value"]);
        return names;
    }

    public List<string> getKeys()
    {
        return this.keys;
    }

    public void setKey(string key)
    {
        this.keys.Add(key);
    }

    protected void setColumn(string name)
    {
        if (!this.columns.ContainsKey(name))
        {
            this.columns.Add(name, new Dictionary<string, object>());
            this.columns[name].Add("name", name);
            this.columns[name].Add("value", null);
        }
    }
    */
}