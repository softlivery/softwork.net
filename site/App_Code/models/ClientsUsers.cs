﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ClientsUsers
/// </summary>
public class ClientsUsers : ORM
{
    const string table = "oauth_clients_users";

    public ClientsUsers()
        : base(table, true)
	{
        //this.setColumns();
	}

    public ClientsUsers(int client, string user)
        : base(table, false)
    {
        Dictionary<string, object> filters = new Dictionary<string, object>();
        filters.Add("client", client);
        filters.Add("user", user);
        if (this.hydrate_fromDB(filters))
            this.setColumns();
        else
            this.hydrate_empty();
       
    }

    private void setColumns()
    {
        this.keys.Add("client");
        this.keys.Add("user");
    }

    private ClientsUsers(bool hydrate = true)
        : base(table, hydrate)
    {
        //this.setColumns();
    }

    public static ClientsUsers fromAccessToken(string access_token)
    {
        ClientsUsers cu = new ClientsUsers(false);
        Dictionary<string, object> filters = new Dictionary<string, object>();
        filters.Add("access_token", access_token);
        if (cu.hydrate_fromDB(filters))
        {
            return cu;
        }
        return null;
    }

    public static ClientsUsers fromRefreshToken(string refresh_token)
    {
        ClientsUsers cu = new ClientsUsers(false);
        Dictionary<string, object> filters = new Dictionary<string, object>();
        filters.Add("refresh_token", refresh_token);
        if (cu.hydrate_fromDB(filters))
        {
            return cu;
        }
        return null;
    }

    public static ClientsUsers fromUser(string user)
    {
        ClientsUsers cu = new ClientsUsers(false);
        Dictionary<string, object> filters = new Dictionary<string, object>();
        filters.Add("user", user);
        filters.Add("client", 3);
        if (cu.hydrate_fromDB(filters))
        {
            return cu;
        }
        return null;
    }
}