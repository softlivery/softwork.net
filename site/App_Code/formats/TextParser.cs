﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TextParser
/// </summary>
public class TextParser : Parser
{
    private RequestWrapper request;

	public TextParser(RequestWrapper request)
	{
        this.request = request;
	}

    public Object decode(string encoded)
    {
        return null;
    }

    public String encode()
    {
        return (string)this.request.response["text"];
    }

    public String encode(object decoded)
    {
        return decoded.ToString();
    }


    public void render()
    {
        this.request.response_obj.Write(this.encode());
    }

    public void error(string msg)
    {
        this.request.status_code = 200;
        this.request.status_msg = "OK";
        this.request.response = new Dictionary<string, object>();
        this.request.response.Add("text", msg);
        this.render();
    }
}