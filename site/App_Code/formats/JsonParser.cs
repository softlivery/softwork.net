﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Helpers;
using System.Dynamic;
using Newtonsoft.Json;

public class JsonParser : Parser
{
    private RequestWrapper request;
    private JsonSerializer serializer;

    public JsonParser(RequestWrapper request)
    {
        this.request = request;
        this.serializer = new JsonSerializer();
    }

    public Object decode(string encoded)
    {
        Object obj = (Object) Json.Decode(encoded);
        //Object obj = JsonConvert.DeserializeObject(encoded);
        return obj;
    }

    public String encode()
    {
        return Json.Encode(this.request.response);
        //return JsonConvert.SerializeObject(this.request.response);
    }

    public String encode(object decoded)
    {
        return Json.Encode(decoded);
        //return JsonConvert.SerializeObject(decoded);
    }


    public void render()
    {
        if (this.request.user_updated && !this.request.response.ContainsKey("session"))
        {
            this.request.response.Add("session", this.request.session);
        }
        this.request.response_obj.Write(this.encode());
    }


    public void error(string msg)
    {
        this.request.status_code = 200;
        this.request.status_msg = "OK";
        this.request.response = new Dictionary<string, object>();
        this.request.response.Add("error", msg);
        this.request.render();
    }
}

public class JsonMemberBinder : GetMemberBinder
{
    public JsonMemberBinder(string name, bool ignoreCase)
        : base(name, ignoreCase)
    {
    }

    public override DynamicMetaObject FallbackGetMember(DynamicMetaObject target, DynamicMetaObject errorSuggestion)
    {
        throw new NotImplementedException();
    }
}