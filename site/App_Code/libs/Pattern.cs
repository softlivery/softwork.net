﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;


public class Pattern
{
    private string name;
    private string pattern;
    private string color;
    private bool bold;
    private bool show;
    private string data;
    private string date;
    private string server;
    private string type;
    private string callid;

    /***
        * 
        * 
        * 
        * */
    public Pattern(String pName, String pPattern, String pColor)
    {
        this.name = pName;
        this.pattern = pPattern;
        this.color = pColor;
        this.bold = false;
        this.show = true;
    }

    public String toCSS()
    {
        String ret = ".class_" + this.name + ", " + this.name + " {\n";
        ret += "   color: " + this.color + ";\n";
        if (this.bold)
        {
            ret += "   font-weight: bold;\n";
        }
        if (!this.show)
        {
            ret += "   display: none;\n";
        }
        ret += "}\n";
        return ret;
    }

    public String toHTML(String line)
    {
        String ret = "<span class=\"class_" + this.name + "\">" + line + "\n</span><br />";
        return ret;
    }

    public String toHTML()
    {
        String ret = "<span class=\"class_" + this.name + "\">" + this.date + " " + this.callid + " " + this.type + " " + this.data + "</span><br />";
        return ret;
    }

    public String toJson(String line)
    {
        String ret = "{\"event\":\"" + this.name + "\",\"data\":\"" + line + "\"}";
        return ret;
    }

    public String toJson()
    {
        String ret = "{\"event\":\"" + this.name + "\",\"data\":\"" + this.data.Replace("\"", "\\\"") + "\",\"date\":\"" + this.date + "\",\"server\":\"" + this.server + "\",\"type\":\"" + this.type + "\",\"callid\":\"" + this.callid + "\"}";
        return ret;
    }

    public String toXML(String line)
    {
        String ret = "<" + this.name + ">" + line + "</" + this.name + ">";
        return ret;
    }

    public bool matches(String line)
    {
        Match res = Regex.Match(line, this.pattern, RegexOptions.IgnoreCase);
        if (res.Success)
        {
            String[] tmp = Regex.Split(line, @"[\s>]+", RegexOptions.IgnoreCase);
            this.date = tmp[0] + " " + tmp[1];
            this.callid = tmp[2];
            this.type = tmp[3];
            this.data = "";
            for (int i = 4; i < tmp.Length; i++)
                this.data += tmp[i] + " ";
            return true;
        }
        return false;
    }
}