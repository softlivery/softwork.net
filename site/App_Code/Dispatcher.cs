﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using System.Diagnostics;

/// <summary>
/// Summary description for Dispatcher
/// </summary>
public class Dispatcher
{
    public Dispatcher()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static object dispatch(RequestWrapper request, string target)
    {
        string[] seps = { "::" };
        string[] parts = target.Split(seps, StringSplitOptions.None);

        if (parts.Length > 1)
        {
            string controller_name = parts[0];
            string action_name = parts[1];

            Assembly assem = typeof(Controller).Assembly;
            object controller = null;
            Type controller_type = null;
            MethodInfo action = null;

            controller = assem.CreateInstance(controller_name);
            //controller.Initialize();
            controller_type = controller.GetType();
            action = controller_type.GetMethod(action_name);

            if (action != null)
            {
                Logger.Activity("Executing " + action_name + " from " + controller_name);
                return action.Invoke(controller, new object[] { request });
            }
        }
        return null;
    }
    /*
    public static object dispatchDLL(string target, object[] action_args, object[] controller_args, out int error_code)
    {
        error_code = -1;
        bool HayError;
        List<WalletFunction.Entities.RespuestasAPI> respuestas;
        object result = Dispatcher.dispatchDLL(target, ref action_args, controller_args, out respuestas, out HayError);
        if(HayError) {
            error_code = 0;
        }
        if (respuestas != null && respuestas.Count > 0 && respuestas[0] != null)
        {
            error_code = (int)respuestas[0].codigo;
        }
        return result;
    }
    */
    public static object dispatchDLL(string target, object[] action_args, object[] controller_args)
    {
        List<GearUp_API.RespuestasAPI> respuestas;
        bool HayError;
        return Dispatcher.dispatchDLL(target, ref action_args, controller_args, out respuestas, out HayError);
    }
    /*
    public static object dispatchDLL(string target, object[] action_args, object[] controller_args, out bool HayError)
    {
        List<WalletFunction.Entities.RespuestasAPI> respuestas;
        return Dispatcher.dispatchDLL(target, ref action_args, controller_args, out respuestas, out HayError);
    }
    */

    public static object dispatchDLL(string target, object[] action_args, object[] controller_args, out List<GearUp_API.RespuestasAPI> respuestas, out bool HayError)
    {
        return Dispatcher.dispatchDLL(target, ref action_args, controller_args, out respuestas, out HayError);
    }

    public static object dispatchDLL(string target, ref object[] action_args, object[] controller_args, out List<GearUp_API.RespuestasAPI> respuestas, out bool HayError)
    {
        Stopwatch timer = Stopwatch.StartNew();
        object result = null;
        respuestas = null;
        HayError = false;

        string[] seps = { "::" };
        string[] parts = target.Split(seps, StringSplitOptions.None);

        if (parts.Length > 1)
        {
            string controller_name = parts[0];
            string action_name = parts[1];

            Assembly assem = typeof(GearUp_API.UsuariosBL).Assembly;
            GearUp_API.Mensajes controller = null;
            Type controller_type = null;
            MethodInfo action = null;

            Stopwatch t = Stopwatch.StartNew();
            controller = (GearUp_API.Mensajes)assem.CreateInstance("GearUp_API." + controller_name, false, BindingFlags.Default, null, controller_args, null, null);
            Logger.Timing("Tiempo de instancia DLL: " + t.ElapsedMilliseconds + "ms");

            if (controller != null)
            {
                Type[] parameters_type = new Type[action_args.Length];
                for (int i = 0; i < action_args.Length; i++)
                {
                    parameters_type[i] = action_args[i].GetType();
                }

                controller_type = controller.GetType();
                action = controller_type.GetMethod(action_name, parameters_type);

                if (action == null)
                {
                    action = controller_type.GetMethod(action_name);
                }

                if (action != null)
                {
                    Logger.Debug("Executing " + action_name + " from " + controller_name);
                    try
                    {
                        result = action.Invoke(controller, action_args);
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex.Message);
                        //Logger.Debug(ex.StackTrace);
                        if (ex.InnerException != null)
                        {
                            ex = ex.InnerException;
                            Logger.Error(ex.Message);
                            //Logger.Debug(ex.StackTrace);
                        }
                        throw new Exception("DLL0000");
                    }
                    HayError = controller.HayError;
                    respuestas = controller.Respuestas;

                    if (controller.HayRespuesta())
                    {
                        foreach (GearUp_API.RespuestasAPI respuesta in controller.Respuestas)
                        {
                            if (respuesta != null)
                                Logger.Debug("Dispatcher - dispatchDLL: respuestasAPI " + respuesta.codigo + " - " + respuesta.descripcion);
                        }
                    }

                    /*if (controller.HayError)
                    {

                        throw new Exception("DLL0001");
                    }*/
                }
                else
                {
                    throw new Exception("DLL0002");
                }
            }
            else
            {
                throw new Exception("DLL0003");
            }
        }
        Logger.Timing("Tiempo total DLL: " + timer.ElapsedMilliseconds + "ms");
        return result;
    }
}