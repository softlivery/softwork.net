﻿using System;
using System.Collections.Generic;

public interface Controller
{
    void Initialize();

    Dictionary<String, Object> get(RequestWrapper request);

    Dictionary<String, Object> post(RequestWrapper request);
}