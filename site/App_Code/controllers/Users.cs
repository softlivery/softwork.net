﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Users
/// </summary>
public class Users
{
	public Users()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static void registrarse(RequestWrapper request)
    {
        List<GearUp_API.Paises> paises = (List<GearUp_API.Paises>)Dispatcher.dispatchDLL("PaisesBL::ObtenerLista", new object[] { }, new object[] { request.lang });
        if (paises == null)
        {
            throw new Exception("WEB_ERROR_USERS_1");
        }
        request.response.Add("paises", paises);

        List<GearUp_API.TimeZones> timezones = (List<GearUp_API.TimeZones>)Dispatcher.dispatchDLL("TimeZonesBL::ObtenerLista", new object[] { }, new object[] { request.lang });
        if (timezones == null)
        {
            throw new Exception("WEB_ERROR_USERS_1");
        }
        request.response.Add("timezones", timezones);

        if (!request.verb.Contains("GET"))
        {
            Logger.Debug("Users registrarse - POST");

            bool error = false;
            List<GearUp_API.RespuestasAPI> respuestas = new List<GearUp_API.RespuestasAPI>();
            GearUp_API.Usuarios user = new GearUp_API.Usuarios();

            foreach (var prop in user.GetType().GetProperties())
            {
                if (request.parameters.ContainsKey(prop.Name))
                {
                    try
                    {
                        object tmp = request.parameters[prop.Name];
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            if (!request.parameters[prop.Name].Equals(""))
                            {
                                tmp = DateTime.ParseExact(tmp.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                //tmp = DateTime.ParseExact(tmp.ToString(), request.culture.DateTimeFormat.ShortDatePattern, request.culture);
                            }
                            else
                            {
                                tmp = null;
                            }
                        }
                        else if (prop.PropertyType.FullName.Contains("Decimal"))
                        {
                            decimal dec;
                            if (decimal.TryParse(tmp.ToString(), out dec))
                            {
                                tmp = dec;
                            }
                            else
                            {
                                tmp = 0.0m;
                            }
                        }
                        else if (prop.PropertyType.FullName.Contains("Int16"))
                        {
                            Int16 dec;
                            if (Int16.TryParse(tmp.ToString(), out dec))
                            {
                                tmp = dec;
                            }
                            else
                            {
                                tmp = 0;
                            }
                        }
                        else if (prop.PropertyType.FullName.Contains("Int"))
                        {
                            int dec;
                            if (int.TryParse(tmp.ToString(), out dec))
                            {
                                tmp = dec;
                            }
                            else
                            {
                                tmp = 0;
                            }
                        }
                        else
                        {
                            tmp = Convert.ChangeType(request.parameters[prop.Name], prop.PropertyType);
                        }
                        prop.SetValue(user, tmp, null);
                        Logger.Debug("Usuarios actualizando: " + prop.Name);
                    }
                    catch (Exception e)
                    {
                        Logger.Error(prop.Name + ": " + prop.PropertyType);
                        Logger.Error(e.Message);
                        error = true;
                    }
                }
            }
            request.response.Add("usuario", user);

            if(!request.parameters["Pass"].Equals(request.parameters["PassVerif"]))
                throw new Exception("WEB_ERROR_USERS_2");

            if (!error && (bool)Dispatcher.dispatchDLL("UsuariosBL::Registrar", new object[] { user }, new object[] { request.lang }, out respuestas, out error))
            {
                //validación de persona, el usuario fue creado en la DB
                request.response = new Dictionary<string, object>();
                dynamic client = ClientModel.fromClientId(ConfigManager.GET("OauthClientID"));

                //oAuth generar code
                Dictionary<string, string> generated = oAuth.Implicit(client, user.Email);

                if (generated.ContainsKey("error"))
                {
                    throw new Exception(generated["error"]);
                }
                else
                {
                    Logger.Activity("Token generated: " + generated["access_token"]);
                    request.response.Remove("usuario");
                    request.response.Add("client_id", ConfigManager.GET("OauthClientID"));
                    //request.response.Add("delivered", generated["delivered"]);
                    //request.response.Add("expire", generated["expire"]);
                    request.response.Add("access_token", generated["access_token"]);
                    //request.response.Add("refresh_token", generated["refresh_token"]);
                    //request.response.Add("token_type", generated["token_type"]);
                    //request.response.Add("scope", generated["scope"]);
                    request.redirect = "registro_validacion";
                    return;
                }
            }
            else
            {
                if (respuestas.Count > 0)
                    request.response.Add("errores", respuestas);
                else
                    request.response.Add("result", false);

                return;
            }

            throw new Exception("WEB_ERROR_USERS_3");
        }
    }

    public static void login(RequestWrapper request)
    {
        Logger.Debug("Users login");

        //verifico si es otro aplicativo o la web
        if (request.parameters.ContainsKey("client_id"))
        {
            request.response.Add("client_id", request.parameters["client_id"]);
        }
        else
        {
            request.response.Add("client_id", ConfigManager.GET("OauthClientID"));
        }

        if (!request.verb.Contains("GET"))
        {
            //verificacion del cliente
            /*if (request.client == null)
            {
                throw new Exception("USR0031");
            }*/

            //verificacion de parametros
            if (!request.parameters.ContainsKey("email") || request.parameters["email"].Equals(""))
            {
                throw new Exception("WEB_ERROR_USERS_1");
            }
            if (!request.parameters.ContainsKey("password") || request.parameters["password"].Equals(""))
            {
                throw new Exception("WEB_ERROR_USERS_1");
            }

            string email = request.parameters["email"];
            string password = request.parameters["password"];

            /*
            string IPAddress = "";
            IPHostEntry Host = default(IPHostEntry);
            string Hostname = null;
            Hostname = System.Environment.MachineName;
            Host = Dns.GetHostEntry(Hostname);
            foreach (IPAddress IP in Host.AddressList)
            {
                if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    IPAddress = Convert.ToString(IP);
                }
            }
            */
            string IPAddress = request.request.UserHostAddress;

            List<GearUp_API.RespuestasAPI> respuestas;
            bool error;
            GearUp_API.Usuarios result = (GearUp_API.Usuarios)Dispatcher.dispatchDLL("UsuariosBL::Login", new object[] { email, password, IPAddress }, new object[] { request.lang }, out respuestas, out error);

            if (result == null)
            {
                if (respuestas.Count > 0)
                {
                    throw new Exception(respuestas[0].descripcion);
                }
                throw new Exception("WEB_ERROR_USERS_1");
            }
            else
            {
                Logger.Debug("Usuario encontrado");

                try
                {
                    //oAuth generar code
                    //Dictionary<string, string> generated = oAuth.Implicit(request.client.client_id, result.ANI, request.client.scope);
                    Dictionary<string, string> generated = oAuth.Implicit(request.client, result.Email);

                    if (generated.ContainsKey("error"))
                    {
                        throw new Exception(generated["error"]);
                    }
                    else
                    {
                        Logger.Activity("Token generated: " + generated["access_token"]);

                        if (request.is_web())
                        {
                            //request.response.Add("client_id", request.client.client_id);
                            request.response.Add("access_token", generated["access_token"]);

                            if (request.parameters.ContainsKey("response_url") && !request.parameters["response_url"].Equals(""))
                            {
                                //request.response.Remove("response_url");
                                request.redirect = request.parameters["response_url"];
                            }
                            else
                            {
                                //request.redirect = "inicio";
                                request.redirect = request.client.response_url;
                            }
                        }
                        else
                        {
                            //request.response.Add("client_id", request.client.client_id);
                            request.response.Add("delivered", generated["delivered"]);
                            request.response.Add("expire", generated["expire"]);
                            request.response.Add("access_token", generated["access_token"]);
                            request.response.Add("refresh_token", generated["refresh_token"]);
                            request.response.Add("token_type", generated["token_type"]);
                            request.response.Add("scope", generated["scope"]);
                            //request.response.Add("user", result);
                            //request.user2session(result);
                            request.user_updated = true;
                        }
                        return;
                    }
                }
                catch (Exception e)
                {
                    Logger.Error("Users - Login: oAuth Error");
                    throw e;
                }
            }
        }

        //mensaje de error en Redirect previo
        if (request.parameters.ContainsKey("error"))
        {
            request.response.Add("error", request.parameters["error"]);
        }
        /*
        //redirect a otra paginas
        if (request.parameters.ContainsKey("response_url") && !request.parameters["response_url"].Equals(""))
        {
            String redirect = request.parameters["response_url"];
            String querystring = String.Empty;
            int iqs = redirect.IndexOf('?');

            if (iqs >= 0)
            {
                if (iqs < redirect.Length - 1)
                {
                    querystring = redirect.Substring(iqs + 1);
                    redirect = redirect.Substring(0, iqs);
                }

                NameValueCollection parametros = HttpUtility.ParseQueryString(querystring);
                parametros.Remove("client_id");
                parametros.Remove("access_token");

                StringBuilder sb = new StringBuilder(redirect + "?");
                foreach (String key in parametros.AllKeys)
                {
                    sb.Append(key + "=" + parametros[key] + "&");
                }
                redirect = sb.ToString();
                redirect = redirect.Substring(0, redirect.Length - 1);
            }

            request.response.Add("response_url", redirect);
        }
        */
    }

    public static void mis_viajes_abiertos(RequestWrapper request)
    {
        List<GearUp_API.Viajes> viajes = (List<GearUp_API.Viajes>)Dispatcher.dispatchDLL("ViajesBL::ObtenerLista", new object[] { request.user, "A" }, new object[] { request.lang });
        if(viajes.Count > 0)
            request.response.Add("viajes", viajes);
    }

    public static void mi_perfil(RequestWrapper request)
    {
        List<GearUp_API.Paises> paises = (List<GearUp_API.Paises>)Dispatcher.dispatchDLL("PaisesBL::ObtenerLista", new object[] { }, new object[] { request.lang });
        if (paises == null)
        {
            throw new Exception("WEB_ERROR_USERS_1");
        }
        request.response.Add("paises", paises);

        List<GearUp_API.TimeZones> timezones = (List<GearUp_API.TimeZones>)Dispatcher.dispatchDLL("TimeZonesBL::ObtenerLista", new object[] { }, new object[] { request.lang });
        if (timezones == null)
        {
            throw new Exception("WEB_ERROR_USERS_1");
        }
        request.response.Add("timezones", timezones);

        if (!request.verb.Contains("GET"))
        {
            Logger.Debug("Users registrarse - POST");

            bool error = false;
            List<GearUp_API.RespuestasAPI> respuestas = new List<GearUp_API.RespuestasAPI>();
            GearUp_API.Usuarios user = new GearUp_API.Usuarios();

            foreach (var prop in user.GetType().GetProperties())
            {
                if (request.parameters.ContainsKey(prop.Name))
                {
                    try
                    {
                        object tmp = request.parameters[prop.Name];
                        if (prop.PropertyType.FullName.Contains("DateTime"))
                        {
                            if (!request.parameters[prop.Name].Equals(""))
                            {
                                tmp = DateTime.ParseExact(tmp.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                                //tmp = DateTime.ParseExact(tmp.ToString(), request.culture.DateTimeFormat.ShortDatePattern, request.culture);
                            }
                            else
                            {
                                tmp = null;
                            }
                        }
                        else if (prop.PropertyType.FullName.Contains("Decimal"))
                        {
                            decimal dec;
                            if (decimal.TryParse(tmp.ToString(), out dec))
                            {
                                tmp = dec;
                            }
                            else
                            {
                                tmp = 0.0m;
                            }
                        }
                        else if (prop.PropertyType.FullName.Contains("Int16"))
                        {
                            Int16 dec;
                            if (Int16.TryParse(tmp.ToString(), out dec))
                            {
                                tmp = dec;
                            }
                            else
                            {
                                tmp = 0;
                            }
                        }
                        else if (prop.PropertyType.FullName.Contains("Int"))
                        {
                            int dec;
                            if (int.TryParse(tmp.ToString(), out dec))
                            {
                                tmp = dec;
                            }
                            else
                            {
                                tmp = 0;
                            }
                        }
                        else
                        {
                            tmp = Convert.ChangeType(request.parameters[prop.Name], prop.PropertyType);
                        }
                        prop.SetValue(user, tmp, null);
                        Logger.Debug("Usuarios actualizando: " + prop.Name);
                    }
                    catch (Exception e)
                    {
                        Logger.Error(prop.Name + ": " + prop.PropertyType);
                        Logger.Error(e.Message);
                        error = true;
                    }
                }
            }
            request.response.Add("usuario", user);

            if (!request.parameters["Pass"].Equals(request.parameters["PassVerif"]))
                throw new Exception("WEB_ERROR_USERS_2");

            if (!error && (bool)Dispatcher.dispatchDLL("UsuariosBL::Registrar", new object[] { user }, new object[] { request.lang }, out respuestas, out error))
            {
                //validación de persona, el usuario fue creado en la DB
                request.response = new Dictionary<string, object>();
                dynamic client = ClientModel.fromClientId(ConfigManager.GET("OauthClientID"));

                //oAuth generar code
                Dictionary<string, string> generated = oAuth.Implicit(client, user.Email);

                if (generated.ContainsKey("error"))
                {
                    throw new Exception(generated["error"]);
                }
                else
                {
                    Logger.Activity("Token generated: " + generated["access_token"]);
                    request.response.Remove("usuario");
                    request.response.Add("client_id", ConfigManager.GET("OauthClientID"));
                    //request.response.Add("delivered", generated["delivered"]);
                    //request.response.Add("expire", generated["expire"]);
                    request.response.Add("access_token", generated["access_token"]);
                    //request.response.Add("refresh_token", generated["refresh_token"]);
                    //request.response.Add("token_type", generated["token_type"]);
                    //request.response.Add("scope", generated["scope"]);
                    request.redirect = "registro_validacion";
                    return;
                }
            }
            else
            {
                if (respuestas.Count > 0)
                    request.response.Add("errores", respuestas);
                else
                    request.response.Add("result", false);

                return;
            }

            throw new Exception("WEB_ERROR_USERS_3");
        }
    }
}