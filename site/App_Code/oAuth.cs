﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.IO;

/*
* Parameters:
* -	(required) client_id: client identifier
* -	(optional) state: used for CSRF Protection
* -	(required) scope: https://tools.ietf.org/html/rfc6749#section-3.3
* - (required) grant_type, https://tools.ietf.org/html/rfc6749#section-1.3 https://tools.ietf.org/html/rfc6749#section-4:
    * - authorization_code: code for exchange an access_token https://tools.ietf.org/html/rfc6749#section-4.1
    * - implicit: obtain an access_token directly (NOT RECOMENDED) https://tools.ietf.org/html/rfc6749#section-4.2
    * - resource owner password (password): obtain an access_token directly from user/password https://tools.ietf.org/html/rfc6749#section-4.3
    * - client credentials (client): obtain an access_token with client_id and client_secret https://tools.ietf.org/html/rfc6749#section-4.4
* -	(required) response_type, https://tools.ietf.org/html/rfc6749#section-3.1.1:
    * - code: used for authorization_code grant type.
    * - token: used for implicit grant type.
* -	(optional) redirect_uri: registered redirection endpoint
*
* Success Response:
    * -	code or access_token: authorization_code or access_token
    * - token_type: "bearer" https://tools.ietf.org/html/rfc6749#section-7.1
    * - expires_in: lifetime of access_token, default 3600 secs
    * - refresh_token: https://tools.ietf.org/html/rfc6749#section-6
    * - scope: granted scopes
    * -	(optional) state: request state variable
*
* Error Response, https://tools.ietf.org/html/rfc6749#section-5.2:
* - error:
 * - invalid_request
 * - invalid_client
 * - invalid_grant
 * - unauthorized_client
 * - unsupported_grant_type
 * - invalid_scope
* - error_description
* - error_uri
*/
public class oAuth
{
	public oAuth()
	{

	}

    public static bool Validate(RequestWrapper request)
    {
        Logger.Debug("oAuth - Validate");

        if (request.parameters.ContainsKey("access_token") && !request.parameters["access_token"].Equals(""))
        {
            //verificar cliente
            if (!request.parameters.ContainsKey("client_id") || request.parameters["client_id"].Equals(""))
            {
                Logger.Error("oAuth - Validate: client_id no especificado");
                return false;
            }

            dynamic client = ClientModel.fromClientId(request.parameters["client_id"]);
            if (client == null)
            {
                Logger.Error("oAuth - Validate: client_id no valido " + request.parameters["client_id"]);
                //throw new Exception("invalid_request");
                return false;
            }

            //TODO verificacion completa de Client

            request.client = client;
            Logger.Activity("oAuth - Validate: Client loaded " + request.client.description);

            /*
            //client_id | user_id | delivered | expire
            string[] parts = oAuth.Decrypt(request.parameters["access_token"]).Split('|');

            //check expiration
            DateTime expire = DateTime.ParseExact(parts[3], "yyyyMMddHHmmssffff", CultureInfo.InvariantCulture);
            if (expire < DateTime.Now)
            {
                Logger.Error("oAuth - Validate: token expirado " + expire.ToString());
                return false;
            }
            */

            dynamic cu = ClientsUsers.fromAccessToken(request.parameters["access_token"]);
            if (cu == null)
            {
                Logger.Error("oAuth - Validate: token no encontrado " + request.parameters["access_token"]);
                return false;
            }

            //check expiration
            if ((DateTime)cu.expiration < DateTime.Now)
            {
                Logger.Error("oAuth - Validate: token expirado " + cu.expiration.ToString());
                return false;
            }

            //verificar combinacion cliente-token
            //if (!client.client_id.Equals(parts[0]))
            if ((int)client.id != (int)cu.client)
            {
                Logger.Error(string.Format("oAuth - Validate: access_token (para client_id {1}) no pertenece a client_id {0}", cu.client, client.id));
                //throw new Exception("invalid_request");
                return false;
            }

            //validaciones completas, cargo request
            //request.session.Add("ws_user", parts[1]);
            request.session.Add("ws_user", cu.user);
            request.session.Add("ws_firebase", cu.firebase_token);
            //return true;
            return oAuth.SetSession(request);
        }
        else if (request.parameters.ContainsKey("client_id") && !request.parameters["client_id"].Equals(""))
        {
            dynamic client = ClientModel.fromClientId(request.parameters["client_id"]);
            if (client == null)
            {
                Logger.Error("oAuth - Validate: client_id no valido " + request.parameters["client_id"]);
                //throw new Exception("invalid_request");
                return false;
            }

            //TODO verificacion completa de Client

            request.client = client;
            Logger.Activity("oAuth - Validate: Client loaded " + request.client.client_id);
            return true;
        }
        return false;
    }

    public static bool RecalcExpire(RequestWrapper request)
    {
        Logger.Debug("oAuth - RecalcExpire");
        if (request.parameters.ContainsKey("client_id") && request.parameters.ContainsKey("access_token"))
        {
            /*
            string client_id = request.parameters["client_id"];
            string token = request.parameters["access_token"];

            //client_id | user_id | delivered | expire
            string[] parts = oAuth.Decrypt(token).Split('|');
            DateTime expiration_date = DateTime.ParseExact(parts[3], "yyyyMMddHHmmssffff", CultureInfo.InvariantCulture);
            Logger.Debug("Current Time: " + DateTime.Now.ToString());
            Logger.Debug("Token update time: " + expiration_date.AddMinutes(-55).ToString());
            */

            dynamic cu = ClientsUsers.fromAccessToken(request.parameters["access_token"]);
            if (cu == null)
            {
                Logger.Error("oAuth - RecalcExpire: token no encontrado " + request.parameters["access_token"]);
                return false;
            }

            DateTime expiration_date = (DateTime)cu.expiration;
            Logger.Debug("Current Time: " + DateTime.Now.ToString());
            Logger.Debug("Token update time: " + expiration_date.AddMinutes(-5).ToString());

            if (expiration_date.AddMinutes(-5) < DateTime.Now)
            {
                Logger.Activity("oAuth - Recalc access_token expire time");
                /*
                string delivered = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                expiration_date = getExpiration(delivered, "20");
                string expire = expiration_date.ToString("yyyyMMddHHmmssffff");
                token = oAuth.Encrypt(parts[0] + '|' + parts[1] + '|' + delivered + '|' + expire);
                */

                //Dictionary<string, string> generated = oAuth.Implicit(request.client, parts[1]);
                Dictionary<string, string> generated = oAuth.Implicit(request.client, cu.user);
                if (generated.ContainsKey("error"))
                {
                    throw new Exception(generated["error"]);
                }

                //query string parameters
                request.response = new Dictionary<string, object>();
                foreach (String param in request.request.QueryString)
                {
                    request.response.Add(param, request.request.QueryString[param]);
                }
                request.response.Remove("client_id");
                request.response.Remove("access_token");

                request.status_code = 307;
                request.status_msg = "Temporary Redirect";
                request.redirect = request.page + "?client_id=" + generated["client_id"] + "&access_token=" + generated["access_token"];
                return true;
            }
        }
        return false;
    }

    public static bool SetSession(RequestWrapper request)
    {
        Logger.Debug("oAuth - SetSession: loading for user " + request.session["ws_user"]);
        request.timer_start("oauth_session");

        GearUp_API.Usuarios user = (GearUp_API.Usuarios)Dispatcher.dispatchDLL("UsuariosBL::ObtenerPorEmail", new object[] { request.session["ws_user"] }, new object[] { request.lang });
        request.timer_stop("oauth_session");
        if (user != null)
        {
            Logger.Activity("oAuth - SetSession: user loaded " + user.Email);
            return request.user2session(user);
        }
        
        return false;
    }

    //-----------------------------------Implicit --------------------------------------------------------------//
    public static Dictionary<string, string> Implicit(dynamic client, string user_id)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();
        if (client == null || client.client_id == null || client.ttl == null || client.scope == null)
        {
            Logger.Error("oAuth - Implicit: client no valido");
            data.Add("error", "invalid_request");
            return data;
        }
        else
        {
            /*
            string delivered = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            DateTime expiration_date = getExpiration(delivered, client.ttl.ToString());
            string expire = expiration_date.ToString("yyyyMMddHHmmssffff");
            string token = oAuth.Encrypt(client.client_id + '|' + user_id + '|' + delivered + '|' + expire);

            data.Add("client_id", client.client_id);
            data.Add("delivered", delivered);
            data.Add("expire", expire);
            data.Add("access_token", token);
            data.Add("refresh_token", "");
            data.Add("token_type", "bearer");
            data.Add("scope", client.scope);
            */

            dynamic cu = new ClientsUsers((int)client.id, user_id);

            if ((bool)cu.loaded())
            {
                //verifico si el token guardado aun esta activo
                DateTime expiration_date = (DateTime)cu.expiration;
                if (expiration_date.AddMinutes(-5) > DateTime.Now)
                {
                    Logger.Debug("oAuth - Implicit: token vigente");
                    data.Add("client_id", client.client_id.ToString());
                    data.Add("delivered", cu.delivered.ToString());
                    data.Add("expire", cu.expiration.ToString());
                    data.Add("access_token", cu.access_token.ToString());
                    data.Add("refresh_token", cu.refresh_token.ToString());
                    data.Add("token_type", "bearer");
                    data.Add("scope", cu.scope.ToString());
                    return data;
                }
            }

            cu.client = client.id;
            cu.user = user_id;
            cu.scope = client.scope;
            cu.delivered = DateTime.Now;
            cu.expiration = getExpiration(cu.delivered.ToString("yyyyMMddHHmmssffff"), client.ttl.ToString());
            //cu.access_token = oAuth.Encrypt(client.client_id + '|' + cu.user + '|' + cu.delivered.ToString("yyyyMMddHHmmssffff") + '|' + cu.expiration.ToString("yyyyMMddHHmmssffff"));
            cu.access_token = GearUp_API.FuncionesGenerales.TypeAway(200);
            //cu.refresh_token = oAuth.Encrypt(client.client_id + '|' + cu.user + '|' + cu.delivered.ToString("yyyyMMddHHmmssffff"));
            cu.refresh_token = GearUp_API.FuncionesGenerales.TypeAway(200);
            if (cu.save())
            {
                data.Add("client_id", client.client_id.ToString());
                data.Add("delivered", cu.delivered.ToString());
                data.Add("expire", cu.expiration.ToString());
                data.Add("access_token", cu.access_token.ToString());
                data.Add("refresh_token", cu.refresh_token.ToString());
                data.Add("token_type", "bearer");
                data.Add("scope", cu.scope.ToString());
            }
            else
            {
                Logger.Error("oAuth - Implicit: client_user no guardado");
                data.Add("error", "invalid_request");
            }

            return data;
        }
    }

    public static Dictionary<string, string> Implicit(string client_id, string user_id, string scope)
    {
        return Implicit(client_id, user_id, scope, DateTime.Now.ToString("yyyyMMddHHmmssffff"));
    }

    public static Dictionary<string, string> Implicit(string client_id, string user_id, string scope, string delivered)
    {
        Dictionary<string, string> data = new Dictionary<string, string>();

        DateTime expiration_date = getExpiration(delivered, "20");
        string expire = expiration_date.ToString("yyyyMMddHHmmssffff");
        string token = oAuth.Encrypt(client_id + '|' + user_id + '|' + delivered + '|' + expire);
        data.Add("client_id", client_id);
        data.Add("delivered", delivered);
        data.Add("expire", expire);
        data.Add("access_token", token);
        data.Add("refresh_token", "");
        data.Add("token_type", "bearer");
        data.Add("scope", scope);
        return data;
     }

    private static DateTime getExpiration(string delivered, string ttl)
    {
        DateTime delivered_date = DateTime.ParseExact(delivered, "yyyyMMddHHmmssffff", CultureInfo.InvariantCulture);
        double ttl_num;
        if (!double.TryParse(ttl, out ttl_num))
        {
            ttl_num = 0;
        }
        return delivered_date.AddMinutes(ttl_num);
    }

    public static string Encrypt(string text)
    {
        return Convert.ToBase64String(Encoding.Unicode.GetBytes(text));
    }

    public static string Decrypt(string text)
    {
        return Encoding.Unicode.GetString(Convert.FromBase64String(text));
    }
}