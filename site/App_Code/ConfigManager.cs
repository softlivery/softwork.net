﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConfigManager
/// </summary>
public class ConfigManager
{
    private static Dictionary<string, string> configs = null;

    protected ConfigManager()
    {
    }

    public static void Initialize(bool reload = false)
    {
        if (reload || configs == null)
        {
            Logger.Debug("Initializing Configuration Manager");
            configs = new Dictionary<string, string>();
            /*
            GeneralModel params_mod = new GeneralModel("params", true);
            List<Dictionary<string, object>> params_ = params_mod.search();
            foreach (Dictionary<string, object> param in params_)
            {
                configs.Add((string)param["key"], (string)param["value"]);
            }*/
        }
    }

    public static string GET(string property)
    {
        if (configs.ContainsKey(property))
        {
            return configs[property];
        }
        else
        {
            //DynamicXml configs_xml = DynamicXml.Load(HttpContext.Current.Server.MapPath("config.xml"));
            String filename = HttpContext.Current.Request.PhysicalApplicationPath + "config.config";
            DynamicXml configs_xml = DynamicXml.Load(filename);
            ConfigManagerBinder binder = new ConfigManagerBinder(property, false);
            object value;
            if (configs_xml.TryGetMember(binder, out value))
            {
                return (string)value;
            }
            else
            {
                return null;
            }
        }
    }

    public static DynamicXml GETxml(string property)
    {
        //DynamicXml configs_xml = DynamicXml.Load(HttpContext.Current.Server.MapPath("config.xml"));
        String filename = HttpContext.Current.Request.PhysicalApplicationPath + "config.config";
        DynamicXml configs_xml = DynamicXml.Load(filename);
        ConfigManagerBinder binder = new ConfigManagerBinder(property, false);
        object value;
        if (configs_xml.TryGetMember(binder, out value))
        {
            return (DynamicXml)value;
        }
        else
        {
            return null;
        }

    }

    public static DynamicXml GetRoutes()
    {
        //DynamicXml configs_xml = DynamicXml.Load(HttpContext.Current.Server.MapPath("config.xml"));
        String filename = HttpContext.Current.Request.PhysicalApplicationPath + "routing.config";
        DynamicXml configs_xml = DynamicXml.Load(filename);
        ConfigManagerBinder binder = new ConfigManagerBinder("routes", false);
        object value;
        if (configs_xml.TryGetMember(binder, out value))
        {
            return (DynamicXml)value;
        }
        else
        {
            return null;
        }
    }
}

public class ConfigManagerBinder : GetMemberBinder
{
    public ConfigManagerBinder(string name, bool ignoreCase)
    : base(name, ignoreCase)
    {
    }

    public override DynamicMetaObject FallbackGetMember(DynamicMetaObject target, DynamicMetaObject errorSuggestion)
    {
        throw new NotImplementedException();
    }
}