﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Dynamic;

/// <summary>
/// Summary description for DBManager
/// </summary>
public class DBManager
{
    static SqlConnection ODBC;
    static SqlCommand command;
    static SqlDataReader result;

	public DBManager()
	{
	}

    private static SqlConnection getODBC()
    {
        if (ODBC == null || ODBC.State != System.Data.ConnectionState.Open)
        {
            string connectionString = ConfigManager.GET("ConectionString");
            Logger.Debug("DBManager - Connecting to " + connectionString);
            ODBC = new SqlConnection(connectionString);
            ODBC.Open();
        }

        return ODBC;
    }

    public static SqlDataReader query(string query)
    {
        Logger.Db(query);
        try
        {

            command = new SqlCommand(query, getODBC());
            if (result!=null && !result.IsClosed)
            {
                result.Close();
            }
            result = command.ExecuteReader();
        }
        catch (Exception e)
        {
            Logger.Error(query + " -> " + e.Message);
            result = null;
        }
        return result;
    }

    public static SqlDataReader query(string query, out object inserted)
    {
        Logger.Db(query);
        inserted = null;
        try
        {
            command = new SqlCommand(query, getODBC());
            if (result != null && !result.IsClosed)
            {
                result.Close();
            }
            inserted = command.ExecuteScalar();
        }
        catch (Exception e)
        {
            Logger.Error(query + " -> " + e.Message);
        }
        return result;
    }
}